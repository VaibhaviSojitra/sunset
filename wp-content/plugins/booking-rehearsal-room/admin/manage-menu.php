<?php

function fun_ssrb_settings()
{
	if (isset($_GET['pg']) && !empty($_GET['pg'])) {
		include 'views/settings.php';
	}else{
		include 'views/settings.php';
	}
}

function fun_ssrb_rooms()
{
	if (isset($_GET['pg']) && !empty($_GET['pg'])) {
		include 'views/rooms-add.php';
	}else{
		include 'views/rooms-list.php';
	}
}

function fun_ssrb_instruments()
{
	include 'views/instrument-list.php';
}

function fun_ssrb_booking_band()
{

	global $wpdb;
	define('SSRB_URL_BOOKING_BAND', menu_page_url(SSRB_BOOKING_BAND, false));
	$table_rooms = SSRB_PREFIX ."rehearsal_rooms";
	$rooms_list = $wpdb->get_results("SELECT * FROM $table_rooms where is_delete=0 ");

	$table_rates = SSRB_PREFIX ."rate_service";
	$rates_list = $wpdb->get_results("SELECT * FROM $table_rates where is_delete=0 ");

	$times1 = create_time_range('7:00', '11:00', '1 hours');
	$times2 = create_time_range('12:00', '24:00', '1 hours');
	$times3 = create_time_range('1:00', '2:00', '1 hours');

	$times = array_merge($times1,$times2,$times3);

	$cart_page_url = get_option('ssrb_cart_page',true);

	if(isset($_GET['date']))
	{
		$date=$_GET['date'];
		$current_day = date('l',strtotime($date));     
	}else{
		$current_day = date('l');      
		$date = date('Y-m-d');
	}
	$session_type = [];
	$day_time = [];
	$night_time = [];
	$price = [];
	foreach ($rates_list as $k => $rate) {
	
		if($current_day == $rate->week_day){
			if($rate->session_type == 1){
				$price[0] = $rate->price;
				$session_type[0] = $rate->session_type;
			}else 
			if($rate->session_type == 2){
				$price[1] = $rate->price;
				$session_type[1] = $rate->session_type;
			}
			
		}
	}

	$user_id = get_current_user_id();

	foreach($rooms_list as $key => $rval){
		$rooms_list[$key] = $rval;
		$day_time =  create_time_range(date('H:i', strtotime($rval->day_start_time)), date('H:i', strtotime($rval->day_end_time) ), '1 hours');

		$night_time =  create_time_range(date('H:i', strtotime($rval->night_start_time)), date('H:i', strtotime($rval->night_end_time) + 60*60 ), '1 hours');

		$startday_time['time'] = array($day_time[0], $night_time[0]);
		$startday_time['price'] = $price;
		$startday_time['session_type'] = $session_type;
		$endday_time = array(end($day_time), end($night_time));

		$roomsData[$rval->id]['start_day'] = $startday_time;
		$roomsData[$rval->id]['end_day'] = $endday_time;

	}

	include 'users/user-style.php'; 

	$table_instruments = SSRB_PREFIX ."instruments";
	$instruments_list = $wpdb->get_results("SELECT * FROM $table_instruments where is_active = 1 AND is_delete = 0 ");

	$band_users = get_users( [ 'role__in' => [ 'subscriber' ] ] );
	
	include 'views/booking-for-band.php';
}

function create_time_range($start, $end, $interval = '30 mins', $format = '12') {

	$startTime = strtotime($start); 
	$endTime   = strtotime($end);
	$returnTimeFormat = ($format == '12')?'g:ia':'G:ia';

	$current   = time(); 
	$addTime   = strtotime('+'.$interval, $current); 
	$diff      = $addTime - $current;

	$times = array(); 
	while ($startTime < $endTime) { 
		$times[] = date($returnTimeFormat, $startTime); 
		$startTime += $diff; 
	} 
	$times[] = date($returnTimeFormat, $startTime); 
	return $times; 
}
	
function fun_ssrb_booking_list()
{
	include 'views/booking-list.php';
}
?>