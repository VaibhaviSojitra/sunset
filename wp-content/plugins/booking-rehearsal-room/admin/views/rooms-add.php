<?php
if (!defined('ABSPATH')) {
	die();
}
define('SSRB_URL_ROOMS', menu_page_url(SSRB_ROOMS, false));

$id = $_GET['rooms_id'] ? sanitize_text_field($_GET['rooms_id']) : "";
$pg = $_GET['pg'] ? sanitize_title($_GET['pg']) : "";
global $wpdb;
$table_rooms = SSRB_PREFIX ."rehearsal_rooms";
if (isset($id) && !empty($id)) {

	$roomsData = $wpdb->get_results("SELECT * FROM $table_rooms where id='" . $id . "' AND is_delete=0 ORDER BY id ASC ");
	$roomsData = $roomsData[0];
	
	if(empty($roomsData)){
		echo '<div class="container-fluid">
		<h3 class="text-center">No Data Found </h3></div>';
		exit();
	}
}

$table_session_type = SSRB_PREFIX ."session_type";
$session_list = $wpdb->get_results("SELECT * FROM $table_session_type where is_delete = 0 ");

$day_start_time = '';
$day_end_time = '';
$night_start_time = '';
$night_end_time = '';
if(!empty($session_list[0]) && $session_list[0]->start_time){
	$day_start_time = $session_list[0]->start_time;
}
if(!empty($session_list[0]) && $session_list[0]->end_time){
	$day_end_time = $session_list[0]->end_time;
}
if(!empty($session_list[1]) && $session_list[1]->start_time){
	$night_start_time = $session_list[1]->start_time;
}
if(!empty($session_list[1]) && $session_list[1]->end_time){
	$night_end_time = $session_list[1]->end_time;
}

?>
<p style="margin-top: 20px;">
	<a href="<?php echo esc_url(SSRB_URL_ROOMS); ?>" title="<?php _e('View List', SSRB_DOMAIN); ?>" class="btn btn-info"><span>Back to List</span></a>
</p>
<input type="hidden" id="site_url" value="<?php echo site_url(); ?>">
<input type="hidden" id="page_url" value="<?php echo SSRB_URL_ROOMS; ?>">
<div class="container-fluid">
	<h3 class="text-center"><?php echo esc_html($pg == 'edit' ? "Edit" : "Add")?> Room Time Sessions </h3>
	<hr>
	<div class="row">
		<div class="col-md-2 col-lg-2"></div>
			<div class="col-md-8 col-lg-8 ssrb-card">
				<form method="post" id="frmAddUpdateRooms" enctype="multipart/form-data">
				<?php wp_nonce_field('addedit_session_type','addedit_session_type'); ?>
				<section>
						<div class="row">
							<div class="form-group col-md-4 col-sm-4">
								<label for="room_title">Room Title</label>
								<input class="form-control" name="room_title" value="<?php echo esc_html($roomsData->room_title ? $roomsData->room_title : ''); ?>" type="text" placeholder="Enter title" required>
							</div>

							<div class="form-group col-md-4 col-sm-4">
								<label for="room_color">Room Color</label>
								<input class="form-control" name="room_color" value="<?php echo esc_html($roomsData->room_color ? $roomsData->room_color : ''); ?>" type="text" placeholder="Enter Color" required>
							</div>

							<div class="form-group col-md-4 col-sm-4">
								<label for="room_code">Room Code</label>
								<input class="form-control" name="room_code" value="<?php echo esc_html($roomsData->room_code ? $roomsData->room_code : ''); ?>" type="text" placeholder="Enter Code" required>
							</div>
							
						</div>
						<hr>
						<h5 class="text-center">Day Sessions Time </h5>
						<hr>
						<div class="row">
							<div class="form-group col-md-6 col-sm-4">
								<label for="day_starttime">Start Sesion Time</label>
								<input class="form-control" name="day_starttime" id="day_starttime" value="<?php echo esc_html($roomsData->day_start_time ? $roomsData->day_start_time : $day_start_time); ?>" type="text" placeholder="Enter Start Time" readonly>
							</div>

							<div class="form-group col-md-6 col-sm-4">
								<label for="day_endtime">End Sesion Time</label>
								<input class="form-control" name="day_endtime" id="day_endtime" value="<?php echo esc_html($roomsData->day_end_time ? $roomsData->day_end_time : $day_end_time ); ?>" type="text" placeholder="Enter End Time" readonly>
							</div>
							
						</div>

						<div class="row hide">

							<div class="form-group col-md-6 col-sm-4">
								<label for="day_price">Price</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">$</span>
									</div>
									<input class="form-control" name="day_price" id="day_price" value="<?php echo esc_html($roomsData->day_price ? number_format($roomsData->day_price,2) : ''); ?>" type="text" placeholder="Enter Price">
								</div>
							</div>
						</div>
						<hr>
						<h5 class="text-center">Night Sessions Time </h5>
						<hr>
						<div class="row">
							<div class="form-group col-md-6 col-sm-4">
								<label for="night_starttime">Start Sesion Time</label>
								<input class="form-control" name="night_starttime" id="night_starttime" value="<?php echo esc_html($roomsData->night_start_time ? $roomsData->night_start_time : $night_start_time); ?>" type="text" placeholder="Enter Start Time" readonly>
							</div>

							<div class="form-group col-md-6 col-sm-4">
								<label for="night_endtime">End Sesion Time</label>
								<input class="form-control" name="night_endtime" id="night_endtime" value="<?php echo esc_html($roomsData->night_end_time ? $roomsData->night_end_time : $night_end_time); ?>" type="text" placeholder="Enter End Time" readonly>
							</div>
							
						</div>
						
						<div class="row hide">

							<div class="form-group col-md-6 col-sm-4">
								<label for="night_price">Price</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">$</span>
									</div>
									<input class="form-control" name="night_price" id="night_price" value="<?php echo esc_html($roomsData->night_price ? number_format($roomsData->night_price,2) : ''); ?>" type="text" placeholder="Enter Price">
								</div>
							</div>
						</div>
					</section>
					<hr>
					<section>
						<div class="row">
						
							<div class="col-md-10"> <p class="error"></p> </div>
							<div class="col-md-2" style="text-align:right">
								<input type="hidden" id="update" name="hidden_room_id" value="<?= !empty($id) ? $id : 0 ?>">
								<input type="hidden" value="addedit_rooms" name="action">
								<button type="submit" id="save_rooms" class="btn btn-success" >Save</button>
								
							</div>
							
						</div>
					</section>
				</form>
			</div>
		<div class="col-md-2 col-lg-2"></div>
	</div>
</div>
