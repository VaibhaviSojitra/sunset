<?php

if (!defined('ABSPATH')) {

	die();
}

define('SSRB_URL_INSTRUMENT', menu_page_url(SSRB_INSTRUMENT, false));


global $wpdb;
$table_instruments = SSRB_PREFIX ."instruments";
$instruments_list = $wpdb->get_results("SELECT * FROM $table_instruments where is_delete=0 ");

?>

<h2 id="settings_ibl_title">Manage Time Slot</h2>

<p>

	<button class="btn btn-info" role="button" data-toggle="modal" data-target="#MdlInstrument">Add New Instrument</button>
</p>


<div>

	<table id="instrumentDataTable" class="table table-striped w-100 dataTable no-footer no-border dtr-inline">

		<thead>

			<tr>

				<th>Instrument</th>

				<th>Per Session</th>

				<th>Price</th>

				<th>Image</th>

				<th>Status</th>

				<th>Action</th>

			</tr>

		</thead>

		<tbody>

			<?php 

			foreach ($instruments_list as $k => $list) {
				if ($list->session_type == 1) {
					$session_type = 'Day';
				} else if ($list->session_type == 2) {
					$session_type = 'Night';
				} else {
					$session_type = 'Both';
				}

				if($list->is_active == 1){
					$checked = "checked";
				}else{
					$checked = "";
				}

				if ($list->image) {
					$image = site_url().''.$list->image;
				} else {
					$image = SSRB_URL.'/images/default.jpg';
				}
				?>

				<tr id="<?php echo $list->id; ?>">

					<td><?php echo esc_html($list->instrument_name); ?></td>

					<td><?php echo esc_html($session_type); ?></td>

					<td><?=SSRB_BOOKING_CURRENCY?> <?php echo esc_html($list->price); ?></td>

					<td><img class="instrumentimg" src="<?php echo $image ; ?>" width="70" height="70"></td>

					<td>  <label class="switch">
                        <span class="nss-display">Active</span>
                        <input type="checkbox" class="instrumentStatusChanaged" data-id="<?php echo $list->id; ?>" data-status="<?php echo $list->is_active; ?>" <?php echo esc_html($checked); ?>/>
                        <span class="slider slider-green round"></span>
                        </label></td>

					<td>
						<button class="btn btn-info actionbtn edit_instrument" title="Edit" data-toggle="modal" data-target="#MdlInstrument" data-id="<?php echo $list->id; ?>" >  <i class="fa fa-pencil-square-o"></i></button>
						<button title="Delete" data-id="<?php echo $list->id; ?>" class="btn btn-danger actionbtn instrumentDelete"><span class="fa fa-trash"></span></button>
					</td>

				</tr>

			<?php } ?>

		</tbody>

	</table>

</div>

<input type="hidden" id="site_url" value="<?php echo site_url(); ?>">
<input type="hidden" id="plugin_path" value="<?php echo SSRB_URL; ?>">

<div class="modal fade" id="MdlInstrument" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title mdltitle">Add New Instrument</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
	  	<form method="post" id="frmAddUpdateInstrument">
		<?php wp_nonce_field('AddUpdateInstrument','AddUpdateInstrument'); ?>
			<div class="modal-body">
				<div class="row">
					<div class="form-group col-md-12 col-sm-4">
						<label for="instrument_name">Instrument Name</label>
						<input class="form-control" name="instrument_name" id="instrument_name"  type="text" placeholder="Enter Instrument Name" required>
					</div>			
				</div>		

				<div class="row">
					<div class="form-group col-md-6 col-sm-4">
						<label for="session_type">Per Session</label>
						<select name="session_type" class="form-control" id="session_type" required>
							<option value="">Select Session</option>
							<option value="1">Day</option>
							<option value="2">Night</option>
							<option value="3">Both</option>
						</select>
					</div>

					<div class="form-group col-md-6 col-sm-4">
						<label for="price">Price Per Session</label>
						<div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                            </div>
                            <input class="form-control" name="price" id="price" type="text" placeholder="Enter Price" required>

                        </div>
						
					</div>
					
				</div>	


				<div class="row">
					<div class="form-group col-md-12 col-sm-12">
						<label for="upload_instrument_image">Instrument Image</label>
						<div class="">
							<input id="instrument_image_path" type="text" size="36" name="instrument_image" class="form-control2" readonly/>
							<input type="button" value="Upload Image" class="btn btn-primary mediaupload" id="upload_instrument_image" />
							<!-- <input type="file" class="upload_instrument_image" id="upload_instrument_image" name="filename">
							<label class="custom-file-label" for="customFile">Choose file</label> -->
						</div>
					</div>

					<div class="img_preview_div col-md-6 col-sm-6" style="display:none">
						<img id="img_preview" src="" class="img_preview" width="200" height="200" alt="Preview">
						<p class="text-center"><a id="remove_instrument_image">Remove Instrument Image</a></p>
						<!-- <a class="remove-image" href="#" style="display: inline;">×</a> -->
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<p class="error"></p>
				<input type="hidden" name="action" value="addedit_instrument">
				<input type="hidden" name="hidden_id" id="hidden_id" value="0">
				<button type="submit" class="btn btn-success" id="save_instrument">Save</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</form>
    </div>
  </div>
</div>

<script>
	$(document).ready(function() {
		$("#instrumentDataTable").DataTable();
	});
</script>