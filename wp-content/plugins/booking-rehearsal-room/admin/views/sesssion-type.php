<?php

if (!defined('ABSPATH')) {

	die();
}

define('SSRB_URL_ADMIN_PAGE', menu_page_url(SSRB_DOMAIN, false));


global $wpdb;
$tablename = SSRB_PREFIX ."session_type";
$session_list = $wpdb->get_results("SELECT * FROM $tablename where is_delete=0 ");

?>
<form method="post" id="frmAddUpdateSessionType" >
<?php wp_nonce_field('frmAddUpdateSessionType','frmAddUpdateSessionType'); ?>
    <div class="card-body">
        <div class="form-row form-group bookly-js-range-row">
            <div class="col-12 col-lg-6">
                <div class="form-row align-items-center">
                    <div class="col-3">
                        Day Session              
                    </div>
                    <div class="col">
                        <input class="form-control ui-timepicker-input" name="session[1][starttime]" id="day_starttime" value="<?= !empty($session_list[0]) ? $session_list[0]->start_time : '' ?>" type="text" placeholder="Select Start Time" autocomplete="off" required>
                    </div>
                    <div class="col-auto">
                        to                    
                    </div>
                    <div class="col">
                        <input class="form-control ui-timepicker-input" name="session[1][endtime]" id="day_endtime" value="<?= !empty($session_list[0]) ? $session_list[0]->end_time : '' ?>" type="text" placeholder="Select End Time" autocomplete="off" required>
                    </div>
                </div>
                <input type="hidden" name="session[1][hidden_id]" value="<?= !empty($session_list[0]) ? $session_list[0]->id : 0 ?>">
            </div>
        </div>  
        <div class="form-row form-group bookly-js-range-row">
            <div class="col-12 col-lg-6">
                <div class="form-row align-items-center">
                    <div class="col-3">
                        Night Session              
                    </div>
                    <div class="col">
                        <input class="form-control ui-timepicker-input" name="session[2][starttime]" id="night_starttime" value="<?= !empty($session_list[1]) ? $session_list[1]->start_time : '' ?>" type="text" placeholder="Select Start Time" autocomplete="off" required>
                    </div>
                    <div class="col-auto">
                        to                    
                    </div>
                    <div class="col">
                        <input class="form-control ui-timepicker-input" name="session[2][endtime]" id="night_endtime" value="<?= !empty($session_list[1]) ? $session_list[1]->end_time : '' ?>" type="text" placeholder="Select End Time" autocomplete="off" required>
                    </div>
                </div>
                <input type="hidden" name="session[2][hidden_id]" value="<?= !empty($session_list[1]) ? $session_list[1]->id : '' ?>">
            </div>
        </div>        
    </div>   
    <div class="card-footer bg-transparent d-flex justify-content-end">
        <input type="hidden" name="action" value="addedit_settings">
        <input type="hidden" name="hiddenflag" value="session_type">
        <button type="submit" id="settings-save" class="btn ladda-button btn-success" ><span class="ladda-label">Save</span></button>      
    </div>
</form> 

