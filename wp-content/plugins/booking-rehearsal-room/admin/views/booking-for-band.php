<link rel='stylesheet' id='ssrb-users-css'  href="<?php echo SSRB_URL ?>/css/ssrb-user-style.css" type='text/css' media='all' />
<div id="loader"></div>
<div class="container-fluid">
<h2>Book Room For Band</h2>
    <input type="hidden" class="current_url" value="<?php echo SSRB_URL_BOOKING_BAND; ?>">
    <input type="hidden" id="site_url" value="<?php echo site_url(); ?>">
    <input type="hidden" id="plugin_path" value="<?php echo SSRB_URL; ?>">
    <input type="hidden" class="current_date" value="<?php echo date('Y-m-d', strtotime($date)) ?>"> 
    <div class="row">
        <div class="col-md-2 pr-0">
                <div class="custom-control custom-checkbox phone_booking">
                    <input type="checkbox" class="custom-control-input" name="phone_booking" id="phone_booking">
                    <label class="custom-control-label" for="phone_booking">Not Registered User(Phone Booking)</label>
                </div>                
            </div>
        <div class="form-group col-md-4 registered_user">
            <label class="control-label" for="user_id"><b>Select Band/User</b></label>
            <div class="">
                <select type="text" class="form-control" name="user_id" id="user_id">
                    <option value=""> Select</option>   
                    <?php foreach($band_users as $val){
                    $band_name = get_user_meta( $val->id,'band_name');
                    if($band_name){
                        $band = '( '. ucfirst($band_name[0]).' )';
                    }else{
                        $band = '';
                    }
                    
                    if($val->first_name && $val->last_name ){
                        $username =  ucfirst($val->first_name) . ' ' . ucfirst($val->last_name);
                    }else{
                        $username = ucfirst($val->display_name);
                    }

                    ?>
                    <option value="<?php echo $val->id?>"> <?php echo $username.' '. $band;?></option>  
                    <?php } ?>                               
                </select>
            </div>
        </div> 

        <div class="form-group col-md-3 non_registered_user hide">
            <label class="control-label" for="user_name"><b>Enter User Full Name</b></label>
            <div class="">
                <input type="text" class="form-control" name="user_name" id="user_name">
            </div>
        </div>  
        <div class="form-group col-md-3 non_registered_user hide">
            <label class="control-label" for="user_email"><b>Enter User Email</b></label>
            <div class="">
                <input type="text" class="form-control" name="user_email" id="user_email">
            </div>
        </div> 
        <div class="form-group col-md-3 non_registered_user hide">
            <label class="control-label" for="band_name"><b>Enter User Band Name</b></label>
            <div class="">
                <input type="text" class="form-control" name="band_name" id="band_name">
            </div>
        </div>   
    </div>

    <?php 
    if($date == date('Y-m-d')){
        $class="col-md-10";
        $showdiv = 'display:none;'; 
    }else{
        $class="col-md-8";
        $showdiv = 'display:block;'; 
    }

    $weekday = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];

    ?>
    <div class="row ssrbbox">
        <div class="col-md-2 " style="<?php echo $showdiv ?>">
            <a class="day_link" href="<?php echo (SSRB_URL_BOOKING_BAND); ?>&date=<?php echo date('Y-m-d', strtotime($date.'-1 day')) ?>"><i class="fa fa-arrow-left"></i><span>Yesterday</span></a>
        </div>
        <div class="row <?php echo  $class ?>">
            <div class="col-md-5">
                <div class="custom-control custom-checkbox weekly_book">
                    <input type="checkbox" class="custom-control-input" name="is_week" id="weekliChecked">
                    <label class="custom-control-label" for="weekliChecked"> Permanent Bookings </label>
                </div>                
            </div>
            <div class="col-md-4">

                <input type="text" id="change_bookdate" class="form-control"  value="<?php echo date('Y-m-d', strtotime($date)) ?>">

                <select class="form-control hide" id="change_bookweekday">
                    <?php foreach($weekday as $val){ ?>
                        <option value="<?php echo $val?>"><?php echo $val?></option>
                    <?php } ?>
                </select>

            </div>

        </div>
        <div class="col-md-2 text-right">
            <a class="day_link" href="<?php echo (SSRB_URL_BOOKING_BAND); ?>&date=<?php echo date('Y-m-d', strtotime($date.'+1 day')) ?>"><span class="small-text">Tomorrow</span><i class="fa fa-arrow-right"></i></a>
        </div>
    </div>
    <div class="row">
        <table  class="ssrb_table mt-0">
        <tr>
                <th rowspan="2" class="ssrb_room_th text-left"><span>Room</span></th>
                <th colspan="<?php echo count($times);?>" class="ssrb_time_th text-center"><span class="display_date"><?php echo date('l, dS F', strtotime($date)) ?></span></th>
              
            </tr>
            <tr>
                <?php foreach($times as $key=>$val){ ?>
                    <th class="ssrb_time_th text-center"><?php echo date('ga', strtotime($val)); ?></th>
                <?php } ?>
                
            </tr>
        <?php
        $user_id = get_current_user_id();
        $table_cart = SSRB_PREFIX ."cart";
        $table_booking_details = SSRB_PREFIX ."booking_details";
        foreach ($rooms_list as $roomkey => $room) {
            echo "<tr>
                <td class='ssrb_room_td text-left'><div>" .$room->room_title.' ('.$room->room_color.')' ."</div></td>";
                $indexCurrentBook = -1;
                $tdColspan = 0;
               
                foreach ($times as $hourIndex => $hour) {
                    if ($indexCurrentBook >= 0) {
                        $disabled = '';
                        if ($hour == $roomsData[$room->id]['end_day'][$indexCurrentBook]) { 

                            $week_day = date('l', strtotime($date));

                            $cart_list = $wpdb->get_results("SELECT * FROM $table_cart where room_id = $room->id AND session_type = $session_type AND ( booking_date = '".$date."' OR week_day = '".$week_day."') AND is_delete = 0 ");

                            if(!empty($cart_list)){
                                $user = get_userdata( $cart_list[0]->user_id);
                                $user_info = get_user_meta( $cart_list[0]->user_id,'band_name');
                                if($user_info[0]){
                                    $username = ucfirst($user_info[0]);
                                }else
                                if($user->first_name && $user->last_name ){
                                    $username =  ucfirst($user->first_name) . ' ' . ucfirst($user->last_name);
                                }else{
                                    $username = ucfirst($user->display_name);
                                }
                                $disabled = 'disabled';
                                $class = "ssrb_booked ssrb_booked".$session_type;
                                $lbltext = strtoupper($username);
                                $book_date = $cart_list[0]->booking_date;
                                $dataactive = 1;
                            }

                            $booking_list = $wpdb->get_results("SELECT * FROM $table_booking_details where room_id = $room->id AND session_type = $session_type AND ( booking_date = '".$date."' OR week_day = '".$week_day."') AND is_delete = 0 ");

                            if(!empty($booking_list)){
                                $_payment_status = get_post_meta($booking_list[0]->order_id, '_payment_status', true);
                                if($_payment_status != 3 && $_payment_status != 4){
                                    $user = get_userdata( $booking_list[0]->user_id);
                                    $user_info = get_user_meta( $booking_list[0]->user_id,'band_name');
                                    if($user_info[0]){
                                        $username = ucfirst($user_info[0]);
                                    }else
                                    if($user->first_name && $user->last_name ){
                                        $username =  ucfirst($user->first_name) . ' ' . ucfirst($user->last_name);
                                    }else{
                                        $username = ucfirst($user->display_name);
                                    }
                                    $disabled = 'disabled';
                                    $lbltext = strtoupper($username);
                                    $class = "ssrb_booked ssrb_booked".$session_type;
                                    if($booking_list[0]->is_week == 1){
                                        $book_date = $booking_list[0]->week_day;
                                    }else{
                                        $book_date = $booking_list[0]->booking_date;
                                    }
                                    $dataactive = 1;
                                }else{
                                    $disabled = '';
                                    $lbltext = 'AVAILABLE';
                                    $class = "ssrb_available ssrb_available".$session_type;
                                    $book_date = date('Y-m-d', strtotime($date));
                                    $dataactive = 0;
                                } 
                                
                            }
                            if(empty($cart_list) && empty($booking_list)){
                                $disabled = '';
                                $lbltext = 'AVAILABLE';
                                $class = "ssrb_available ssrb_available".$session_type;
                                $book_date = date('Y-m-d', strtotime($date));
                                $dataactive = 0;
                            }
                            echo '<td colspan="'.$tdColspan.'" class="ssrb_time_td"> <a class="booking_room booking_room_'.$session_type.' '.$disabled.'" data-id="'.$room->id.'" data-session_type="'.$session_type.'" data-booking_date="'.$book_date.'" data-price="'.$price1.'" ><div class="ssrb_roundCorners '.$class.'" data-active="'.$dataactive.'"> '.$lbltext.' | '.SSRB_BOOKING_CURRENCY.' '.$price1.' </div>
                            </a> </td>';
                            $indexCurrentBook = -1;
                        } else {
                            $tdColspan++;
                        }
                    }
            
                    if ($indexCurrentBook < 0) {
                        $tdColspan = 1;
                        foreach($roomsData[$room->id]['start_day']['time'] as $startBookIndex => $startBookDate) {
                            if ($hour == $startBookDate) {
                                $indexCurrentBook = $startBookIndex;
                                $price1 = $roomsData[$room->id]['start_day']['price'][$startBookIndex];
                                $session_type = $roomsData[$room->id]['start_day']['session_type'][$startBookIndex];
                            }
                        }
                    if ($indexCurrentBook < 0) echo "<td class=\"blank\"></td>";
                    }
                }
            echo "</tr>";
        } ?>
        
        </table>
    </div>
    <div class="row">
        <div class="form-group col-md-3">
            <label class="control-label" for="instrument"><b>Instrument</b></label>
            <select type="text" class="form-control" name="instrument" id="instrument">
                <option value=""> Select</option>                                   
            </select>
        </div>
        <div class="form-group col-md-2">
            <label class="control-label" for="instrument"><b>Total Price</b></label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text"><?=SSRB_BOOKING_CURRENCY?></span>
                </div>
                <input type="text" class="form-control" name="total_price" id="total_price" readonly>
            </div>
        </div>
        <div class="form-group col-md-4 displaybookedroom">
            
        </div>
        <div class="col-md-3">
           <button class="book_room submit_button">Book Room</button>
        </div>
    </div>
    <div class="error"></div>       

</div>

<script>
var $ = jQuery;
var ajaxurl = "<?php echo admin_url('admin-ajax.php') ?>";

$(document).ready(function () {
    $('#loader').hide();
    $('.booking_room').click(function () { if ($(this).hasClass('disabled')) {console.log(1); return false; } });

    $(document).on('click', '#phone_booking', function () {

        if($(this).prop("checked") == true){
            $('.registered_user').addClass('hide');
            $('.non_registered_user').removeClass('hide');
        }
        else if($(this).prop("checked") == false){   
            $('.registered_user').removeClass('hide');
            $('.non_registered_user').addClass('hide');         
        }
    });

    var date = $('#change_bookdate').val();

    $('#change_bookdate').datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: new Date(),
    });
    
    var suffix = "";
    function ordinal_suffix_of(i) {
        var j = i % 10,
            k = i % 100;
        if (j == 1 && k != 11) {
            return i + "st";
        }
        if (j == 2 && k != 12) {
            return i + "nd";
        }
        if (j == 3 && k != 13) {
            return i + "rd";
        }
        return i + "th";
    }

    function getRateByDay(weekdayname,date,flag){
        $('#loader').show();
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: { "weekdayname": weekdayname, "date" : date, "flag": flag, "action": 'get_ratebyweekday' },
            cache: false,
            dataType: "json",
            success: function (response) {
                $('#loader').hide();
                if (response.success == 1) {
                    var html = '';
                    $(".booking_room div").attr('data-active',0);
                    $('.booking_room').removeClass('disabled');
                    $('.booking_room div').addClass('ssrb_available ssrb_available1');
                    $('.booking_room div').removeClass('ssrb_booked ssrb_booked1');

                    $(response.data).each(function( index,value ) {
                        if(value.session_type == 1){
                            $('.booking_room_1').attr('data-price',value.price);
                            $('.booking_room_1 div').text('AVAILABLE | '+value.price);
                        }else{
                            $('.booking_room_2').attr('data-price',value.price);
                            $('.booking_room_2 div').text('AVAILABLE | '+value.price);
                        }
                        $('.booking_room').attr('data-booking_date',date);
                    });
                    var  is_html = 0;
                    if((response.bookingdata).length > 0){
                        html += '<label>Already Booked Room for '+weekdayname+'</label>';
                        $(response.bookingdata).each(function( index,value ) {

                            if(flag == 1){
                               
                                var dayIndex = getDayIndex(weekdayname);
                                var mydate = new Date();
                                mydate.setDate(mydate.getDate() + (dayIndex - 1 - mydate.getDay() + 7) % 7 + 1);

                                var dd = ("0" + mydate.getDate()).slice(-2);
                                var mm = ("0" + (mydate.getMonth() + 1)).slice(-2);
                                var yy = mydate.getFullYear();
                                var maxDate = yy + '-'+ mm + '-'+ dd;

                                var frmdate_dayame = weekday[mydate.getDay()];
                                var frmdate_mame = monthNames[mydate.getMonth()];
                                var fromDate = frmdate_dayame + ', '+ ordinal_suffix_of(mydate.getDate()) + ' '+ frmdate_mame;

                                $('.display_date').text('Every '+weekdayname); 

                                if(value.is_week == 0){

                                    // if(value.booking_date==maxDate){
                                    //     var mydate = new Date(value.booking_date);
                                    //     mydate.setDate(mydate.getDate() + (dayIndex - 1 - mydate.getDay() + 7) % 7 + 1);
                                    // }                                  

                                    if( new Date().getTime() <= new Date(value.booking_date).getTime() ){
                                        var mydate1 = new Date(value.booking_date);

                                        var frmdate_dayame = weekday[mydate1.getDay()];
                                        var frmdate_mame = monthNames[mydate1.getMonth()];
                                        var fromDate = frmdate_dayame + ', '+ ordinal_suffix_of(mydate1.getDate()) + ' '+ frmdate_mame;
                                        if(value.session_type == 1){
                                            var subhtml = 'Day ('+value.day_start_time+')';
                                        }else{
                                            var subhtml = 'Night ('+value.night_start_time+')';
                                        }
                                        html += '<h6>'+ value.room_title+' - '+ subhtml+ ' - ' +fromDate+'</h6>';
                                        is_html = 1;
                                    }
                                }else{
                                    
                                    var user_name = '';
                                    $('.booking_room').each(function( i,v ) {
                                        if ( $(this).attr('data-id') == value.room_id && $(this).attr('data-session_type') == value.session_type ) {
                                            $.ajax({
                                                type: 'POST',
                                                url: ajaxurl,
                                                data: { "user_id": value.user_id, "action": 'get_username' },
                                                cache: false,
                                                dataType: "json",
                                                success: function (response) {
                                                    user_name = response.username;
                                                    $(".booking_room_"+value.session_type+"[data-id='"+value.room_id+"']").find("div").text(user_name+' | '+value.price);
                                                }
                                            });
                                            $(".booking_room_"+value.session_type+"[data-id='"+value.room_id+"']").addClass('disabled');
                                            $(".booking_room_"+value.session_type+"[data-id='"+value.room_id+"']").find("div").removeClass('ssrb_available ssrb_available'+value.session_type);
                                            $(".booking_room_"+value.session_type+"[data-id='"+value.room_id+"']").find("div").addClass('ssrb_booked ssrb_booked1');
                                            $(".booking_room_"+value.session_type+"[data-id='"+value.room_id+"']").find("div").attr('data-active',1);
                                            
                                        }
                                    });
                                }
                                
                            }else{
                                var user_name = '';
                                $('.booking_room').each(function( i,v ) {
                                    if(value.booking_date == date){
                                        if ( $(this).attr('data-id') == value.room_id && $(this).attr('data-session_type') == value.session_type ) {
                                            $.ajax({
                                                type: 'POST',
                                                url: ajaxurl,
                                                data: { "user_id": value.user_id, "action": 'get_username' },
                                                cache: false,
                                                dataType: "json",
                                                success: function (response) {
                                                    user_name = response.username;
                                                    $(".booking_room_"+value.session_type+"[data-id='"+value.room_id+"']").find("div").text(user_name+' | '+value.price);
                                                }
                                            });
                                            $(".booking_room_"+value.session_type+"[data-id='"+value.room_id+"']").addClass('disabled');
                                            $(".booking_room_"+value.session_type+"[data-id='"+value.room_id+"']").find("div").removeClass('ssrb_available ssrb_available'+value.session_type);
                                            $(".booking_room_"+value.session_type+"[data-id='"+value.room_id+"']").find("div").addClass('ssrb_booked ssrb_booked1');
                                            $(".booking_room_"+value.session_type+"[data-id='"+value.room_id+"']").find("div").attr('data-active',1);
                                            
                                        }
                                    }
                                });
                            }
                        });
                    }else{
                        if(flag == 1){
                            var dayIndex = getDayIndex(weekdayname);
                            var mydate = new Date();
                            mydate.setDate(mydate.getDate() + (dayIndex - 1 - mydate.getDay() + 7) % 7 + 1);

                            var frmdate_dayame = weekday[mydate.getDay()];
                            var frmdate_mame = monthNames[mydate.getMonth()];
                            var fromDate = frmdate_dayame + ', '+ ordinal_suffix_of(mydate.getDate()) + ' '+ frmdate_mame;
                            $('.display_date').text('Every '+weekdayname);   
                        }
                    }
                    if(is_html == 1){
                        $('.displaybookedroom').html(html);  
                    }else{
                        $('.displaybookedroom').html("");  
                    }
                                     
                }
            }
        });
    }
    function getDayIndex(weekdayname){
        var dayIndex = 0;
        switch(weekdayname){ 
            case 'Monday': 
                dayIndex = 1;
                break;
            case 'Tuesday': 
                dayIndex = 2;
                break;
            case 'Wednesday': 
                dayIndex = 3;
                break;		
            case 'Thursday': 
                dayIndex = 4;
                break;
            case 'Friday': 
                dayIndex = 5;
            break;
            case 'Saturday': 
                dayIndex = 6;
            break;
            default:
                dayIndex = 0;
        }
        return dayIndex;
    }
    function DisplayDate(weekdayname){
        var dayIndex = getDayIndex(weekdayname);
        var mydate = new Date();
        mydate.setDate(mydate.getDate() + (dayIndex - 1 - mydate.getDay() + 7) % 7 + 1);

        // console.log(mydate);
        var dd = ("0" + mydate.getDate()).slice(-2);
        var mm = ("0" + (mydate.getMonth() + 1)).slice(-2);
        var yy = mydate.getFullYear();
        var maxDate = yy + '-'+ mm + '-'+ dd;

        getRateByDay(weekdayname,maxDate,1);
    }

    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ]; 

    $(document).on('click', '#weekliChecked', function () {
        var date = $('.current_date').val();
        var current_url = $('.current_url').val();

        if($(this).prop("checked") == true){
            window.history.pushState({}, null, current_url);
            // var myNewURL = "my-new-URL.php";//the new URL
            // window.history.pushState("object or string", "Title", "/" + myNewURL );

            $('#change_bookdate').addClass('hide');
            $('#change_bookweekday').removeClass('hide');
            $('.day_link').addClass('hide');

            var weekdayname = $('#change_bookweekday').val();
           
            DisplayDate(weekdayname);		
                
            // var mydate = new Date(date);
                      
            // var frmdate_dayame = weekday[mydate.getDay()];
            // var frmdate_mame = monthNames[mydate.getMonth()];
            // var fromDate = frmdate_dayame + ', '+ ordinal_suffix_of(mydate.getDate()) + ' '+ frmdate_mame;

            // mydate.setDate(mydate.getDate() + 6); 

            // var dd = mydate.getDate();
            // var mm = mydate.getMonth() + 1;
            // var yy = mydate.getFullYear();
            // var maxDate = yy + '-'+ mm + '-'+ dd;

            // var todate_dayame = weekday[mydate.getDay()];
            // var todate_mame = monthNames[mydate.getMonth()];
            // var toDate = todate_dayame + ', '+ ordinal_suffix_of(dd) + ' '+ todate_mame;
            // $('.display_date').text(fromDate+' - '+toDate);
            // $('#change_bookdate').datepicker("option", { "minDate":  new Date(date),"maxDate":  new Date(maxDate) });  
        }
        else if($(this).prop("checked") == false){
            $('.displaybookedroom').html("");  
            $('#change_bookdate').removeClass('hide');
            $('#change_bookweekday').addClass('hide');
            $('.day_link').removeClass('hide');
            var date = $('#change_bookdate').val(); 
            
            var mydate = new Date(date);
                      
            var frmdate_dayame = weekday[mydate.getDay()];

            var dd = ("0" + mydate.getDate()).slice(-2);
            var mm = ("0" + (mydate.getMonth() + 1)).slice(-2);
            var yy = mydate.getFullYear();
            var maxDate = yy + '-'+ mm + '-'+ dd;
            getRateByDay(frmdate_dayame,maxDate,2);

            var frmdate_mame = monthNames[mydate.getMonth()];
            var fromDate = frmdate_dayame + ', '+ ordinal_suffix_of(mydate.getDate()) + ' '+ frmdate_mame;
            $('.display_date').text(fromDate);
            $('#change_bookdate').datepicker("option", { "minDate": new Date() });                
        }
    });
    $(document).on('change', '#change_bookdate', function () {
        
        var date = $(this).val();
        var current_url = $('.current_url').val();
        window.location.href = current_url + '&date=' + date; 
    });

    $(document).on('change', '#change_bookweekday', function () {
        
        var weekdayname = $(this).val();
        DisplayDate(weekdayname)
        
    });

    $(document).on('click', '.booking_room_1', function (event) {
        event.preventDefault();
        $('.error').hide();
        $('.ssrb_booked1').each(function () {
            if (!$(this).parent().hasClass('disabled')) {
                $(this).removeClass('ssrb_booked');
                $(this).removeClass('ssrb_booked1');
                $(this).addClass('ssrb_available');
                $(this).addClass('ssrb_available1');
            }
        });

        if ($(this).find('div').attr('data-active') == 1) {
            $(this).find('div').removeClass('ssrb_booked');
            $(this).find('div').removeClass('ssrb_booked1');
            $(this).find('div').addClass('ssrb_available');
            $(this).find('div').addClass('ssrb_available1');
            $(this).find('div').attr('data-active', 0);
            var html = '<option value="">Select</option>';
            $('#instrument').html(html);

        } else {
            $(this).find('div').removeClass('ssrb_available');
            $(this).find('div').removeClass('ssrb_available1');
            $(this).find('div').addClass('ssrb_booked');
            $(this).find('div').addClass('ssrb_booked1');
            $(this).find('div').attr('data-active', 1);

            var ItemArray = [];
            $('.ssrb_booked').each(function () {
                if (!$(this).parent().hasClass('disabled')) {
                    var session_type = $(this).parent().attr('data-session_type');
                    ItemArray.push(session_type);
                }
            });

            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: { "ItemArray": ItemArray, "flag": 1, "action": 'get_instrument' },
                cache: false,
                dataType: "json",
                success: function (response) {
                    var html = '<option value="">Select</option>';

                    if (response.success == 1) {
                        $(response.data).each(function(i,value) {
                            if(value.session_type == 1){
                                var session_type = 'per Day Session';
                            }else if(value.session_type == 2){
                                var session_type = 'per Night Session';
                            }else{
                                var session_type = 'per Day/Night Session';
                            }
                            html += '<option value="'+value.id+'">'+value.instrument_name+' (<?=SSRB_BOOKING_CURRENCY?> '+value.price+' '+ session_type +')</option>';
                        });

                    }
                    $('#instrument').html(html);
                }
            });
        }
        var pricerray = [];
        $('.ssrb_booked').each(function () {
            if (!$(this).parent().hasClass('disabled')) {
                var price = $(this).parent().attr('data-price');
                pricerray.push(price);
            }
        });
        // console.log(pricerray);
        var total = 0;
        for (var i = 0; i < pricerray.length; i++) {
            total += pricerray[i] << 0;
        }
        $('#total_price').val(total.toFixed(2));
    });
    
    $(document).on('click', '.booking_room_2', function (event) {
        event.preventDefault();
        $('.error').hide();
        $('.ssrb_booked2').each(function () {
            if (!$(this).parent().hasClass('disabled')) {
                $(this).removeClass('ssrb_booked');
                $(this).removeClass('ssrb_booked2');
                $(this).addClass('ssrb_available');
                $(this).addClass('ssrb_available2');
            }
        });

        if ($(this).find('div').attr('data-active') == 1) {
            $(this).find('div').removeClass('ssrb_booked');
            $(this).find('div').removeClass('ssrb_booked2');
            $(this).find('div').addClass('ssrb_available');
            $(this).find('div').addClass('ssrb_available2');
            $(this).find('div').attr('data-active', 0);
            var html = '<option value="">Select</option>';
            $('#instrument').html(html);

        } else {
            $(this).find('div').removeClass('ssrb_available');
            $(this).find('div').removeClass('ssrb_available2');
            $(this).find('div').addClass('ssrb_booked');
            $(this).find('div').addClass('ssrb_booked2');
            $(this).find('div').attr('data-active', 1);
            var ItemArray = [];
            $('.ssrb_booked').each(function () {
                if (!$(this).parent().hasClass('disabled')) {
                    var session_type = $(this).parent().attr('data-session_type');
                    ItemArray.push(session_type);
                }
            });
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: { "ItemArray": ItemArray, "flag": 1, "action": 'get_instrument' },
                cache: false,
                dataType: "json",
                success: function (response) {
                    var html = '<option value="">Select</option>';

                    if (response.success == 1) {
                        $(response.data).each(function(i,value) {
                            if(value.session_type == 1){
                                var session_type = 'per Day Session';
                            }else if(value.session_type == 2){
                                var session_type = 'per Night Session';
                            }else{
                                var session_type = 'per Day/Night Session';
                            }
                            html += '<option value="'+value.id+'">'+value.instrument_name+' (<?=SSRB_BOOKING_CURRENCY?> '+value.price+' '+ session_type +')</option>';
                        });
                    }
                    $('#instrument').html(html);
                }
            });
        }
        var pricerray = [];
        $('.ssrb_booked').each(function () {
            if (!$(this).parent().hasClass('disabled')) {
                var price = $(this).parent().attr('data-price');
                pricerray.push(price);
            }
        });
        console.log(pricerray);
        var total = 0;
        for (var i = 0; i < pricerray.length; i++) {
            total += pricerray[i] << 0;
        }
        $('#total_price').val(total.toFixed(2));
    });
    
    
    $(document).on('change', '#instrument', function (event) {
        var id = $(this).val();
        var site_url = $('#site_url').val();
        var plugin_path = $('#plugin_path').val();
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: { "id": id, "flag": 2, "action": 'get_instrument' },
            cache: false,
            dataType: "json",
            success: function (response) {
                
                if (response.success == 1) {
                    var ItemArray = [];
                    var pricerray = [];
                    $('.ssrb_booked').each(function () {
                        if (!$(this).parent().hasClass('disabled')) {
                            var session_type = $(this).parent().attr('data-session_type');
                            var price = $(this).parent().attr('data-price');
                            ItemArray.push(session_type);
                            pricerray.push(price);
                        }
                    });
                    if(response.data[0].session_type == 3){
                        var cnt = ItemArray.length;
                        var price = response.data[0].price * cnt;
                    }else{
                        var price = response.data[0].price;
                    }
                    var total = 0;
                    for (var i = 0; i < pricerray.length; i++) {
                        total += pricerray[i] << 0;
                    }
                    total = parseFloat(total) + parseFloat(price);
                    $('#total_price').val(total.toFixed(2));
                }else{
                    var ItemArray = [];
                    var pricerray = [];
                    $('.ssrb_booked').each(function () {
                        if (!$(this).parent().hasClass('disabled')) {
                            var session_type = $(this).parent().attr('data-session_type');
                            var price = $(this).parent().attr('data-price');
                            ItemArray.push(session_type);
                            pricerray.push(price);
                        }
                    });

                    var total = 0;
                    for (var i = 0; i < pricerray.length; i++) {
                        total += pricerray[i] << 0;
                    }
                    total = parseFloat(total);
                    $('#total_price').val(total.toFixed(2));
                }
                
            }
        });
    });

    $('.error').hide();
    $(document).on('click', '.book_room', function (event) {
        event.preventDefault();
        var ItemArray = [];
        $('.error').hide();
        var $this = $(this);
        $(this).prop("disabled", true);

        $(this).html(
            '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...'
        );

        var current_url = $('.current_url').val();
        var instrument = $('#instrument').val();
       
        var total_price = $('#total_price').val();
       
        if($('#weekliChecked').prop("checked") == true){
            var is_week = 1;
            var week_day = $('#change_bookweekday').val();
        }else{
            var is_week = 0;
            var week_day = "";
        }

        if($('#phone_booking').prop("checked") == true){
            var user_id = "";
            var user_name = $('#user_name').val();
            var user_email = $('#user_email').val();
            var band_name = $('#band_name').val();
            var user_flag = 1;
        }else{
            var user_id = $('#user_id').val();
            var user_name = "";
            var user_email = "";
            var band_name = "";
            var user_flag = 2;
        }
        // var booking_date = $('.current_date').val();
        $('.ssrb_booked').each(function () {
            if (!$(this).parent().hasClass('disabled')) {
                var room_id = $(this).parent().attr('data-id');
                var price = $(this).parent().attr('data-price');
                var session_type = $(this).parent().attr('data-session_type');
                var booking_date = $(this).parent().attr('data-booking_date');
                ItemArray.push({
                    session_type: session_type,
                    price: price,
                    room_id: room_id,
                    booking_date: booking_date,
                    week_day: week_day
                });
            }
        });
        if(user_flag == 1){
            // if(user_name == "" || user_email == ""|| band_name == ""){
            if(user_name == ""){
                $this.prop("disabled", false);

                $this.html(
                    'Book Room'
                );
                $('#user_id').attr('required',true);
                $('.error').show();
                $('.error').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error! </strong><span> Please Enter Band/User Name.</span> </div> '); 
                return false;
            }
        }else if(user_flag == 2){
            if(user_id == ""){
                $this.prop("disabled", false);

                $this.html(
                    'Book Room'
                );
                $('#user_id').attr('required',true);
                $('.error').show();
                $('.error').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error! </strong><span> Please Select Band/User.</span> </div> '); 
                return false;
            }
        }
        
            if (ItemArray.length != 0) {
                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: { "ItemArray": ItemArray, "instrument": instrument,  "is_week": is_week, "week_day": week_day, "user_id": user_id, "user_name": user_name,"user_email": user_email, "band_name": band_name, "user_flag": user_flag, "total_price": total_price, "action": 'proccess_booking_room_from_admin' },
                    cache: false,
                    dataType: "json",
                    success: function (response) {
                        $this.prop("disabled", false);

                        $this.html(
                            'Book Room'
                        );
                        if (response.success == 1) {
                            window.location.reload();

                            $('.error').show();
                            $('.error').html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success! </strong><span> Booked Room Successfully.</span> </div> ');
                            
                        } else if (response.success == 2) {
                            $('.error').show();
                            $('.error').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Error! </strong><span>User Already Registerd With Same Email, Please Select From Select Band/User. </span>  </div> ');
                            
                        } else {
                            $('.error').show();
                            $('.error').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Error! </strong><span>You have Already Booked Room With Same Session for This Day. </span>  </div> ');
                        }
                    }
                });
            } else {
                $this.prop("disabled", false);

                $this.html(
                    'Book Room'
                );
                $('.error').show();
                $('.error').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error! </strong><span> Please Select Avilable Room.</span> </div> ');
            }
    });
});
</script>