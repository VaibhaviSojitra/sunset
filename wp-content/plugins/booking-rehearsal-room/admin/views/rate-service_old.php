<?php

if (!defined('ABSPATH')) {

	die();
}

define('SSRB_URL_ADMIN_PAGE', menu_page_url(SSRB_DOMAIN, false));


global $wpdb;
$tablename = SSRB_PREFIX ."rate_service";
$rooms_list = $wpdb->get_results("SELECT * FROM $tablename where is_delete=0 ");

?>
 <form method="post" id="addeditsessiontypeform" >
 <?php wp_nonce_field('addedit_sessiontype','addeditsessiontypeform'); ?>
    <div class="card-body">
        <div class="form-row form-group bookly-js-range-row">
            <div class="col-12 col-lg-12">
                <div class="form-row align-items-center">
                    <div class="col-1">
                        <b>Week Day</b>              
                    </div>
                    <div class="col-4 text-center">
                        <b>Day Session</b>
                    </div>
                    <div class="col-auto">

                    </div>
                    <div class="col-4 text-center">
                       <b>Night Session</b>
                    </div>

                    <div class="col-2 text-center">
                       <b>Price</b>
                    </div>

                </div>
            </div>
        </div>  

        <div class="form-row form-group bookly-js-range-row">
            <div class="col-12 col-lg-12">
                <div class="form-row align-items-center">
                    <div class="col-1">
                        Monday              
                    </div>
                    <div class="col-2">
                        <input class="form-control ui-timepicker-input" name="session[1][starttime]" id="day_starttime" value="<?= !empty($rooms_list[0]) ? $rooms_list[0]->start_time : '' ?>" type="text" placeholder="Start Time" autocomplete="off" required>
                    </div>
                    <div class="col-auto">
                        to                    
                    </div>
                    <div class="col-2">
                        <input class="form-control ui-timepicker-input" name="session[1][endtime]" id="day_endtime" value="<?= !empty($rooms_list[0]) ? $rooms_list[0]->end_time : '' ?>" type="text" placeholder="End Time" autocomplete="off" required>
                    </div>

                    <div class="col-2">
                        <input class="form-control ui-timepicker-input" name="session[1][starttime]" id="day_starttime" value="<?= !empty($rooms_list[0]) ? $rooms_list[0]->start_time : '' ?>" type="text" placeholder="Start Time" autocomplete="off" required>
                    </div>
                    <div class="col-auto">
                        to                    
                    </div>
                    <div class="col-2">
                        <input class="form-control ui-timepicker-input" name="session[1][endtime]" id="day_endtime" value="<?= !empty($rooms_list[0]) ? $rooms_list[0]->end_time : '' ?>" type="text" placeholder="End Time" autocomplete="off" required>
                    </div>

                    <div class="col-1">
                        <input class="form-control ui-timepicker-input" name="session[1][endtime]" id="day_endtime" value="<?= !empty($rooms_list[0]) ? $rooms_list[0]->end_time : '' ?>" type="text" placeholder="Price" autocomplete="off" required>
                    </div>

                </div>
                <input type="hidden" name="session[1][hidden_id]" value="<?= !empty($rooms_list[0]) ? $rooms_list[0]->id : 0 ?>">
            </div>
        </div>  
          
    </div>   
    <div class="card-footer bg-transparent d-flex justify-content-end">
        <input type="hidden" name="action" value="addedit_sessiontype">
        <button type="submit" id="settings-save" class="btn ladda-button btn-success" ><span class="ladda-label">Save</span></button>      
    </div>
</form> 

