<?php
if (! defined('ABSPATH')) {
	die();
}
$id = sanitize_text_field($_GET['ssrbig_id']);
$pg = sanitize_title($_GET['pg']);

global $wpdb;

?>
   
<div class="ssrb-page-header">
    <h1 class="ssrb-heading">General Settings</h1>
</div>

<div id="ssrb_tabs" class="form-row">
    <div id="ssrb-sidebar" class="col-12 col-sm-auto">
        <div class="nav flex-column nav-pills" role="tablist">
            <a class="nav-link mb-2 active" href="#ssrb_settings_session" data-toggle="ssrb-pill">Session Type</a>    
            <a class="nav-link mb-2" href="#ssrb_settings_rate" data-toggle="ssrb-pill">Rate & Service</a>  
        </div>
    </div>
    <div id="ssrb_settings" class="col">
        <div class="card">
            <div class="tab-content">
                <div class="tab-pane active" id="ssrb_settings_session">
                    <?php include 'sesssion-type.php'; ?>
                </div>
                <div class="tab-pane" id="ssrb_settings_rate">
                    <?php include 'rate-service.php'; ?>                                     
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	$(document).ready(function() {
        var ajaxurl = "<?php echo admin_url('admin-ajax.php') ?>";
	});
</script>