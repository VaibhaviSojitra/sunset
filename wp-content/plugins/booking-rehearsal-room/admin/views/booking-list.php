<?php

if (!defined('ABSPATH')) {

	die();
}

define('SSRB_URL_INSTRUMENT', menu_page_url(SSRB_INSTRUMENT, false));


global $wpdb;
$table_instruments = SSRB_PREFIX ."instruments";
$instruments_list = $wpdb->get_results("SELECT * FROM $table_instruments where is_delete=0 ");

?>

<h2 id="settings_ibl_title">Manage Time Slot</h2>

<p>

	<button class="btn btn-info" role="button" data-toggle="modal" data-target="#MdlInstrument">Add New Instrument</button>
</p>


<div>

	<table id="BookingDataTable" class="table table-striped w-100 dataTable no-footer no-border dtr-inline">

		<thead>

			<tr>

				<th>Instrument</th>

				<th>Per Session</th>

				<th>Price</th>

				<th>Image</th>

				<th>Status</th>

				<th>Action</th>

			</tr>

		</thead>

		<tbody>

			<?php 

			foreach ($instruments_list as $k => $list) {
				if ($list->session_type == 1) {
					$session_type = 'Day';
				} else if ($list->session_type == 2) {
					$session_type = 'Night';
				} else {
					$session_type = 'Both';
				}

				if($list->is_active == 1){
					$checked = "checked";
				}else{
					$checked = "";
				}

				if ($list->image) {
					$image = site_url().''.$list->image;
				} else {
					$image = SSRB_URL.'/images/default.jpg';
				}
				?>

				<tr id="<?php echo $list->id; ?>">

					<td><?php echo esc_html($list->instrument_name); ?></td>

					<td><?php echo esc_html($session_type); ?></td>

					<td><?=SSRB_BOOKING_CURRENCY?> <?php echo esc_html($list->price); ?></td>

					<td><img class="instrumentimg" src="<?php echo $image ; ?>" width="70" height="70"></td>

					<td>  <label class="switch">
                        <span class="nss-display">Active</span>
                        <input type="checkbox" class="instrumentStatusChanaged" data-id="<?php echo $list->id; ?>" data-status="<?php echo $list->is_active; ?>" <?php echo esc_html($checked); ?>/>
                        <span class="slider slider-green round"></span>
                        </label></td>

					<td>
						<button class="btn btn-info actionbtn edit_instrument" title="Edit" data-toggle="modal" data-target="#MdlInstrument" data-id="<?php echo $list->id; ?>" >  <i class="fa fa-pencil-square-o"></i></button>
						<button title="Delete" data-id="<?php echo $list->id; ?>" class="btn btn-danger actionbtn instrumentDelete"><span class="fa fa-trash"></span></button>
					</td>

				</tr>

			<?php } ?>

		</tbody>

	</table>

</div>

<input type="hidden" id="site_url" value="<?php echo site_url(); ?>">
<input type="hidden" id="plugin_path" value="<?php echo SSRB_URL; ?>">

<script>
	$(document).ready(function() {
		$("#BookingDataTable").DataTable();
	});
</script>