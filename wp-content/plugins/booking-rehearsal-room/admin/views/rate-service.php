<?php

if (!defined('ABSPATH')) {

	die();
}

define('SSRB_URL_ADMIN_PAGE', menu_page_url(SSRB_DOMAIN, false));


global $wpdb;
$tablename = SSRB_PREFIX ."rate_service";
$rate_service = $wpdb->get_results("SELECT *,group_concat(price) as price,group_concat(session_type ORDER BY session_type ASC) as session_type FROM $tablename where is_delete=0 GROUP BY week_day ORDER BY id ASC");
$session_tablename = SSRB_PREFIX ."session_type";
$session_list = $wpdb->get_results("SELECT * FROM $session_tablename where is_delete=0 ");
// echo "<pre>";
// print_r($rate_service);

?>
 <form method="post" id="frmAddUpdateRateService" >
 <?php wp_nonce_field('AddUpdateRateService','AddUpdateRateService'); ?>
    <div class="card-body">
        <div class="form-row form-group bookly-js-range-row">
            <div class="col-12 col-lg-12">
                <div class="form-row align-items-center">
                    <div class="col-3">
                        <b>Week Day</b>              
                    </div>
                    <?php foreach($session_list as $list){ 
                    if($list->session_type == 1){
                        $txt = 'Day Session Price';
                        $time = '('.$list->start_time."-".$list->end_time.')'; //"Before ".$list->end_time;
                    }elseif($list->session_type == 2){
                        $txt = 'Night Session Price';
                        $time = '('.$list->start_time."-".$list->end_time.')';
                    }
                    ?>
                    <div class="col-3 text-center">
                        <b><?php echo esc_html($txt); ?></b><br/>
                        <b><?php echo esc_html($time); ?></b>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>  
        <?php foreach($rate_service as $value){ 
            $price_arr = explode(',',$value->price);
            $sessiontype_arr = explode(',',$value->session_type);
        ?>
        <div class="form-row form-group bookly-js-range-row">
            <div class="col-12 col-lg-12">
                <div class="form-row align-items-center">
                    <div class="col-3">
                        <?php echo esc_html($value->week_day); ?>             
                    </div>
            
                    <div class="col-3">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><?=SSRB_BOOKING_CURRENCY?></span>
                            </div>
                            <input class="form-control" name="<?php echo esc_html($value->week_day); ?>[<?php echo esc_html($sessiontype_arr[0]); ?>][price]" id="day_price" value="<?php echo esc_html($price_arr[0] ? number_format($price_arr[0],2) : ''); ?>" type="text" placeholder="Enter Price">

                        </div>
                    </div>

                    <div class="col-3">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><?=SSRB_BOOKING_CURRENCY?></span>
                            </div>
                            <input class="form-control" name="<?php echo esc_html($value->week_day); ?>[<?php echo esc_html($sessiontype_arr[1]); ?>][price]" id="night_price" value="<?php echo esc_html($price_arr[1] ? number_format($price_arr[1],2) : ''); ?>" type="text" placeholder="Enter Price">

                        </div>
                    </div>

                </div>
            </div>
        </div>  
        <?php } ?>         
    </div>   
    <div class="card-footer bg-transparent d-flex justify-content-end">
        <input type="hidden" name="action" value="addedit_settings">
        <input type="hidden" name="hiddenflag" value="rate_service">
        <button type="submit" id="settings-save" class="btn ladda-button btn-success" ><span class="ladda-label">Save</span></button>      
    </div>
</form> 

