<?php
if (!defined('ABSPATH')) {
	die();
}
define('SSRB_URL_ADMIN_PAGE', menu_page_url(SSRB_DOMAIN, false));

$id = $_GET['rooms_id'] ? sanitize_text_field($_GET['rooms_id']) : "";
$pg = $_GET['pg'] ? sanitize_title($_GET['pg']) : "";
global $wpdb;
$table_rooms = $wpdb->prefix ."rehearsal_rooms";
if (isset($id) && !empty($id)) {

	$roosData = $wpdb->get_results("SELECT * FROM $table_rooms where id='" . $id . "' AND is_delete=0 ORDER BY id ASC ");
	$roosData = $roosData[0];
	if(empty($roosData)){
		echo '<div class="container-fluid">
		<h3 class="text-center">No Data Found </h3></div>';
		exit();
	}
}
?>
<p style="margin-top: 20px;">
	<a href="<?php echo esc_url(SSRB_URL_ADMIN_PAGE); ?>" title="<?php _e('View List', SSRB_DOMAIN); ?>" class="btn btn-info"><span>Back to List</span></a>
</p>
<input type="hidden" id="site_url" value="<?php echo site_url(); ?>">
<div class="container-fluid">
	<h3 class="text-center"><?php echo esc_html($pg == 'edit' ? "Edit" : "Add")?> Room Time Sessions </h3>
	<hr>
	<div class="row">
		<div class="col-md-2 col-lg-2"></div>
			<div class="col-md-8 col-lg-8 ssrb-card">
				<form method="post" id="frmAddUpdateRooms" enctype="multipart/form-data">

				<section>
						<div class="row">
							<div class="form-group col-md-6 col-sm-4">
								<label for="room_title">Room Title</label>
								<input class="form-control" name="room_title" value="<?php echo esc_html($roosData->room_title ? $roosData->room_title : ''); ?>" type="text" placeholder="Enter title">
							</div>

							<div class="form-group col-md-6 col-sm-4">
								<label for="room_color">Room Color</label>
								<input class="form-control" name="room_color" value="<?php echo esc_html($roosData->room_color ? $roosData->room_color : ''); ?>" type="text" placeholder="Enter Color">
							</div>
							
						</div>
						<hr>
						<h4 class="text-center">Day Sessions Time </h4>
						<div class="row">
							<div class="form-group col-md-6 col-sm-4">
								<label for="day_starttime">Start Sesion Time</label>
								<input class="form-control" name="day_starttime" id="day_starttime" value="<?php echo esc_html($roosData->day_start_time ? $roosData->day_start_time : ''); ?>" type="text" placeholder="Enter Start Time">
							</div>

							<div class="form-group col-md-6 col-sm-4">
								<label for="day_endtime">End Sesion Time</label>
								<input class="form-control" name="day_endtime" id="day_endtime" value="<?php echo esc_html($roosData->day_end_time ? $roosData->day_end_time : ''); ?>" type="text" placeholder="Enter End Time">
							</div>
							
						</div>

						<div class="row">

							<div class="form-group col-md-6 col-sm-4">
								<label for="day_price">Price</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">$</span>
									</div>
									<input class="form-control" name="day_price" id="day_price" value="<?php echo esc_html($roosData->day_price ? number_format($roosData->day_price,2) : ''); ?>" type="text" placeholder="Enter Price">
								</div>
							</div>
						</div>
						<hr>
						<h4 class="text-center">Night Sessions Time </h4>
						<div class="row">
							<div class="form-group col-md-6 col-sm-4">
								<label for="night_starttime">Start Sesion Time</label>
								<input class="form-control" name="night_starttime" id="night_starttime" value="<?php echo esc_html($roosData->night_start_time ? $roosData->night_start_time : ''); ?>" type="text" placeholder="Enter Start Time">
							</div>

							<div class="form-group col-md-6 col-sm-4">
								<label for="night_endtime">End Sesion Time</label>
								<input class="form-control" name="night_endtime" id="night_endtime" value="<?php echo esc_html($roosData->night_end_time ? $roosData->night_end_time : ''); ?>" type="text" placeholder="Enter End Time">
							</div>
							
						</div>
						
						<div class="row">

							<div class="form-group col-md-6 col-sm-4">
								<label for="night_price">Price</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">$</span>
									</div>
									<input class="form-control" name="night_price" id="night_price" value="<?php echo esc_html($roosData->night_price ? number_format($roosData->night_price,2) : ''); ?>" type="text" placeholder="Enter Price">
								</div>
							</div>
						</div>
					</section>
					<hr>
					<section>
						<div class="col-md-12">

							<div style="text-align:right">
								<input type="hidden" id="update" name="hidden_room_id" value="<?= !empty($id) ? $id : 0 ?>">
								<input type="hidden" value="addedit_rooms" name="action">
								<button type="submit" class="btn btn-success" >Submit</button>
							</div>
							
						</div>
					</section>
				</form>
			</div>
		<div class="col-md-2 col-lg-2"></div>
	</div>
</div>

<script>
	$(document).ready(function() {
		var ajaxurl = "<?php echo admin_url('admin-ajax.php') ?>";
		var site_url = $('#site_url').val();
	
		$('#day_starttime').timepicker({listWidth:1,
			step:60});
		$('#day_endtime').timepicker({
			listWidth:1,
			step:60
		});
		var minTime;
		$(document).on( "change", "#day_starttime", function() {
			minTime = $(this).val();
			var startHour = parseInt(minTime.substring(0, 2));
			var startMinutes = minTime.substring(2, 8);
			minTime = (startHour + 1).toString().concat(startMinutes);
			$('#day_endtime').timepicker('option', { 'minTime': minTime,'maxTime': '11:00pm' });
		});

		$('#night_starttime').timepicker({listWidth:1,
			step:60});
		$('#night_endtime').timepicker({
			listWidth:1,
			step:60
		});
		var minTime2;
		$(document).on( "change", "#night_starttime", function() {
			minTime2 = $(this).val();
			var startHour = parseInt(minTime2.substring(0, 2));
			var startMinutes = minTime2.substring(2, 8);
			minTime2 = (startHour + 1).toString().concat(startMinutes);
			$('#night_endtime').timepicker('option', { 'minTime': minTime2,'maxTime': '11:00pm' });
		});

		$(document).on('submit', '#frmAddUpdateRooms', function(event) {
			event.preventDefault();
			
			var formData = new FormData($("#frmAddUpdateRooms")[0]);
			$.ajax({
				type: 'POST',
				url: ajaxurl,
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				dataType:"json",
				success: function(response) {
					if(response.success == 1){
						window.location.href="<?= site_url(); ?>/wp-admin/admin.php?page=<?php echo SSRB_DOMAIN ?>";
					}
				}
			});
		});

		
	});
</script>