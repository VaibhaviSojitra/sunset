<?php

if (!defined('ABSPATH')) {

	die();
}

define('SSRB_URL_ROOMS', menu_page_url(SSRB_ROOMS, false));


global $wpdb;
$table_rooms = SSRB_PREFIX ."rehearsal_rooms";
$rooms_list = $wpdb->get_results("SELECT * FROM $table_rooms where is_delete=0 ");

?>

<h2 id="settings_ibl_title">Manage Time Slot</h2>

<p>

	<a  href="<?php echo esc_url(SSRB_URL_ROOMS); ?>&pg=add" title="<?php _e('Add New Time Slot', SSRB_DOMAIN); ?>" class="btn btn-info"><?php esc_html_e('Add New Time Slot', SSRB_DOMAIN); ?></a>

</p>


<div>

	<table id="roomsDataTable" class="table table-striped w-100 dataTable no-footer no-border dtr-inline">

		<thead>

			<tr>

				<th>Room</th>

				<th>Color Name</th>

				<th>Room Code</th>

				<th>Day Time Slot</th>

				<th>Night Time Slot</th>

				<th>Action</th>

			</tr>

		</thead>

		<tbody>

			<?php $i = 1;

			foreach ($rooms_list as $k => $list) {

				?>

				<tr id="<?php echo $list->id; ?>">

					<td><?php echo esc_html($list->room_title); ?></td>

					<td>

						<?php echo esc_html($list->room_color); ?>

					</td>

					<td>

						<?php echo esc_html($list->room_code); ?>

					</td>

					<td>
						<?php echo esc_html($list->day_start_time).' - '.esc_html($list->day_end_time); ?>
					</td>

					<td>
						<?php echo esc_html($list->night_start_time).' - '.esc_html($list->night_end_time); ?>
					</td>

					<td>
						<a title="Edit" href="<?php echo  esc_url(SSRB_URL_ROOMS); ?>&pg=edit&rooms_id=<?php echo $list->id; ?>" class="btn btn-info actionbtn"><span class="fa fa-pencil-square-o"></span></a>

						<button title="Delete" data-id="<?php echo $list->id; ?>" class="btn btn-danger actionbtn DeleteRoom"><span class="fa fa-trash"></span></button>
					</td>

				</tr>

			<?php }
			unset($i); ?>

		</tbody>

	</table>

</div>
<script>
	$(document).ready(function() {
		$("#roomsDataTable").DataTable();
	});
</script>