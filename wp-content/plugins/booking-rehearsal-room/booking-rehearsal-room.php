<?php
/*
Plugin Name: Sunset Booking Management
Description: Wordpress Plugin for Custom Booking System for Rehearsal Room.
Author: IBL Infotech
Text Domain: booking-rehearsal-room
Author URI: https://iblinfotech.com/
Version: 1.0.0
*/

if (!defined('ABSPATH')) {
	die;
}

if (!defined('SSRB_DOMAIN')) {
	define('SSRB_DOMAIN', 'booking-rehearsal-room');
}
if (!defined('SSRB_PLUGIN_DIR_PATH')) {
	define('SSRB_PLUGIN_DIR_PATH', plugin_dir_path(__FILE__));
}
if (!defined('SSRB_BASENAME')) {
	define('SSRB_BASENAME', plugin_basename(__FILE__));
}
if (!defined('SSRB_PLUGIN_DIR')) {
	define('SSRB_PLUGIN_DIR', __FILE__);
}
if (!defined('SSRB_PREFIX')) {
	define('SSRB_PREFIX', 'ssrb_');
}
define('SSRB_URL', plugins_url(plugin_basename(dirname(__FILE__))));

if (!defined('SSRB_SETTINGS')) {
	define('SSRB_SETTINGS', 'ssrb_');
}

if (!defined('SSRB_ROOMS')) {
	define('SSRB_ROOMS', 'manage-rooms');
}

if (!defined('SSRB_INSTRUMENT')) {
	define('SSRB_INSTRUMENT', 'manage-instrument');
}

if (!defined('SSRB_BOOKING_BAND')) {
	define('SSRB_BOOKING_BAND', 'manage-book-room-for-band');
}

if (!defined('SSRB_BOOKING_LIST')) {
	define('SSRB_BOOKING_LIST', 'manage-booking-list');
}

$options = get_option('wp_stripe_checkout_options');
if(!is_array($options)){
	$currency = $options['stripe_currency_code'];
}else{
	$currency = 'AUD';
}
define('SSRB_BOOKING_CURRENCY', $currency );

if (!class_exists('Activate_SSRB_Plugin')) {

	class Activate_SSRB_Plugin
	{

		public function __construct()
		{
			add_action('admin_enqueue_scripts', array($this, 'custom_ssrb_styles'));
			add_action('admin_init', array($this, 'ssrb_plugin_loaded'));
			register_activation_hook(SSRB_PLUGIN_DIR, array($this, 'ssrb_plugin_activation'));
			register_deactivation_hook(SSRB_PLUGIN_DIR, array($this, 'ssrb_plugin_deactivatation'));
			add_action('admin_menu', array($this,'ssrb_admin_menu') , 10, 2);
			add_action('init','ssrb_register_session');

		}

		function ssrb_register_session(){
			if( !session_id() )
				session_start();
		}

		public function ssrb_admin_menu() {

			add_menu_page( 'Booking Module', 'Booking Module', 'manage_options', SSRB_DOMAIN,'fun_ssrb_settings',  'dashicons-format-audio' ,25); 
			
			add_submenu_page( SSRB_DOMAIN, 'Session and Rates', 'Session and Rates',
				'manage_options', SSRB_DOMAIN,'fun_ssrb_settings');

			add_submenu_page( SSRB_DOMAIN, 'Manage Rooms', 'Manage Rooms',
				'manage_options', 'manage-rooms','fun_ssrb_rooms');

			add_submenu_page( SSRB_DOMAIN, 'Manage Instrument', 'Manage Instrument',
				'manage_options', 'manage-instrument','fun_ssrb_instruments');

			add_submenu_page( SSRB_DOMAIN, 'Book Room for Band', 'Book Room for Band',
				'manage_options', 'manage-book-room-for-band','fun_ssrb_booking_band');

			// add_submenu_page( SSRB_DOMAIN, 'Booking List', 'Booking List',
			// 	'manage_options', 'manage-booking-list','fun_ssrb_booking_list');
	
		}
		// public function ssrb_plugin_page_load() {
		// 	include('admin/setting-main.php');
		// }
		

		public function ssrb_plugin_activation() {
  
			if ( ! current_user_can( 'activate_plugins' ) ) return;
			
			global $wpdb;
			
			if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'room-booking'", 'ARRAY_A' ) ) {
			   
				$current_user = wp_get_current_user();
				
				// create post object
				$page = array(
					'post_title'  => __( 'Room Booking' ),
					'post_status' => 'publish',
					'post_author' => $current_user->ID,
					'post_type'   => 'page',
					'post_content' => '[ssrb_booking]',
				);
				
				// insert the post into the database
				$postId = wp_insert_post( $page );
				update_post_meta($postId, '_wp_page_template', 'template-booking.php' );
			}

			if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'cart'", 'ARRAY_A' ) ) {
			   
				$current_user = wp_get_current_user();
				
				// create post object
				$page = array(
				  'post_title'  => __( 'Cart' ),
				  'post_status' => 'publish',
				  'post_author' => $current_user->ID,
				  'post_type'   => 'page',
				  'post_content' => '[ssrb_cart]',
				);
				
				// insert the post into the database
				$postId = wp_insert_post( $page );
				update_post_meta($postId, '_wp_page_template', 'template-booking.php' );
				
			}

			if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'checkout'", 'ARRAY_A' ) ) {
			   
				$current_user = wp_get_current_user();
				
				// create post object
				$page = array(
				  'post_title'  => __( 'Checkout' ),
				  'post_status' => 'publish',
				  'post_author' => $current_user->ID,
				  'post_type'   => 'page',
				  'post_content' => '[ssrb_checkout]',
				);
				
				// insert the post into the database
				$postId = wp_insert_post( $page );
				update_post_meta($postId, '_wp_page_template', 'template-booking.php' );
				
			}

			if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'credits'", 'ARRAY_A' ) ) {
			   
				$current_user = wp_get_current_user();
				
				// create post object
				$page = array(
				  'post_title'  => __( 'Credits' ),
				  'post_status' => 'publish',
				  'post_author' => $current_user->ID,
				  'post_type'   => 'page',
				  'post_content' => '[ssrb_credits]',
				);
				
				// insert the post into the database
				$postId = wp_insert_post( $page );
				update_post_meta($postId, '_wp_page_template', 'template-booking.php' );
				
			}

			if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'user-dashboard'", 'ARRAY_A' ) ) {
			
				$current_user = wp_get_current_user();
				
				// create post object
				$page = array(
					'post_title'  => __( 'User Dashboard' ),
					'post_status' => 'publish',
					'post_author' => $current_user->ID,
					'post_type'   => 'page',
					'post_content' => '[ssrb_user_dashboard_page]',
				);
				
				// insert the post into the database
				$postId = wp_insert_post( $page );
				update_post_meta($postId, '_wp_page_template', 'template-booking.php' );
			}

			if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'create-band'", 'ARRAY_A' ) ) {
			
				$current_user = wp_get_current_user();
				
				// create post object
				$page = array(
					'post_title'  => __( 'Create Band' ),
					'post_status' => 'publish',
					'post_author' => $current_user->ID,
					'post_type'   => 'page',
					'post_content' => '[ssrb_credits craete_band="1"]',
				);
				
				// insert the post into the database
				$postId = wp_insert_post( $page );
				update_post_meta($postId, '_wp_page_template', 'template-booking.php' );
			}

			if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'update-band'", 'ARRAY_A' ) ) {
			
				$current_user = wp_get_current_user();
				
				// create post object
				$page = array(
					'post_title'  => __( 'Update Band' ),
					'post_status' => 'publish',
					'post_author' => $current_user->ID,
					'post_type'   => 'page',
					'post_content' => '[ssrb_credits update_band="1"]',
				);
				
				// insert the post into the database
				$postId = wp_insert_post( $page );
				update_post_meta($postId, '_wp_page_template', 'template-booking.php' );
			}

			if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'view-band'", 'ARRAY_A' ) ) {
			
				$current_user = wp_get_current_user();
				
				// create post object
				$page = array(
					'post_title'  => __( 'View Band' ),
					'post_status' => 'publish',
					'post_author' => $current_user->ID,
					'post_type'   => 'page',
					'post_content' => '[ssrb_credits view_band="1"]',
				);
				
				// insert the post into the database
				$postId = wp_insert_post( $page );
				update_post_meta($postId, '_wp_page_template', 'template-booking.php' );
			}

			if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'booking-list'", 'ARRAY_A' ) ) {
			
				$current_user = wp_get_current_user();
				
				// create post object
				$page = array(
					'post_title'  => __( 'Booking List' ),
					'post_status' => 'publish',
					'post_author' => $current_user->ID,
					'post_type'   => 'page',
					'post_content' => '[ssrb_credits booking_list="1"]',
				);
				
				// insert the post into the database
				$postId = wp_insert_post( $page );
				update_post_meta($postId, '_wp_page_template', 'template-booking.php' );
			}
			if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'save-card'", 'ARRAY_A' ) ) {
			
				$current_user = wp_get_current_user();
				
				// create post object
				$page = array(
					'post_title'  => __( 'Save Card' ),
					'post_status' => 'publish',
					'post_author' => $current_user->ID,
					'post_type'   => 'page',
					'post_content' => '[ssrb_credits save_card="1"]',
				);
				
				// insert the post into the database
				$postId = wp_insert_post( $page );
				update_post_meta($postId, '_wp_page_template', 'template-booking.php' );
			}

			if(empty(get_option('ssrb_book_page'))){
				add_option( 'ssrb_book_page', 'room-booking' );
			}else{
				update_option( 'ssrb_book_page', 'room-booking' );
			}
			if(empty(get_option('ssrb_cart_page'))){
				add_option( 'ssrb_cart_page', 'cart' );
			}else{
				update_option('ssrb_cart_page', 'cart');
			}
			if(empty(get_option('ssrb_checkout_page'))){
				add_option( 'ssrb_checkout_page', 'checkout' );
			}else{
				update_option( 'ssrb_checkout_page', 'checkout'  );
			}
			if(empty(get_option('ssrb_credits'))){
				add_option( 'ssrb_credits', 'credits' );
			}else{
				update_option( 'ssrb_credits', 'credits'  );
			}
			if(empty(get_option('ssrb_user_dashboard_page'))){
				add_option( 'ssrb_user_dashboard_page', 'user-dashboard' );
			}else{
				update_option( 'ssrb_user_dashboard_page', 'user-dashboard'  );
			}
			if(empty(get_option('ssrb_create_band_page'))){
				add_option( 'ssrb_create_band_page', 'create-band' );
			}else{
				update_option( 'ssrb_create_band_page', 'create-band'  );
			}
			if(empty(get_option('ssrb_update_band_page'))){
				add_option( 'ssrb_update_band_page', 'update-band' );
			}else{
				update_option( 'ssrb_update_band_page', 'update-band'  );
			}
			if(empty(get_option('ssrb_view_band_page'))){
				add_option( 'ssrb_view_band_page', 'view-band' );
			}else{
				update_option( 'ssrb_view_band_page', 'view-band'  );
			}
			if(empty(get_option('ssrb_booking_list_page'))){
				add_option( 'ssrb_booking_list_page', 'booking-list' );
			}else{
				update_option( 'ssrb_booking_list_page', 'booking-list'  );
			}
			if(empty(get_option('ssrb_save_card_page'))){
				add_option( 'ssrb_save_card_page', 'save-card' );
			}else{
				update_option( 'ssrb_save_card_page', 'save-card'  );
			}
		}

		public function ssrb_plugin_deactivatation()
		{
			if (is_admin())
				flush_rewrite_rules();
				delete_option( 'ssrb_book_page');
				delete_option( 'ssrb_cart_page');
				delete_option( 'ssrb_checkout_page');
				delete_option( 'ssrb_credits');
				delete_option( 'ssrb_user_dashboard_page');
				delete_option( 'ssrb_create_band_page');
				delete_option( 'ssrb_update_band_page');
				delete_option( 'ssrb_view_band_page');
				delete_option( 'ssrb_booking_list_page');
				delete_option( 'ssrb_save_card_page');
		}

		public function custom_ssrb_styles()
		{
			if(sanitize_key($_GET['page']) == SSRB_DOMAIN || sanitize_key($_GET['page']) == SSRB_ROOMS || sanitize_key($_GET['page']) == SSRB_INSTRUMENT || sanitize_key($_GET['page']) == SSRB_BOOKING_LIST)
			{
				wp_enqueue_style('ssrb-custom-admin-css', plugins_url('/css/ssrb-admin-style.css', __FILE__));
				wp_enqueue_style('ssrb-bootstrap-select-css', plugins_url('/css/bootstrap-select.min.css', __FILE__), array(), '');
				wp_enqueue_style('ssrb-bootstrap-min-css', plugins_url('/css/bootstrap.min.css', __FILE__), array(), '');
				wp_enqueue_style('ssrb-datatable-bootstrap-min-css', plugins_url('/css/jquery.dataTables.min.css', __FILE__), array(), '');
				wp_enqueue_style('ssrb-font-icon-awesome-css', plugins_url('/css/font-awesome.min.css', __FILE__));
				wp_enqueue_style('ssrb-bootstrap-toggle-min-css', plugins_url('/css/bootstrap-toggle.min.css', __FILE__));
				wp_enqueue_style('ssrb-jquery-timepicker-min-css', plugins_url('/css/jquery.timepicker.min.css', __FILE__));


				wp_enqueue_script('ssrb-jquery-min-js', plugins_url('/js/jquery.min.js', __FILE__), array('jquery'), false);
				wp_enqueue_script('ssrb-bootstrap-js', plugins_url('/js/bootstrap.min.js', __FILE__), array('jquery'), false);	
				wp_enqueue_script('ssrb-jquery-datatable-min-js', plugins_url('/js/jquery.dataTables.min.js', __FILE__), array('jquery'), false);
				wp_enqueue_script('ssrb-jquery-validate-js', plugins_url('/js/jquery.validate.js', __FILE__), array('jquery'), false);
				wp_enqueue_script('ssrb-bootstrap-select-js', plugins_url('/js/bootstrap-select.min.js', __FILE__), array('jquery'), false);
				wp_enqueue_script('ssrb-jquery-timepicker-min-js', plugins_url('/js/jquery.timepicker.min.js', __FILE__), array('jquery'), false);
				wp_enqueue_script('ssrb-jquery-notify-min-js', plugins_url('/js/notify.min.js', __FILE__), array('jquery'), false);
				wp_enqueue_script('ssrb-jquery-fnStandingRedraw-js', plugins_url('/js/fnStandingRedraw.js', __FILE__), array('jquery'), false);
				
				wp_enqueue_script('ssrb-admin-js', plugins_url('/js/ssrb-admin.js', __FILE__));
				wp_enqueue_media();

			}

			if(sanitize_key($_GET['page']) == SSRB_BOOKING_BAND){
				
				$style = 'bootstrap';
				if( ( ! wp_style_is( $style, 'queue' ) ) && ( ! wp_style_is( $style, 'done' ) ) ) {
					wp_enqueue_style( $style, SSRB_URL.'/css/bootstrap.min.css');
				}
				$style = 'font-awesome';
				if( ( ! wp_style_is( $style, 'queue' ) ) && ( ! wp_style_is( $style, 'done' ) ) ) {
					wp_enqueue_style( $style, SSRB_URL.'/css/font-awesome.min.css');
				}
				$style = 'bootstrap-datepicker';
				if( ( ! wp_style_is( $style, 'queue' ) ) && ( ! wp_style_is( $style, 'done' ) ) ) {
					wp_enqueue_style( $style, SSRB_URL.'/css/bootstrap-datepicker.min.css');
				}
				wp_enqueue_style( 'jquery-ui-theme-css', SSRB_URL.'/css/jquery.ui.theme.css');
				wp_enqueue_style( 'jquery-ui-css', SSRB_URL.'/css/jquery-ui.min.css');
				wp_enqueue_style('ssrb-custom-admin-css', plugins_url('/css/ssrb-admin-style.css', __FILE__));

				wp_enqueue_script('jquery');
				
				$js = 'jquery-ui.min';
				if( ( ! wp_script_is( $js, 'queue' ) ) && ( ! wp_script_is( $js, 'done' ) ) ) {
					wp_enqueue_script( $js, SSRB_URL.'/js/jquery-ui.min.js');
				}
				$js = 'bootstrap';
				if( ( ! wp_script_is( $js, 'queue' ) ) && ( ! wp_script_is( $js, 'done' ) ) ) {
					wp_enqueue_script( $js, SSRB_URL.'/js/bootstrap.min.js');
				}
				wp_enqueue_script('ssrb-jquery-notify-min-js', plugins_url('/js/notify.min.js', __FILE__), array('jquery'), false);
				wp_enqueue_script( 'jquery-ui-datepicker' );
			}
		}
	}
}
require_once (SSRB_PLUGIN_DIR_PATH . 'class-admin-init.php');
require_once (SSRB_PLUGIN_DIR_PATH . 'class-user-init.php');
require_once (SSRB_PLUGIN_DIR_PATH . 'include/manage-admin-ajax.php');
require_once (SSRB_PLUGIN_DIR_PATH . 'include/manage-users-ajax.php');
new Activate_SSRB_Plugin();
new SSRB_USER_INIT();
new SSRB_ADMIN_INIT();
new SSRB_ADMIN_AJAX_INIT();
new SSRB_USER_AJAX_INIT();

?>