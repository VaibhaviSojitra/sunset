<?php

if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

/********************************************
PLUGIN SHORTCODE
**********************************************/
/**
 *
 */
class ssrb_booking_shortcode {
    public function __construct() {
        add_shortcode('ssrb_booking', array(
            $this,
            'ssrb_booking_shortcode'
        ));

        add_shortcode('ssrb_cart', array(
            $this,
            'ssrb_cart_shortcode'
        ));

        add_shortcode('ssrb_checkout', array(
            $this,
            'ssrb_checkout_shortcode'
        ));
        
        add_shortcode('ssrb_credits', array(
            $this,
            'ssrb_credits_shortcode'
        ));
    }

    public function ssrb_booking_shortcode() {
        if(!is_admin()) {
            global $wpdb;
            if (!session_id()) {
                session_start();
            }
            $table_rooms = SSRB_PREFIX ."rehearsal_rooms";
            $rooms_list = $wpdb->get_results("SELECT * FROM $table_rooms where is_delete=0 ");

            $table_rates = SSRB_PREFIX ."rate_service";
            $rates_list = $wpdb->get_results("SELECT * FROM $table_rates where is_delete=0 ");

            $times1 = $this->create_time_range('7:00', '11:00', '1 hours');
            $times2 = $this->create_time_range('12:00', '24:00', '1 hours');
            $times3 = $this->create_time_range('1:00', '2:00', '1 hours');
        
            $times = array_merge($times1,$times2,$times3);

            $cart_page_url = get_option('ssrb_cart_page',true);

            if(isset($_GET['date']))
            {
                $date=$_GET['date'];
                $current_day = date('l',strtotime($date));     
            }else{
                $current_day = date('l');      
                $date = date('Y-m-d');
            }
            $session_type = [];
            $day_time = [];
            $night_time = [];
            $price = [];
            foreach ($rates_list as $k => $rate) {
            
                if($current_day == $rate->week_day){
                    if($rate->session_type == 1){
                        $price[0] = $rate->price;
                        $session_type[0] = $rate->session_type;
                    }else 
                    if($rate->session_type == 2){
                        $price[1] = $rate->price;
                        $session_type[1] = $rate->session_type;
                    }
                    
                }
            }

            $user_id = get_current_user_id();
        
            foreach($rooms_list as $key => $rval){
                $rooms_list[$key] = $rval;
                $day_time =  $this->create_time_range(date('H:i', strtotime($rval->day_start_time)), date('H:i', strtotime($rval->day_end_time) ), '1 hours');

                $night_time =  $this->create_time_range(date('H:i', strtotime($rval->night_start_time)), date('H:i', strtotime($rval->night_end_time) + 60*60 ), '1 hours');

                $startday_time['time'] = array($day_time[0], $night_time[0]);
                $startday_time['price'] = $price;
                $startday_time['session_type'] = $session_type;
                $endday_time = array(end($day_time), end($night_time));

                $roomsData[$rval->id]['start_day'] = $startday_time;
                $roomsData[$rval->id]['end_day'] = $endday_time;

            }
            include 'users/user-style.php'; 

            $table_instruments = SSRB_PREFIX ."instruments";
            $instruments_list = $wpdb->get_results("SELECT * FROM $table_instruments where is_active = 1 AND is_delete = 0 ");
            // print_r($instruments_list);
            // die;
            if(is_user_logged_in()){
             include 'users/booking_template.php'; 
            }else{
                wp_redirect( home_url() );
                exit;
            }
            // return $result;
        }
     }

     public function setDataArray($data, $rooms)
    {
        $newBookings = [];

        $x = 0;
        foreach ($rooms as $key) {
            $newBookings[$x] = array(
                'id' => $key->id, 'Bookings' => []
            );
            
            $y = 0;
            foreach ($data as $value) {
                if ($value->room_id == $newBookings[$x]['id']) {
                    $newBookings[$x]['Bookings'][$y] = [
                        'room_Name' => $value
                    ];
                    $y++;
                }
            }
            $x++;
        }
        return ($newBookings);

    }
    public function create_time_range($start, $end, $interval = '30 mins', $format = '12') {

        $startTime = strtotime($start); 
        $endTime   = strtotime($end);
        $returnTimeFormat = ($format == '12')?'g:ia':'G:ia';
    
        $current   = time(); 
        $addTime   = strtotime('+'.$interval, $current); 
        $diff      = $addTime - $current;
    
        $times = array(); 
        while ($startTime < $endTime) { 
            $times[] = date($returnTimeFormat, $startTime); 
            $startTime += $diff; 
        } 
        $times[] = date($returnTimeFormat, $startTime); 
        return $times; 
    }

    public function ssrb_cart_shortcode() {
        if(!is_admin()) {
            global $wpdb;
            if (!session_id()) {
                session_start();
            }
            $table_cart = SSRB_PREFIX ."cart";
            $table_rooms = SSRB_PREFIX ."rehearsal_rooms";
            $table_instruments = SSRB_PREFIX ."instruments";
            $user_id = get_current_user_id();
        
            $cart_list = $wpdb->get_results("SELECT c.*,r.day_start_time,r.night_start_time,i.instrument_name,i.price as i_price,i.session_type as i_session_type FROM $table_cart as `c` JOIN $table_rooms as `r` ON `r`.`id`=`c`.room_id LEFT JOIN $table_instruments as `i` ON `i`.`id`=`c`.instrument_id where c.user_id = $user_id AND c.status = 1 AND  c.is_delete = 0 ");

            $book_page_url = get_option('ssrb_book_page',true);
            $checkout_page_url = get_option('ssrb_checkout_page',true);

            // echo "<pre>";
            // print_r($cart_list);
            include 'users/user-style.php'; 
            if(is_user_logged_in()){
                include 'users/cart_template.php'; 
            }else{
                wp_redirect( home_url() );
                exit;
            }
        }
     }

     
    public function ssrb_checkout_shortcode() {
        if(!is_admin()) {
            global $wpdb;
            $table_cart = SSRB_PREFIX ."cart";
            $table_rooms = SSRB_PREFIX ."rehearsal_rooms";
            $table_instruments = SSRB_PREFIX ."instruments";

            $user_id = get_current_user_id();
            $band_name = get_user_meta( $user_id,'band_name',true);
          
             $cart_list = $wpdb->get_results("SELECT c.*,r.day_start_time,r.night_start_time,i.instrument_name,i.price as i_price,i.session_type as i_session_type FROM $table_cart as `c` JOIN $table_rooms as `r` ON `r`.`id`=`c`.room_id LEFT JOIN $table_instruments as `i` ON `i`.`id`=`c`.instrument_id where c.user_id = $user_id AND c.status = 2 AND  c.is_delete = 0 ");            

            $book_page_url = get_option('ssrb_book_page',true);
            $band_page_url = get_option('ssrb_credits',true);
            $booking_list_page = get_option( 'ssrb_booking_list_page', true );
            
            $user = get_userdata( $user_id );
           
            $user_info = get_user_meta( $user_id,'contact_no');

            if(!empty(get_user_meta($user_id, 'ssrb_wallet'))){
                $wallet = get_user_meta($user_id, 'ssrb_wallet', true);
            }else{
                $wallet = 0;
            }
            
            include 'users/user-style.php'; 
            if(is_user_logged_in()){
                include 'users/checkout_template.php'; 
            }else{
                wp_redirect( home_url() );
                exit;
            }
        }
     }
     
     public function ssrb_credits_shortcode($atts) {
        if(!is_admin()) {
            global $wpdb;
            if(is_user_logged_in()){
                $user_id = get_current_user_id();
                $link_active_color = get_option('woo_link_hover_color',true);
                include 'users/user-style.php'; 

                $url = SSRB_URL.'/include/country.json'; 
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_URL,$url);
                $result=curl_exec($ch);
                curl_close($ch);
                $country_list = json_decode($result, true);
                $credits =  get_option( 'ssrb_credits', true );
                $create_band_page = get_option( 'ssrb_create_band_page', true );
                $update_band_page = get_option( 'ssrb_update_band_page', true );
                $view_band_page = get_option( 'ssrb_view_band_page', true );
                $booking_list_page = get_option( 'ssrb_booking_list_page', true );
                $save_card_page = get_option( 'ssrb_save_card_page', true );
                
                if(!empty(get_user_meta($user_id, 'ssrb_wallet'))){
                    $wallet = get_user_meta($user_id, 'ssrb_wallet', true);
                }else{
                    $wallet = 0;
                }
            
                if(empty(get_user_meta($user_id, 'is_band'))){ 
                    $bandtext = "Create Band"; 
                    $bandurl = "create-band"; 
                }else {
                    $bandtext = "Update Band";
                    $bandurl = "update-band";
                }

                $table_booking_details = SSRB_PREFIX ."booking_details";
                $table_rooms = SSRB_PREFIX ."rehearsal_rooms";
                $table_instruments = SSRB_PREFIX ."instruments";

                $booking_list = $wpdb->get_results("SELECT b.*,r.room_title,r.day_start_time,r.night_start_time,i.instrument_name,i.price as i_price FROM $table_booking_details as `b` JOIN $table_rooms as `r` ON `r`.`id`=`b`.room_id LEFT JOIN $table_instruments as `i` ON `i`.`id`=`b`.instrument_id where b.user_id = $user_id AND b.is_delete = 0 ORDER BY b.id DESC "); 
              
                $today = date('Y-m-d');

                $current_booking_list = $wpdb->get_results("SELECT b.*,r.room_title,(CASE b.session_type WHEN 1 THEN CONCAT(r.day_start_time, '-',r.day_end_time) WHEN 2 THEN CONCAT(r.night_start_time, '-',r.night_end_time) ELSE '-' END) as session_time FROM $table_booking_details as `b` JOIN $table_rooms as `r` ON `r`.`id`=`b`.room_id where DATE(b.booking_date) = '".$today."' AND b.user_id = $user_id AND b.is_delete = 0 ORDER BY b.id DESC"); 
            

                if (!empty($atts) && ($atts['craete_band']==1 || $atts['update_band']==1 )) {
                    include 'users/create_band_template.php'; 
                }else if (!empty($atts) && $atts['view_band']==1) {
                    include 'users/view_band_template.php'; 
                }else if (!empty($atts) && $atts['booking_list']==1) {
                    include 'users/booking_list_template.php'; 
                }else{
                    include 'users/credits_template.php'; 
                } 
            }else{
                wp_redirect( home_url() );
                exit;
            }
        }
     }

}
new ssrb_booking_shortcode();
?>