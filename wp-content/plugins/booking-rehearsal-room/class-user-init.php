<?php

if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}
class SSRB_USER_INIT {

    public function __construct() {
      
        add_action('show_user_profile', array($this,'custom_user_profile_fields'));
        add_action('edit_user_profile', array($this,'custom_user_profile_fields'));
        add_action( 'personal_options_update', array($this,'update_extra_profile_fields') );
        add_action( 'edit_user_profile_update', array($this,'update_extra_profile_fields') );
        add_action( 'init', array( $this, 'fun_load_init' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'fun_users_script' ) );

    }

    public function fun_users_script()
    {
        $checkout_pag =  get_option( 'ssrb_checkout_page',true );
        $cart_page = get_option( 'ssrb_cart_page', true );       
        $user_dashboard_page =  get_option( 'ssrb_user_dashboard_page', true );
        $book_page = get_option( 'ssrb_book_page', true );
        $credits =  get_option( 'ssrb_credits', true );
        $create_band_page = get_option( 'ssrb_create_band_page', true );
        $update_band_page = get_option( 'ssrb_update_band_page', true );
        $view_band_page = get_option( 'ssrb_view_band_page', true );
        $booking_list_page = get_option( 'ssrb_booking_list_page', true );
        $save_card_page = get_option( 'ssrb_save_card_page', true );

        if(is_page($checkout_pag) || is_page($cart_page) || is_page($credits) || is_page($user_dashboard_page) || is_page($book_page) || is_page($create_band_page) || is_page($update_band_page) || is_page($view_band_page) || is_page($booking_list_page) || is_page($save_card_page)) {

            $style = 'bootstrap';
            if( ( ! wp_style_is( $style, 'queue' ) ) && ( ! wp_style_is( $style, 'done' ) ) ) {
                wp_enqueue_style( $style, SSRB_URL.'/css/bootstrap.min.css');
            }
            $style = 'font-awesome';
            if( ( ! wp_style_is( $style, 'queue' ) ) && ( ! wp_style_is( $style, 'done' ) ) ) {
                wp_enqueue_style( $style, SSRB_URL.'/css/font-awesome.min.css');
            }
            $style = 'bootstrap-datepicker';
            if( ( ! wp_style_is( $style, 'queue' ) ) && ( ! wp_style_is( $style, 'done' ) ) ) {
                wp_enqueue_style( $style, SSRB_URL.'/css/bootstrap-datepicker.min.css');
            }
            wp_enqueue_style( 'jquery-ui-theme-css', SSRB_URL.'/css/jquery.ui.theme.css');
            wp_enqueue_style( 'jquery-ui-css', SSRB_URL.'/css/jquery-ui.min.css');

            wp_enqueue_script('jquery');
            
            $js = 'jquery-ui.min';
            if( ( ! wp_script_is( $js, 'queue' ) ) && ( ! wp_script_is( $js, 'done' ) ) ) {
                wp_enqueue_script( $js, SSRB_URL.'/js/jquery-ui.min.js');
            }
            $js = 'bootstrap';
            if( ( ! wp_script_is( $js, 'queue' ) ) && ( ! wp_script_is( $js, 'done' ) ) ) {
                wp_enqueue_script( $js, SSRB_URL.'/js/bootstrap.min.js');
            }
            wp_enqueue_script('ssrb-jquery-notify-min-js', plugins_url('/js/notify.min.js', __FILE__), array('jquery'), false);
            wp_enqueue_script( 'jquery-ui-datepicker' );
        }
    }

    public function fun_load_init(){
        if(!is_admin()){
            global $wpdb;
            $table_cart = SSRB_PREFIX ."cart";
            $user_id = get_current_user_id();
            $table_rooms = SSRB_PREFIX ."rehearsal_rooms";

            $cart_list = $wpdb->get_results("SELECT c.*,r.day_start_time,r.night_start_time FROM $table_cart as `c` JOIN $table_rooms as `r` ON `r`.`id`=`c`.room_id where c.is_delete = 0 ");
            
            foreach($cart_list as $value){ 
                $time = time();                
                $diff =  $time  - $value->session_expire_time;

                // echo "<br/>===";
                // echo  time();
                // echo "<br/>";
                // echo $value->session_expire_time;
                // echo '------'.$diff;
                if ($diff > (15*60) || empty( $value->session_expire_time))
                { 
                    // echo "expired";
                    $expire_time = '00-00';
                    $result = 	$wpdb->update(
                        $table_cart,    
                        array(
                            'is_delete' => 1,
                            'expire_time' => $expire_time,
                            'session_expire_time' => 0,
                        ),
                        array('id' => $value->id)
                    );
                }else{
                    $time2 = new DateTime(date('Y-m-d h:i:s'));
                    $time1 = new DateTime($value->booking_end_time);
                    $timediff = $time2->diff($time1);
                    $second = $timediff->s;
                    $minute = $timediff->i;
                    if($second<=0){
                        $second = '00';
                    }
                    if($minute<=0){
                        $second = '00';
                    }
                    $expire_time = $minute.'-'.$second;
                    $result = 	$wpdb->update(
                        $table_cart,    
                        array(
                            // 'session_expire_time' => time(),
                            'expire_time' => $expire_time,
                        ),
                        array('id' => $value->id)
                    );
                    // $time = time();
                    // $session_array[$value->id] = $time;
                    // $_SESSION['data'] = $session_array;
                }
            }
        }
    }

    public function custom_user_profile_fields( $user ) {
        if ( in_array( 'subscriber', (array) $user->roles ) ) {
            $url = SSRB_URL.'/include/country.json'; 
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL,$url);
            $result=curl_exec($ch);
            curl_close($ch);
            $country_list = json_decode($result, true);

            ?>
            <h2 id="wordpress-band">Band Details</h2>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="code"><?php _e( 'Band Name' ); ?></label>
                    </th>
                    <td>
                        <input type="text" name="band_name" id="band_name" value="<?php echo esc_attr( get_the_author_meta( 'band_name', $user->ID ) ); ?>" class="regular-text" />
                    </td>
                </tr>

                <tr>
                    <th>
                        <label for="code"><?php _e( 'Description' ); ?></label>
                    </th>
                    <td>
                        <input type="text" name="band_description" id="band_description" value="<?php echo esc_attr( get_the_author_meta( 'band_description', $user->ID ) ); ?>" class="regular-text" />
                    </td>
                </tr>

                <tr>
                    <th>
                        <label for="code"><?php _e( 'Contact no' ); ?></label>
                    </th>
                    <td>
                        <input type="text" name="contact_no" id="contact_no" value="<?php echo esc_attr( get_the_author_meta( 'contact_no', $user->ID ) ); ?>" class="regular-text" />
                    </td>
                </tr>

                <tr>
                    <th>
                        <label for="code"><?php _e( 'Address' ); ?></label>
                    </th>
                    <td>
                        <input type="text" name="address" id="address" value="<?php echo esc_attr( get_the_author_meta( 'address', $user->ID ) ); ?>" class="regular-text" />
                    </td>
                </tr>

                <tr>
                    <th>
                        <label for="code"><?php _e( 'City' ); ?></label>
                    </th>
                    <td>
                        <input type="text" name="city" id="city" value="<?php echo esc_attr( get_the_author_meta( 'city', $user->ID ) ); ?>" class="regular-text" />
                    </td>
                </tr>

                <tr>
                    <th>
                        <label for="code"><?php _e( 'State' ); ?></label>
                    </th>
                    <td>
                        <input type="text" name="state" id="state" value="<?php echo esc_attr( get_the_author_meta( 'state', $user->ID ) ); ?>" class="regular-text" />
                    </td>
                </tr>

                <tr>
                    <th>
                        <label for="code"><?php _e( 'Postal Code' ); ?></label>
                    </th>
                    <td>
                        <input type="text" name="postal_code" id="postal_code" value="<?php echo esc_attr( get_the_author_meta( 'postal_code', $user->ID ) ); ?>" class="regular-text" />
                    </td>
                </tr>

                <tr>
                    <th>
                        <label for="code"><?php _e( 'Country' ); ?></label>
                    </th>
                    <td>
                        <select type="text" class="regular-text" name="country" id="country" required>
                            <?php foreach($country_list as $value){ 
                                if($value['code'] == get_the_author_meta( 'country', $user->ID )){
                                    $selected = 'selected';
                                }else{ 
                                    $selected = '';   
                                }?>
                                <option value="<?php echo $value['code'] ?>" <?php echo $selected; ?>><?php echo $value['name'] ?></option>
                            <?php } ?>                                    
                        </select>
                    </td>
                </tr>
            </table>
        <?php
        }
    }
    public function update_extra_profile_fields( $user_id ) {
        if ( current_user_can( 'edit_user', $user_id ) )
            update_user_meta( $user_id, 'band_name', $_POST['band_name'] );
            update_user_meta( $user_id, 'contact_no', $_POST['contact_no'] );
            update_user_meta( $user_id, 'band_description', $_POST['band_description'] );
            update_user_meta( $user_id, 'address', $_POST['address'] );
            update_user_meta( $user_id, 'city', $_POST['city'] );
            update_user_meta( $user_id, 'state', $_POST['state'] );
            update_user_meta( $user_id, 'postal_code', $_POST['postal_code'] );
            update_user_meta( $user_id, 'country', $_POST['country'] );
    }

}
