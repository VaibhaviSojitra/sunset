$(document).ready(function () {

    $(document).on("click", "#ssrb-sidebar div.nav a", function (e) {
        e.preventDefault();
        console.log(1);
        $('#ssrb-sidebar div.nav .nav-link.active').removeClass('active');
        $('#ssrb_settings .tab-pane.active').removeClass('active');
        var $parent = $(this);
        $parent.addClass('active');
        var $href = $(this).attr('href');
        $($href).addClass('active');
    });
    if ($('#day_starttime').length > 0) {
        $('#day_starttime').timepicker({
            listWidth: 1,
            step: 60
        });
        $('#day_endtime').timepicker({
            listWidth: 1,
            step: 60
        });
    }
    var minTime;
    $(document).on("change", "#day_starttime", function () {
        minTime = $(this).val();
        var startHour = parseInt(minTime.substring(0, 2));
        var startMinutes = minTime.substring(2, 8);
        minTime = (startHour + 1).toString().concat(startMinutes);
        $('#day_endtime').timepicker('option', { 'minTime': minTime, 'maxTime': '11:00pm' });
    });
    if ($('#night_starttime').length > 0) {
        $('#night_starttime').timepicker({
            listWidth: 1,
            step: 60
        });

        $('#night_endtime').timepicker({
            listWidth: 1,
            step: 60
        });
    }
    var minTime2;
    $(document).on("change", "#night_starttime", function () {
        minTime2 = $(this).val();
        var startHour = parseInt(minTime2.substring(0, 2));
        var startMinutes = minTime2.substring(2, 8);
        minTime2 = (startHour + 1).toString().concat(startMinutes);
        $('#night_endtime').timepicker('option', { 'minTime': minTime2, 'maxTime': '11:00pm' });
    });

    $(document).on('submit', '#frmAddUpdateSessionType', function (event) {
        event.preventDefault();
        $("#settings-save").prop("disabled", true);

        $("#settings-save").html(
            '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...'
        );
        var formData = new FormData($("#frmAddUpdateSessionType")[0]);
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (response) {
                $("#settings-save").prop("disabled", false);
                $("#settings-save").html('Save');
                if (response.success == 1) {
                    $.notify("Successfully Update Data", "success");
                } else {
                    $.notify("Error While Update Data", "error");
                }
            }
        });
    });


    $(document).on('submit', '#frmAddUpdateRateService', function (event) {
        event.preventDefault();
        $("#settings-save").prop("disabled", true);

        $("#settings-save").html(
            '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...'
        );
        var formData = new FormData($("#frmAddUpdateRateService")[0]);
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (response) {
                $("#settings-save").prop("disabled", false);
                $("#settings-save").html('Save');
                if (response.success == 1) {
                    $.notify("Successfully Update Data", "success");
                } else {
                    $.notify("Error While Update Data", "error");
                }
            }
        });
    });

    $(document).on('submit', '#frmAddUpdateRooms', function (event) {
        event.preventDefault();
        $('.error').text("");
        $("#save_rooms").prop("disabled", true);

        $("#save_rooms").html(
            '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...'
        );
        var page_url = $('#page_url').val();
        var formData = new FormData($("#frmAddUpdateRooms")[0]);
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (response) {
                if (response.flag == 1) {
                    $('.error').text("Room Title or Room Code Already Exist").fadeOut(10000);
                    // $.notify("Room Title Already Exist", "error");
                    $("#save_rooms").prop("disabled", false);

                    $("#save_rooms").html(
                        'Save'
                    );
                } else {
                    if (response.success == 1) {
                        window.location.href = page_url;
                    } else {
                        $.notify("Error While Update Data", "error");
                        $("#save_rooms").prop("disabled", false);

                        $("#save_rooms").html(
                            'Save'
                        );
                    }
                }
            }
        });
    });

    $(document).on('click', '.DeleteRoom', function (event) {
        event.preventDefault();
        var id = $(this).attr('data-id');
        var that = $(this);
        var table = $('#roomsDataTable').DataTable();
        var info = table.page.info();
        var currentpage = info.start;
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: { 'id': id, 'action': 'delete_rooms' },
            cache: false,
            dataType: "json",
            success: function (response) {
                if (response.success == 1) {
                    table.row(that.parents('tr')).remove().draw();
                    $.notify("Successfully Delete Data", "success");
                } else {
                    $.notify("Error While Delete Data", "error");
                }
            }
        });
    });

    $(document).on('click', '#upload_instrument_image', function (e) {
        var site_url = $('#site_url').val();
        var mediaUploader;

        e.preventDefault();
        // If the uploader object has already been created, reopen the dialog
        if (mediaUploader) {
            mediaUploader.open();
            return;
        }
        // Extend the wp.media obase_urlbject
        mediaUploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            multiple: false
        });

        // When a file is selected, grab the URL and set it as the text field's value
        mediaUploader.on('select', function () {
            var attachment = mediaUploader.state().get('selection').first().toJSON();
            var attachment_url = (attachment.url).split(site_url).pop();
            $('#instrument_image_path').val(attachment_url);
            $('.img_preview_div').show();
            $('.img_preview').attr('src', attachment.url);
        });
        // Open the uploader dialog
        mediaUploader.open();
    });

    $(document).on('click', '#remove_instrument_image', function (e) {
        e.preventDefault();
        $('#instrument_image_path').val('');
        $('.img_preview_div').hide();
    });


    $('#MdlInstrument').on('hidden.bs.modal', function (e) {
        $("#frmAddUpdateInstrument")[0].reset();
        $('.img_preview_div').hide();
        $("#save_instrument").prop("disabled", false);

        $("#save_instrument").html(
            'Save'
        );
    });

    $(document).on('click', '.edit_instrument', function (event) {
        event.preventDefault();
        var id = $(this).attr('data-id');
        var site_url = $('#site_url').val();
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: { "id": id, 'action': 'get_instrument_databyid' },
            cache: false,
            dataType: "json",
            success: function (response) {

                if (response.success == 1) {
                    var data = response.data[0];
                    $('.mdltitle').text('Update New Instrument');
                    $('#instrument_name').val(data.instrument_name);
                    $('#session_type').val(data.session_type);
                    $('#price').val(data.price);
                    if (data.image != '') {
                        var image = site_url + data.image;
                        $('.img_preview_div').attr('style', 'display:block;');
                        $('#img_preview').attr('src', image);
                        $('#instrument_image_path').val(data.image);
                    } else {
                        $('.img_preview_div').attr('style', 'display:none;');
                        $('#img_preview').attr('src', "");
                        $('#instrument_image_path').val("");
                    }

                    $('#hidden_id').val(data.id);
                    setTimeout(function () {
                        $("#MdlInstrument").modal('show');
                    }, 1000);
                } else {
                    $.notify("No Data Found", "error");
                }
            }
        });
    });

    $(document).on('submit', '#frmAddUpdateInstrument', function (event) {
        event.preventDefault();
        $('.error').text("");
        $("#save_instrument").prop("disabled", true);

        $("#save_instrument").html(
            '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...'
        );
        var site_url = $('#site_url').val();
        var plugin_path = $('#plugin_path').val();
        var table = $('#instrumentDataTable').DataTable();
        var info = table.page.info();
        var currentpage = info.start;
        var formData = new FormData($("#frmAddUpdateInstrument")[0]);
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (response) {

                if (response.flag == 3) {
                    $('.error').text("Instrument Name Already Exist").fadeOut(10000);
                    // $.notify("Instrument Name Already Exist", "error");
                    $("#save_instrument").prop("disabled", false);

                    $("#save_instrument").html(
                        'Save'
                    );
                } else {
                    $('#MdlInstrument').modal('hide');

                    if (response.success == 1) {
                        var data = response.data[0];

                        var message = "Add";
                        if (response.flag == 2) {
                            $('#instrumentDataTable').dataTable().fnDeleteRow($("#" + data.id));
                            var message = "Update";
                        }
                        if (data.image) {
                            var image = site_url + data.image;
                        } else {
                            var image = plugin_path + '/images/default.jpg';
                        }

                        if (data.session_type == 1) {
                            var session_type = 'Day';
                        } else if (data.session_type == 2) {
                            var session_type = 'Night';
                        } else {
                            var session_type = 'Both';
                        }

                        if (data.is_active == 1) {
                            var checked = 'checked';
                        } else {
                            var checked = '';
                        }
                        newRow = $('#instrumentDataTable').dataTable().fnAddData([
                            data.instrument_name,
                            session_type,
                            data.price,
                            ' <img class="instrumentimg" src="' + image + '" width="70" height="70">',
                            '<label class="switch"><span class="nss-display">Active</span> <input type="checkbox" class="instrumentStatusChanaged" data-id="' + data.id + '" data-status="' + data.is_active + '" ' + checked + ' /><span class="slider slider-green round"></span></label>',
                            '<button class="btn btn-info actionbtn edit_instrument" title="Edit" data-toggle="modal" data-target="#MdlInstrument" data-id="' + data.id + '"> <i class="fa fa-pencil-square-o"></i> </button> <button title="Delete" data-id="' + data.id + '" class="btn btn-danger actionbtn instrumentDelete"><span class="fa fa-trash"></span></button>'
                        ]);
                        var row = $('#instrumentDataTable').dataTable().fnGetNodes(newRow);
                        $(row).attr('id', data.id);
                        var oSettings = $('#instrumentDataTable').dataTable().fnSettings();
                        var nTr = oSettings.aoData[newRow[0]].nTr;
                        $('#instrumentDataTable').dataTable().fnStandingRedraw(currentpage);

                        $.notify("Successfully " + message + " Data", "success");

                    } else {
                        $.notify("Error While " + message + " Data", "error");
                        $("#save_instrument").prop("disabled", false);

                        $("#save_instrument").html(
                            'Save'
                        );
                    }
                }
            }
        });
    });

    $(document).on('change', '.instrumentStatusChanaged', function (event) {
        event.preventDefault();
        var id = $(this).attr('data-id');
        var status = $(this).attr('data-status');
        if (status == 1) {
            is_active = 0;
        } else {
            is_active = 1;
        }
        var that = $(this);
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: { "id": id, "status": is_active, 'action': 'change_instrument_status' },
            cache: false,
            dataType: "json",
            success: function (response) {
                if (response.success == 1) {
                    that.attr('data-status', is_active);
                    $.notify("Successully Change Status ", "success");
                } else {
                    $.notify("Error While Change Status", "error");
                }
            }
        });
    });

    $(document).on('click', '.instrumentDelete', function (event) {
        event.preventDefault();
        var id = $(this).attr('data-id');
        var that = $(this);
        var table = $('#instrumentDataTable').DataTable();
        var info = table.page.info();
        var currentpage = info.start;

        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: { "id": id, 'action': 'delete_instrument' },
            cache: false,
            dataType: "json",
            success: function (response) {
                if (response.success == 1) {
                    table.row(that.parents('tr')).remove().draw();
                    $.notify("Successully Delete Data ", "success");
                } else {
                    $.notify("Error While Delete Data", "error");
                }
            }
        });
    });
});