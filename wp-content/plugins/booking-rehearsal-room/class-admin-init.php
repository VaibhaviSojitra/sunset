<?php

if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}
class SSRB_ADMIN_INIT {

     
    function __construct()
    {
        require_once (SSRB_PLUGIN_DIR_PATH . 'admin/manage-menu.php');
        require_once (SSRB_PLUGIN_DIR_PATH . 'manage-shortcode.php');
        require_once (SSRB_PLUGIN_DIR_PATH . 'include/manage-dbtable.php');
    }

}
