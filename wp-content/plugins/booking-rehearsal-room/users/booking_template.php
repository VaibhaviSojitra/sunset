<link rel='stylesheet' id='ssrb-users-css'  href="<?php echo SSRB_URL ?>/css/ssrb-user-style.css" type='text/css' media='all' />
<div class="container-fluid">
    <input type="hidden" class="current_url" value="<?php echo parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); ?>">
    <input type="hidden" class="cart_page_url" value="<?php echo site_url().'/'.$cart_page_url; ?>">
    <input type="hidden" id="site_url" value="<?php echo site_url(); ?>">
    <input type="hidden" id="plugin_path" value="<?php echo SSRB_URL; ?>">
    <input type="hidden" class="current_date" value="<?php echo date('Y-m-d', strtotime($date)) ?>"> 
    <?php 
    if($date == date('Y-m-d')){
        $class="col-md-10";
        $showdiv = 'display:none;'; 
    }else{
        $class="col-md-8";
        $showdiv = 'display:block;'; 
    }
    ?>
    <div class="row ssrbbox">
        <div class="col-md-2 " style="<?php echo $showdiv ?>">
            <a class="day_link" href="<?php echo parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); ?>?date=<?php echo date('Y-m-d', strtotime($date.'-1 day')) ?>"><i class="fa fa-arrow-left"></i><span>Yesterday</span></a>
        </div>
        <div class="row <?php echo  $class ?>">
            <!-- <div class="col-md-5">
                <div class="custom-control custom-checkbox weekly_book">
                    <input type="checkbox" class="custom-control-input" name="is_week" id="weekliChecked">
                    <label class="custom-control-label" for="weekliChecked"> Book slots for Week </label>
                </div>                
            </div> -->
            <div class="col-md-4">

                <input type="text" id="change_bookdate" class="form-control "  value="<?php echo date('Y-m-d', strtotime($date)) ?>">
            </div>

        </div>
        <div class="col-md-2 text-right">
            <a class="day_link" href="<?php echo parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); ?>?date=<?php echo date('Y-m-d', strtotime($date.'+1 day')) ?>"><span class="small-text">Tomorrow</span><i class="fa fa-arrow-right"></i></a>
        </div>
    </div>
    <div class="row">
        <table  class="ssrb_table mt-0">
        <tr>
                <th rowspan="2" class="ssrb_room_th text-left"><span>Room</span></th>
                <th colspan="<?php echo count($times);?>" class="ssrb_time_th text-center"><span class="display_date"><?php echo date('l, dS F', strtotime($date)) ?></span></th>
              
            </tr>
            <tr>
                <?php foreach($times as $key=>$val){ ?>
                    <th class="ssrb_time_th text-center"><?php echo date('ga', strtotime($val)); ?></th>
                <?php } ?>
                
            </tr>
        <?php
        $user_id = get_current_user_id();
        $table_cart = SSRB_PREFIX ."cart";
        $table_booking_details = SSRB_PREFIX ."booking_details";
        foreach ($rooms_list as $roomkey => $room) {
            echo "<tr>
                <td class='ssrb_room_td text-left'><div>" .$room->room_title.' ('.$room->room_color.')' ."</div></td>";
                $indexCurrentBook = -1;
                $tdColspan = 0;
               
                foreach ($times as $hourIndex => $hour) {
                    if ($indexCurrentBook >= 0) {
                        $disabled = '';
                      
                        if ($hour == $roomsData[$room->id]['end_day'][$indexCurrentBook]) { 
                            $cart_list = $wpdb->get_results("SELECT * FROM $table_cart where room_id = $room->id AND session_type = $session_type AND booking_date = '".$date."' AND is_delete = 0 ");
                            $week_day = date('l', strtotime($date));
                            if(!empty($cart_list)){

                                $user = get_userdata( $cart_list[0]->user_id);
                                $user_info = get_user_meta( $cart_list[0]->user_id,'band_name');

                                if($user_info[0]){
                                    $username = ucfirst($user_info[0]);
                                }else
                                if($user->first_name && $user->last_name ){
                                    $username =  ucfirst($user->first_name) . ' ' . ucfirst($user->last_name);
                                }else{
                                    $username = ucfirst($user->display_name);
                                }
                                $disabled = 'disabled';
                                $class = "ssrb_booked ssrb_booked".$session_type;
                                $lbltext = strtoupper($username);
                                $book_date = $cart_list['booking_date'];
                                $dataactive = 1;
                            }
                            $booking_list = $wpdb->get_results("SELECT * FROM $table_booking_details where room_id = $room->id AND session_type = $session_type AND ( booking_date = '".$date."' OR week_day = '".$week_day."')  AND is_delete = 0 ");
                            
                            if(!empty($booking_list)){
                                $_payment_status = get_post_meta($booking_list[0]->order_id, '_payment_status', true);
                                if($_payment_status != 3 && $_payment_status != 4){
                                    $user = get_userdata( $booking_list[0]->user_id);
                                    $user_info = get_user_meta( $booking_list[0]->user_id,'band_name');
                                    if($user_id == $booking_list[0]->user_id){
                                        $lbltext = 'RESERVED BY YOU';
                                    }else{
                                        $lbltext = 'RESERVED';
                                    }
                                    $disabled = 'disabled';
                                    $class = "ssrb_booked ssrb_booked".$session_type;
                                    $book_date = $booking_list['booking_date'];
                                    $dataactive = 1;
                                }else{
                                    $disabled = '';
                                    $lbltext = 'AVAILABLE';
                                    $class = "ssrb_available ssrb_available".$session_type;
                                    $book_date = date('Y-m-d', strtotime($date));
                                    $dataactive = 0;
                                } 
                                
                            }
                            if(empty($cart_list) && empty($booking_list)){
                                $disabled = '';
                                $lbltext = 'AVAILABLE';
                                $class = "ssrb_available ssrb_available".$session_type;
                                $book_date = date('Y-m-d', strtotime($date));
                                $dataactive = 0;
                            }

                            echo '<td colspan="'.$tdColspan.'" class="ssrb_time_td"> <a class="booking_room booking_room_'.$session_type.' '.$disabled.'" data-id="'.$room->id.'" data-session_type="'.$session_type.'" data-booking_date="'.$book_date.'" data-price="'.$price1.'" ><div class="ssrb_roundCorners '.$class.'" data-active="'.$dataactive.'"> '.$lbltext.' | '.SSRB_BOOKING_CURRENCY.' '.$price1.' </div>
                            </a> </td>';
                            $indexCurrentBook = -1;
                        } else {
                            $tdColspan++;
                        }
                    }
            
                    if ($indexCurrentBook < 0) {
                        $tdColspan = 1;
                        foreach($roomsData[$room->id]['start_day']['time'] as $startBookIndex => $startBookDate) {
                            if ($hour == $startBookDate) {
                                $indexCurrentBook = $startBookIndex;
                                $price1 = $roomsData[$room->id]['start_day']['price'][$startBookIndex];
                                $session_type = $roomsData[$room->id]['start_day']['session_type'][$startBookIndex];
                            }
                        }
                    if ($indexCurrentBook < 0) echo "<td class=\"blank\"></td>";
                    }
                }
            echo "</tr>";
        } ?>
        
        </table>
    </div>
    <div class="row mb-40">
    <div class="col-md-12"> <div class="error"></div>   </div> 
        <div class="col-md-10">
            <div class="row">
                <div class="form-group col-md-4">
                    <label class="control-label" for="instrument"><b>Instrument</b></label>
                    <div class="">
                        <select type="text" class="form-control" name="instrument" id="instrument">
                            <option value=""> Select</option>                                   
                        </select>
                        <!-- <p>Select time slot to show instrument.</p> -->
                    </div>
                </div>   
                
                <div class="form-group col-md-4">
                    <label class="control-label" for="instrument"><b>Instrument Preview</b></label>
                    <div class="">
                        <img id="img_preview" src="<?php echo SSRB_URL.'/images/default.jpg'; ?>" class="img_preview" width="200" height="200" alt="Preview">
                    </div>
                </div> 
            </div> 
        </div>

        <div class="col-md-2">
           <button class="add_to_cart submit_button">Add to Cart</button>
        </div>
    </div>
       

</div>

<script>
var $ = jQuery;
var ajaxurl = "<?php echo admin_url('admin-ajax.php') ?>";

$(document).ready(function () {
    $('.booking_room.disabled').click(function () { return false; });
    var plugin_path = $('#plugin_path').val();

    var date = $('#change_bookdate').val();

    $('#change_bookdate').datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: new Date(),
    });
    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ]; 
    var suffix = "";
    function ordinal_suffix_of(i) {
        var j = i % 10,
            k = i % 100;
        if (j == 1 && k != 11) {
            return i + "st";
        }
        if (j == 2 && k != 12) {
            return i + "nd";
        }
        if (j == 3 && k != 13) {
            return i + "rd";
        }
        return i + "th";
    }
    // $(document).on('click', 'input[type="checkbox"]', function () {
    //     var date = $('.current_date').val();
       
    //     if($(this).prop("checked") == true){

    //         var mydate = new Date(date);
                      
    //         var frmdate_dayame = weekday[mydate.getDay()];
    //         var frmdate_mame = monthNames[mydate.getMonth()];
    //         var fromDate = frmdate_dayame + ', '+ ordinal_suffix_of(mydate.getDate()) + ' '+ frmdate_mame;

    //         mydate.setDate(mydate.getDate() + 6); 

    //         var dd = mydate.getDate();
    //         var mm = mydate.getMonth() + 1;
    //         var yy = mydate.getFullYear();
    //         var maxDate = yy + '-'+ mm + '-'+ dd;

    //         var todate_dayame = weekday[mydate.getDay()];
    //         var todate_mame = monthNames[mydate.getMonth()];
    //         var toDate = todate_dayame + ', '+ ordinal_suffix_of(dd) + ' '+ todate_mame;
    //         $('.display_date').text(fromDate+' - '+toDate);
    //         $('#change_bookdate').datepicker("option", { "minDate":  new Date(date),"maxDate":  new Date(maxDate) });  
    //     }
    //     else if($(this).prop("checked") == false){
    //         var mydate = new Date();
                      
    //         var frmdate_dayame = weekday[mydate.getDay()];
    //         var frmdate_mame = monthNames[mydate.getMonth()];
    //         var fromDate = frmdate_dayame + ', '+ ordinal_suffix_of(mydate.getDate()) + ' '+ frmdate_mame;
    //         $('.display_date').text(fromDate);
    //         $('#change_bookdate').datepicker("option", { "minDate": new Date() });                
    //     }
    // });
    $(document).on('change', '#change_bookdate', function () {
        var date = $(this).val();
        var current_url = $('.current_url').val();
        window.location.href = current_url + '?date=' + date; 
        // if($('input[type="checkbox"]').prop("checked") == false){
        //     window.location.href = current_url + '?date=' + date; 
        // }else{
        //     window.location.href = current_url + '?date=' + date; 
        //     var mydate = new Date(date);
        //     var frmdate_dayame = weekday[mydate.getDay()];
        //     var frmdate_mame = monthNames[mydate.getMonth()];
        //     var fromDate = frmdate_dayame + ', '+ ordinal_suffix_of(mydate.getDate()) + ' '+ frmdate_mame;

        //     mydate.setDate(mydate.getDate() + 6); 

        //     var dd = mydate.getDate();
        //     var mm = mydate.getMonth() + 1;
        //     var yy = mydate.getFullYear();
        //     var maxDate = yy + '-'+ mm + '-'+ dd;

        //     var todate_dayame = weekday[mydate.getDay()];
        //     var todate_mame = monthNames[mydate.getMonth()];
        //     var toDate = todate_dayame + ', '+ ordinal_suffix_of(dd) + ' '+ todate_mame;
        //     $('.display_date').text(fromDate+' - '+toDate);

        //     $('#change_bookdate').datepicker("option", { "minDate":  new Date(date),"maxDate":  new Date(maxDate) }); 
        // }
    });

    $(document).on('click', '.booking_room_1', function (event) {
        event.preventDefault();

        $('.ssrb_booked1').each(function () {
            if (!$(this).parent().hasClass('disabled')) {
                $(this).removeClass('ssrb_booked');
                $(this).removeClass('ssrb_booked1');
                $(this).addClass('ssrb_available');
                $(this).addClass('ssrb_available1');
            }
        });
        if ($(this).find('div').attr('data-active') == 1) {
            $(this).find('div').removeClass('ssrb_booked');
            $(this).find('div').removeClass('ssrb_booked1');
            $(this).find('div').addClass('ssrb_available');
            $(this).find('div').addClass('ssrb_available1');
            $(this).find('div').attr('data-active', 0);
            var html = '<option value="">Select</option>';
            $('#instrument').html(html);
            var image = plugin_path+'/images/default.jpg';
            $('#img_preview').attr('src',image);
        } else {
            $(this).find('div').removeClass('ssrb_available');
            $(this).find('div').removeClass('ssrb_available1');
            $(this).find('div').addClass('ssrb_booked');
            $(this).find('div').addClass('ssrb_booked1');
            $(this).find('div').attr('data-active', 1);
            var ItemArray = [];

            $('.ssrb_booked').each(function () {
                if (!$(this).parent().hasClass('disabled')) {
                    var session_type = $(this).parent().attr('data-session_type');
                    ItemArray.push(session_type);
                }
            });
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: { "ItemArray": ItemArray, "flag": 1, "action": 'get_instrument' },
                cache: false,
                dataType: "json",
                success: function (response) {
                    var html = '<option value="">Select</option>';

                    if (response.success == 1) {
                        $(response.data).each(function(i,value) {
                            if(value.session_type == 1){
                                var session_type = 'per Day Session';
                            }else if(value.session_type == 2){
                                var session_type = 'per Night Session';
                            }else{
                                var session_type = 'per Day/Night Session';
                            }
                            html += '<option value="'+value.id+'">'+value.instrument_name+' (<?php echo SSRB_BOOKING_CURRENCY?> '+value.price+' '+ session_type +')</option>';
                        });
                    }
                    $('#instrument').html(html);
                }
            });
        }

    });
    $(document).on('click', '.booking_room_2', function (event) {
        event.preventDefault();

        $('.ssrb_booked2').each(function () {
            if (!$(this).parent().hasClass('disabled')) {
                $(this).removeClass('ssrb_booked');
                $(this).removeClass('ssrb_booked2');
                $(this).addClass('ssrb_available');
                $(this).addClass('ssrb_available2');
            }
        });

        if ($(this).find('div').attr('data-active') == 1) {
            $(this).find('div').removeClass('ssrb_booked');
            $(this).find('div').removeClass('ssrb_booked2');
            $(this).find('div').addClass('ssrb_available');
            $(this).find('div').addClass('ssrb_available2');
            $(this).find('div').attr('data-active', 0);
            var html = '<option value="">Select</option>';
            $('#instrument').html(html);
            var image = plugin_path+'/images/default.jpg';
            $('#img_preview').attr('src',image);
        } else {
            $(this).find('div').removeClass('ssrb_available');
            $(this).find('div').removeClass('ssrb_available2');
            $(this).find('div').addClass('ssrb_booked');
            $(this).find('div').addClass('ssrb_booked2');
            $(this).find('div').attr('data-active', 1);
            var ItemArray = [];
            $('.ssrb_booked').each(function () {
                if (!$(this).parent().hasClass('disabled')) {
                    var session_type = $(this).parent().attr('data-session_type');
                    ItemArray.push(session_type);
                }
            });
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: { "ItemArray": ItemArray, "flag": 1, "action": 'get_instrument' },
                cache: false,
                dataType: "json",
                success: function (response) {
                    var html = '<option value="">Select</option>';

                    if (response.success == 1) {
                        $(response.data).each(function(i,value) {
                            if(value.session_type == 1){
                                var session_type = 'per Day Session';
                            }else if(value.session_type == 2){
                                var session_type = 'per Night Session';
                            }else{
                                var session_type = 'per Day/Night Session';
                            }
                            html += '<option value="'+value.id+'">'+value.instrument_name+' ($'+value.price+' '+ session_type +')</option>';
                        });
                    }
                    $('#instrument').html(html);
                }
            });
        }
    });
    $(document).on('change', '#instrument', function (event) {
        var id = $(this).val();
        var site_url = $('#site_url').val();
        if(id){
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: { "id": id, "flag": 2, "action": 'get_instrument' },
                cache: false,
                dataType: "json",
                success: function (response) {
                    
                    if (response.success == 1) {
                        var image = site_url+''+response.data[0].image;
                        $('#img_preview').attr('src',image);
                    }else{
                        var image = plugin_path+'/images/default.jpg';
                        $('#img_preview').attr('src',image);
                    }
                    
                }
            });
        }else{
             var image = plugin_path+'/images/default.jpg';
                $('#img_preview').attr('src',image);
        }
    });
    
    $('.error').hide();
    $(document).on('click', '.add_to_cart', function (event) {
        event.preventDefault();
        var ItemArray = [];
        $('.error').hide();
        var booking_date = $('#change_bookdate').val();

        $('.ssrb_booked').each(function () {
            if (!$(this).parent().hasClass('disabled')) {
                var room_id = $(this).parent().attr('data-id');
                var price = $(this).parent().attr('data-price');
                var session_type = $(this).parent().attr('data-session_type');
                ItemArray.push({
                    session_type: session_type,
                    price: price,
                    room_id: room_id,
                    booking_date: booking_date
                });
            }
        });
        var cart_page_url = $('.cart_page_url').val();
        var instrument = $('#instrument').val();
        if($('input[type="checkbox"]').prop("checked") == true){
            var is_week = 1;
        }else{
            var is_week = 0;
        }
        if (ItemArray.length != 0) {
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: { "ItemArray": ItemArray, "instrument": instrument,  "is_week": is_week, "action": 'proccess_booking_room' },
                cache: false,
                dataType: "json",
                success: function (response) {
                    if (response.success == 1) {
                        window.location.href = cart_page_url;
                    } else {
                        $('.error').show();
                        $('.error').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Error! </strong><span>You have Already Booked Room With Same Session for This Day. </span>  </div> ');
                    }
                }
            });
        } else {
            $('.error').show();
            $('.error').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error! </strong><span> Please Select Avilable Room.</span> </div> ');
        }
    });
});
</script>