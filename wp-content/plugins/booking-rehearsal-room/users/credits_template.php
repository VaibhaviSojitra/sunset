<link rel='stylesheet' id='ssrb-users-css'  href="<?php echo SSRB_URL ?>/css/ssrb-user-style.css" type='text/css' media='all' />
<div class="container-fluid">
    <div class="row">        
        <div class="col-md-8">
            <div style="width:50%; float:left;">            
                <h2>Wallet Credits</h2>
                    <p><?php if($wallet != 0 ){ echo '$'.$wallet; }else { echo 'You do not have any credits.'; } ?></p>
            </div>
            <div style="width:50%; float:left;">            
                <h2>Card Detail</h2>
                <?php if(!empty(get_user_meta($user_id, 'stripe_customer_cardid'))){
                    echo '<p style="margin-bottom:0px;">'.get_user_meta($user_id, 'stripe_card_type', true).' **** '.get_user_meta($user_id, 'stripe_last4', true).'</p>
                    <p>Expire at : '.get_user_meta($user_id, 'stripe_exp_month', true).'/'.get_user_meta($user_id, 'stripe_exp_year', true).'</p>';
                } ?>
                   
            </div>
            <div class="divider"></div>
            <div style="width:50%; float:left;">            
                <h2>Today Booking</h2>
                <?php 
                    if($current_booking_list){
                        foreach($current_booking_list as $value){ 
                            echo '<p>'.$value->room_title.' - '.$value->session_time.'</p>';
                        }
                    }else{
                ?>
                    <p>You don't have any booking</p>
                <?php                        
                    }
                ?>
            </div>
            <div class="divider"></div>

            <?php echo do_shortcode('[button class="ss-orange-gradient" size="large" link="'.site_url().'/room-booking"]Book Room[/button]'); ?>
        </div>
        <div class="col-md-4">
            <div class="content"> 

            <div class="ssrb_sidebar">
                    <h3 class="ssrb_sidebar_title">My Credits</h3>
                    <ul>                                      
                        <li class="page_item">
                            <a class="<?php if(is_page($credits)){ echo "active"; }?>" href="<?php echo site_url()?>/credits" title="Dashboard">Dashboard</a>
                        </li>              
                        <!-- <li class="page_item">
                            <a class="<?php if(is_page($create_band_page) || is_page($update_band_page)){ echo "active"; }?>" href="<?php echo site_url()?>/<?php echo $bandurl ?>" title="<?php echo  $bandtext ?>"><?php echo  $bandtext ?></a>
                        </li>   -->
                        <li class="page_item">
                            <a class="<?php if(is_page($booking_list_page)){ echo "active"; }?>" href="<?php echo site_url()?>/booking-list" title="Booking List">Booking List</a>
                        </li> 
                        <!-- <li class="page_item">
                            <a class="<?php if(is_page($save_card_page)){ echo "active"; }?>" href="<?php echo site_url()?>/save-card" title="Save Card">Save Card</a>
                        </li>   -->
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
var $ = jQuery;
var ajaxurl = "<?php echo admin_url('admin-ajax.php') ?>";
$(document).ready(function () {
    
});
</script>