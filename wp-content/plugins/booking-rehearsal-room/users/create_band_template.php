<link rel='stylesheet' id='ssrb-users-css'  href="<?php echo SSRB_URL ?>/css/ssrb-user-style.css" type='text/css' media='all' />
<div class="container-fluid">
<input type="hidden" id="myband" value="<?php echo site_url().'/'.$credits; ?>">
    <div class="row">        
        <div class="col-md-8">
           
            <form class="form-horizontal" method="post" id="frmaddupdateband">
            <?php wp_nonce_field('addupdateband','addupdateband'); ?>
            <input type="hidden" name="action" value="add_update_band">
            <input type="hidden" name="hidden_id" value="0">
                <div class="form-group">
                    <label class="control-label" for="band_name">Band Name</label>
                    <div class="">
                        <input type="text" class="form-control" name="band_name" id="band_name" value="<?php if(!empty(get_user_meta($user_id, 'is_band'))){ echo get_user_meta($user_id, 'band_name',true); } ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="description">Description</label>
                    <div class="">
                        <input type="text" class="form-control" name="description" id="description" value="<?php if(!empty(get_user_meta($user_id, 'is_band'))){ echo get_user_meta($user_id, 'band_description',true); } ?>" required>
                    </div>
                </div>
            
                <fieldset>
                    <legend><p>Contact Information</p></legend>
                        <div class="form-group">
                            <label class="control-label" for="contact_no">Your contact Phone No.</label>
                            <div class="">
                                <input type="text" class="form-control" name="contact_no" id="contact_no" value="<?php if(!empty(get_user_meta($user_id, 'is_band'))){ echo get_user_meta($user_id, 'contact_no',true); } ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="address">Address</label>
                            <div class="">
                                <input type="text" class="form-control" name="address" id="address"  value="<?php if(!empty(get_user_meta($user_id, 'is_band'))){ echo get_user_meta($user_id, 'address',true); } ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="city">City</label>
                            <div class="">
                                <input type="text" class="form-control" name="city" id="city" value="<?php if(!empty(get_user_meta($user_id, 'is_band'))){ echo get_user_meta($user_id, 'city',true); } ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="state">State</label>
                            <div class="">
                                <input type="text" class="form-control" name="state" id="state"  value="<?php if(!empty(get_user_meta($user_id, 'is_band'))){ echo get_user_meta($user_id, 'state',true); } ?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="postal_code">Postal Code</label>
                            <div class="">
                                <input type="text" class="form-control" name="postal_code" id="postal_code" value="<?php if(!empty(get_user_meta($user_id, 'is_band'))){ echo get_user_meta($user_id, 'postal_code',true); } ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="country">Country</label>
                            <div class="">
                                <select type="text" class="form-control" name="country" id="country" required>
                                    <?php foreach($country_list as $value){ 
                                        if($value['code'] == get_user_meta($user_id, 'country', true )){
                                            $selected = 'selected';
                                        }else{ 
                                            $selected = '';   
                                        }?>
                                        <option value="<?php echo $value['code'] ?>" <?php echo $selected; ?>><?php echo $value['name'] ?></option>
                                    <?php } ?>                                    
                                </select>
                            </div>
                        </div>
            
                </fieldset>
                <div class="error"></div> 
                <button type="submit" class="mb-40 submit_button"><?php echo $bandtext ?></button>
            </form>
        </div>
        <div class="col-md-4">
            <div class="content"> 

                <div class="ssrb_sidebar">
                    <h3 class="ssrb_sidebar_title">My Credits</h3>
                    <ul>                                      
                                                
                        <li class="page_item">
                            <a class="<?php if(is_page($credits)){ echo "active"; }?>" href="<?php echo site_url()?>/credits" title="Dashboard">Dashboard</a>
                        </li>              
                        <li class="page_item">
                         
                            <a class="<?php if(is_page($create_band_page) || is_page($update_band_page)){ echo "active"; }?>" href="<?php echo site_url()?>/<?php echo $bandurl ?>" title="<?php echo  $bandtext ?>"><?php echo  $bandtext ?></a>
                        </li>  
                        <li class="page_item">
                            <a class="<?php if(is_page($booking_list_page)){ echo "active"; }?>" href="<?php echo site_url()?>/booking-list" title="Booking List">Booking List</a>
                        </li> 
                        <!-- <li class="page_item">
                            <a class="<?php if(is_page($save_card_page)){ echo "active"; }?>" href="<?php echo site_url()?>/save-card" title="Save Card">Save Card</a>
                        </li>  -->
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
var $ = jQuery;
var ajaxurl = "<?php echo admin_url('admin-ajax.php') ?>";
$(document).ready(function () {
    
    $(document).on('submit', '#frmaddupdateband', function (event) {
        event.preventDefault();
        $('.error').hide();
        $(".createband").prop("disabled", true);

        $(".createband").html(
            '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Updating...'
        );
        var myband = $('#myband').val();
        var formData = new FormData($("#frmaddupdateband")[0]);
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (response) {
                $(".createband").prop("disabled", false);
                $(".createband").html('Save');
                if (response.success == 1) {
                    $('.error').show();
                    $('.error').html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success! </strong><span>Successfully Update Data. </span>  </div> '); 
                    window.location = myband;
                } else {
                    $('.error').show();
                    $('.error').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Error! </strong><span>You have Already Booked Room With Same Session for This Day. </span>  </div> ');                
                }
            }
        });
    });
});
</script>