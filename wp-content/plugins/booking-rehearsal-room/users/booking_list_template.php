<link rel='stylesheet' id='ssrb-users-css'  href="<?php echo SSRB_URL ?>/css/ssrb-user-style.css" type='text/css' media='all' />
<link rel='stylesheet'  href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" type='text/css' media='all' />
<link rel='stylesheet'  href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" type='text/css' media='all' />
<link rel='stylesheet'  href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css" type='text/css' media='all' />
<style>
    .stripe-button-el{
        padding: 0px 10px !important;
    }
    .stripe-button-el span {
        font-size: 12px !important;
    }
</style>

<div class="container-fluid">
    <div class="row">        
        <div class="col-md-9">
            
                <table id="bookingList" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Order ID</th>
                        <th>Room Details</th>
                        <th>Is Permanent</th>
                        <th>Booking Day</th>
                        <th>Total</th>
                        <th>Payment Status</th>
                        <th class="none">Price</th>
                        <th class="none">Instrument Name</th>
                        <th class="none">Instrument Price</th>
                        <th>Pay Now</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($booking_list as $value){ 
                           $payment_status = get_post_meta($value->order_id, '_payment_status', true);
                           $amount = get_post_meta($value->order_id, '_amount', true);
                           if($value->is_week == 1){

                                $bookdate = $value->booking_date;
                                if($payment_status == 2){
                                    $weekNoNextday =    $booking_date = date('Y-m-d', strtotime('next '.$value->week_day, strtotime($value->booking_date))); 
                                }else{
                                    $nextday = strtotime('next '.$value->week_day);
                                    $weekNoNextday = date('Y-m-d', $nextday);
                                } 
                                
                                if($weekNoNextday == $bookdate){
                                    $book_date = 'Every '.$value->week_day.' <br/> Upcoming Booking -  <br/>'.$bookdate;
                                }else if($bookdate == date('Y-m-d')){
                                    $book_date = 'Every '.$value->week_day.' <br/> Today Booking -  <br/>'.$bookdate;
                                }else{
                                    $book_date = 'Every '.$value->week_day.' <br/> '.$bookdate;
                                }
                                $booking_date = $bookdate;
                            }else{
                                $booking_date = $value->booking_date;
                                $book_date = $value->booking_date;
                            }
                            $band_name = get_user_meta( $value->user_id,'band_name');
                            $user = get_userdata( $value->user_id );
                            if($band_name[0]){
                                $band_name = ucfirst($band_name[0]);
                            }else
                            if($user->first_name && $user->last_name ){
                                $band_name =  ucfirst($user->first_name) . ' ' . ucfirst($user->last_name);
                            }else{
                                $band_name = ucfirst($user->display_name);
                            }
                        ?>
                        <tr>
                            <td><span style="margin-left:30px;"><?php echo $value->order_id ?></span></td>
                            <td><?php echo $value->room_title ?><br/>
                            <?php if($value->session_type == 1){ echo 'Day ('.$value->day_start_time.')'; }else{ echo 'Night ('.$value->night_start_time.')'; } ?></td>
                            <td><?php if($value->is_week == 1){ echo "Yes"; }else{ echo "No"; } ?></td>
                            <td><?php echo $book_date; ?></td>
                            <td><?=SSRB_BOOKING_CURRENCY?><?php echo number_format($amount,2) ?></td>
                            <td><?php if($payment_status == 1){echo "Pending";}elseif($payment_status == 2){echo "Completed";}elseif($payment_status == 3){echo "Refund";}elseif($payment_status == 4){echo "Cancelled";}?></td>
                            <td><?php echo SSRB_BOOKING_CURRENCY.' '.number_format($value->price,2); ?></td>
                            <td><?php if($value->is_instrument == 1){echo $value->instrument_name;} ?></td>
                            <td><?php if($value->is_instrument == 1){echo  SSRB_BOOKING_CURRENCY.' '.number_format($value->i_price,2);} ?></td>
                            <td><?php if($payment_status == 1){ echo do_shortcode('[wp_stripe_checkout name="Sunset" item_name="'. $band_name.'" label="Pay Now" amount="'.number_format($amount,2).'" item_price="'.number_format($value->price,2).'" room_id="'.$value->room_id.'" session_type="'.$value->session_type.'"  booking_date="'.$booking_date.'" is_week="'.$value->is_week.'"  is_instrument="'.$value->is_instrument.'" instrument_id="'.$value->instrument_id.'"  payment_status="2" order_id="'.$value->order_id.'" is_booking_list="1"]'); }elseif($payment_status == 2){echo "Paid";}else{echo "Cancelled";} ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <div class="divider"></div>

            <?php echo do_shortcode('[button class="ss-orange-gradient" size="large" link="'.site_url().'/room-booking"]Book Room[/button]'); ?>
        </div>
        <div class="col-md-3">
            <div class="content"> 

            <div class="ssrb_sidebar">
                    <h3 class="ssrb_sidebar_title">My Credits</h3>
                    <ul>                                      
                                                
                        <li class="page_item">
                            <a class="<?php if(is_page($credits)){ echo "active"; }?>" href="<?php echo site_url()?>/credits" title="Dashboard">Dashboard</a>
                        </li>              
                        <!-- <li class="page_item">
                            <a class="<?php if(is_page($create_band_page) || is_page($update_band_page)){ echo "active"; }?>" href="<?php echo site_url()?>/<?php echo $bandurl ?>" title="<?php echo  $bandtext ?>"><?php echo  $bandtext ?></a>
                        </li>   -->
                        <li class="page_item">
                            <a href="<?php echo site_url()?>/booking-list" title="Booking List">Booking List</a>
                        </li> 
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
<script type='text/javascript' src='https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js'></script>
<script type='text/javascript' src='https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js'></script>
<script type='text/javascript' src='https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js'></script>
<script type='text/javascript' src='https://cdn.datatables.net/responsive/2.2.5/js/responsive.bootstrap4.min.js'></script>

<script>
var $ = jQuery;
var ajaxurl = "<?php echo admin_url('admin-ajax.php') ?>";
$(document).ready(function () {
    $('#bookingList').DataTable( {
        "aaSorting": []
    });
});
</script>