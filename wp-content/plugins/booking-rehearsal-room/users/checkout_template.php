<link rel='stylesheet' id='ssrb-users-css'  href="<?php echo SSRB_URL ?>/css/ssrb-user-style.css" type='text/css' media='all' />
<input type="hidden" class="book_page_url" value="<?php echo site_url().'/'.$book_page_url; ?>">
<input type="hidden" class="band_page_url" value="<?php echo site_url().'/'.$band_page_url; ?>">
<input type="hidden" class="booking_list_page" value="<?php echo site_url().'/'.$booking_list_page; ?>">
<style>
    .stripe-button-el{
        padding: 10px 25px !important;
    }
    .stripe-button-el span {
        font-size: 14px !important;
    }
</style>
<div class="loader"></div>
<div class="row  mb-40">
    
    <div class="col-md-6">
        <h3>Contact Information</h3>
        <table class="checkout_table" border="0">

            <tr>
                <th>
                    <label class="checkout_lable">Name</label>
                </th>
                <th>
                    <label class="checkout_lable"><?php echo ucfirst($user->first_name) . ' ' . ucfirst($user->last_name) ?></label>
                </th>
            </tr>
            <tr>
                <th>
                    <label class="checkout_lable">Email</label>
                </th>
                <th>
                    <label class="checkout_lable"><?php echo $user->user_email ?></label>
                </th>
            </tr>
            <tr>
                <th>
                    <label class="checkout_lable">Phone</label>    
                </th>
                <th>
                    <label class="checkout_lable"><?php echo '0'.chunk_split($user_info[0], 3, ' ') ?></label>
                </th>
            </tr>
        </table>
    </div> 
    <div class="col-md-6">
        <h3>Cancellation Policy</h3>
        <h4>Sunset Rehearsals</h4>
        <ul>                                      
            <li class="page_item">
                If Cancelled within 48 hours the cancellation fee is 40% of the session price.
            </li>  
            <li class="page_item">
            If Cancelled within 24 hours the cancellation fee is 80% of the session price.
            </li> 
        </ul>
    </div>
</div>

<div class="row">
    
    <div class="col-md-12">
        <h3>Your Order</h3>
        <table class="product" border="0">

            <tr>
                <th>
                    <label class="product_label">Description</label>
                </th>
                <th>
                    <label class="product_label product-price">Price</label>    
                </th>
                <th>
                    <label class="product_label product-line-price">Amount</label>
                </th>
                <th>
                    <label class="product_label">Use Credit ?</label>
                </th>
                <th>
                    <label class="product_label"> Pay Now</label>
                </th>
            </tr>
            <?php foreach($cart_list as $key => $value){ 

            $user = get_user_by( 'ID', $value->user_id);
            $time = explode('-',$value->expire_time);
            $mins = $time[0];
            $secs = $time[1];

            if($value->session_type == 1){
                $start_time = $value->day_start_time;
            }else{
                $start_time = $value->night_start_time;
            }
            $band_name = get_user_meta( $value->user_id,'band_name');
            $user = get_userdata( $value->user_id );
            if($band_name[0]){
                $band_name = ucfirst($band_name[0]);
            }else
            if($user->first_name && $user->last_name ){
                $band_name =  ucfirst($user->first_name) . ' ' . ucfirst($user->last_name);
            }else{
                $band_name = ucfirst($user->display_name);
            }

            $room_id[] = $value->room_id;
            $session_type[] = $value->session_type;
            $booking_date[] = $value->booking_date;

            if($value->is_week == 1){
                $is_week[] = 1;
                $is_hidden_week = 1;
                $is_payment_status = 2;
            }else{
                $is_week[]  = 0;
                $is_hidden_week = 0;
                $is_payment_status = 1;
            }
            $datehtml = date('F d, Y', strtotime($value->booking_date));
            $price = $value->price;

            // $room_price[] = $value->price;

            if($value->is_instrument == 1){
                if($value->i_session_type == $value->session_type || $value->i_session_type == 3){
                    $ttlprice = $value->price + $value->i_price;
                    $ihtml = '<p>Instrument : '.$value->instrument_name.' (Price : '.SSRB_BOOKING_CURRENCY.' '.$value->i_price.')</p>';
                    $is_instrument[] = $value->is_instrument;
                    $instrument_id[] = $value->instrument_id;
                }else{
                    $ttlprice =  $value->price;
                    $ihtml = '';
                    $is_instrument[] = 0;
                    $instrument_id[] = 0;
                }
                
            }else{
                $ttlprice =  $value->price;
                $ihtml = '';

                $is_instrument[] = 0;
                $instrument_id[] = 0;
            }
            $room_price[] = $ttlprice;
            $subtotal += $ttlprice;
            $payment_status[] = 2;
            ?>
            <tr id="product_<?php echo $value->id; ?>" class="total_product">
             
                <td style="text-align:center;">
                    <p style="margin-bottom: 5px;">Booking - <?php echo $datehtml; ?>, <?php echo $start_time ?></p>
                    <p style="margin-bottom: 5px;">for: <?php echo $band_name ?></label>
                    <span class="clocktime time" style="display:none;" data-min="<?php echo $mins; ?>" data-sec="<?php echo $secs; ?>" data-cartid="<?php echo $value->id; ?>"><?php echo $mins?> mins <?php echo $secs ?> secs</span>
                    <?php echo $ihtml ?>
                    </p>
                    <input type="hidden" class="ssrb_checkout_booked" data-id="<?php echo $value->room_id; ?>" data-price="<?php echo number_format($ttlprice,2); ?>" data-session_type="<?php echo $value->session_type; ?>" data-booking_date="<?php echo $value->booking_date; ?>" data-is_week="<?php echo $is_hidden_week; ?>" data-instrument_id="<?php echo $value->instrument_id; ?>" data-payment_status="<?php echo $is_payment_status; ?>">

                </td>
                <td>
                    <label class="product_label2 product-price"><?=SSRB_BOOKING_CURRENCY?> <span><?php echo number_format($price,2); ?></span></label>    
                </td>
                <td>
                    <label class="product_label2 product-line-price"><?=SSRB_BOOKING_CURRENCY?> <span><?php echo number_format($ttlprice,2); ?></span></label>
                </td>

                <?php  if($wallet > 0){
                    $disabled = '';   
                }else{
                    $disabled = 'disabled'; 
                }?>
                <td>
                    <div class="switchToggle">
                        <input type="checkbox" data-key="<?php echo $key; ?>" data-price="<?php echo number_format($ttlprice,2); ?>" data-is_week="<?php echo $is_hidden_week; ?>"  class="use_credit" id="use_credit_<?php echo $value->id; ?>" <?php echo $disabled; ?>>
                        <label for="use_credit_<?php echo $value->id; ?>">Toggle</label>
                    </div>

                </td>
                <?php  if($value->is_week == 1){
                    $disabled = '';   
                }else{
                    $disabled = 'disabled'; 
                }?>
                <td>
                    <div class="switchToggle">
                        <input type="checkbox"  data-key="<?php echo $key; ?>" data-price="<?php echo number_format($ttlprice,2); ?>" data-is_week="<?php echo $is_hidden_week; ?>"  class="pay_now" id="pay_now_<?php echo $value->id; ?>" <?php echo $disabled; ?> checked>
                        <label for="pay_now_<?php echo $value->id; ?>">Toggle</label>
                    </div>
                </td>               
            </tr>
            <?php } ?>
        </table>
    </div> 
</div>

<div class="row mb-40">
<div class="col-md-6"></div>
    <div class="col-md-4">
        <table class="total_amount" border="0">

            <tr class="amount_due_now">
                <th>
                    <label class="amount_lable">Amount Due Now</label>
                </th>
                <th>
                    <label class="amount_lable amount_now"><?=SSRB_BOOKING_CURRENCY?> <span><?php echo number_format($subtotal,2) ?></span></label>    
                </th>
            </tr>
          
            <tr>
                <th>
                    <label class="amount_lable">Amount Pay Later</label>
                </th>
                <th>
                    <label class="amount_lable amount_later"><?=SSRB_BOOKING_CURRENCY?> <span>0</span></label>    
                </th>
            </tr>

            <tr>
                <th>
                    <label class="amount_lable">Wallet Amount</label>
                </th>
                <th>
                    <label class="amount_lable amount_wallet"><?=SSRB_BOOKING_CURRENCY?> <span><?php echo $wallet; ?></span></label>  
                    <input type="hidden" value="<?php echo $wallet; ?>" class="wallet">  
                </th>
            </tr>

        </table>
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="defaultUnchecked" checked>
            <label class="custom-control-label" for="defaultUnchecked">I agree to the <span>terms and conditions</span></label>
        </div>
        <input type="hidden" value="<?php echo $is_hidden_week ?>" class="is_week">  
        <input type="hidden" value="" class="is_wallet">  
        <p class="error"></p>
        <?php if($subtotal > 0){ $hide = ''; }else{ $hide = 'hide';}?>
            <div class="pay_now_btn <?php echo $hide ?>" >
            
            <?php echo do_shortcode('[wp_stripe_checkout name="Sunset" item_name="'. $band_name.'" label="Confirm/Pay" amount="'.number_format($subtotal,2).'" item_price="'.implode(',',$room_price).'" room_id="'.implode(',',$room_id).'" session_type="'.implode(',',$session_type).'"  booking_date="'.implode(',',$booking_date).'" is_week="'.implode(',',$is_week).'"  is_instrument="'.implode(',',$is_instrument).'" instrument_id="'.implode(',',$instrument_id).'"  payment_status="'.implode(',',$payment_status).'" ]'); ?> </div>

            <button class="confirm_pay submit_button hide" data-amount="<?php echo number_format($subtotal,2) ?>">Confirm/Pay</button>
        <!-- <button class="confirm_pay submit_button">Confirm/Pay</button> -->

    </div> 
    <div class="col-md-2"></div>
</div>

<script type="text/javascript">
    var $ = jQuery;
    $('.loader').hide();
    var ajaxurl = "<?php echo admin_url('admin-ajax.php') ?>";
    var book_page_url = $('.book_page_url').val();
    var band_page_url =  $('.band_page_url').val();
    var booking_list_page =  $('.booking_list_page').val();
    
    $(document).ready(function () {
        $('.time').each(function() {
            var $this = $(this), min = $(this).attr('data-min'), sec = $(this).attr('data-sec');
            var Minutes = 60 * min;
            var cartid = $(this).attr('data-cartid');
            startTimer(Minutes, $this, cartid, min, sec);

        });
        $(document).on('click', '#defaultUnchecked', function () {
            $('.error').text('');
        });

        $(document).on('click', '.stripe-button-el', function () {
            if($('#defaultUnchecked').prop('checked') == false){
                $('.error').text('please select term & condition');  
                return false;
            }
            // setTimeout(function(){
            //     var ttl =  $('.amount_now span').text();
            //     var html = $("iframe").contents();
            //     console.log(html);
            //     $("iframe").contents().find('form').find('.Button-animationWrapper-child--primary').find('.Button-content span span span').text('$'+(ttl));
            //     return true; 

            // }, 1000);
        });

        $(document).on('click', '.use_credit', function () {
 
     
            // var is_week = $(this).attr('data-is_week');
            // if(is_week == 1){
            //     if($(this).prop('checked') == true){
            //         $(this).parent().parent().parent().find('.pay_now').prop('checked',false);
            //     }else{
            //         $(this).parent().parent().parent().find('.pay_now').prop('checked',true);
            //     }
            // }

            CalculateAmount($(this));

          
        });

        $(document).on('click', '.pay_now', function () {

            var price = $(this).attr('data-price');
                   
            CalculateAmount($(this));         
         
        });

        $(document).on('click','.confirm_pay', function(event) {
            $('.error').text('');  
            event.preventDefault();

            var ItemArray = [];
            $('.error').hide();
            var $this = $(this);
            $(this).html(
                '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...'
            );
        
            $('.ssrb_checkout_booked').each(function () {
                var room_id = $(this).attr('data-id');
                var price = $(this).attr('data-price');
                var session_type = $(this).attr('data-session_type');
                var booking_date = $(this).attr('data-booking_date');
                var is_week = $(this).attr('data-is_week');
                var instrument_id = $(this).attr('data-instrument_id');
                var payment_status = $(this).attr('data-payment_status');

                ItemArray.push({
                    session_type: session_type,
                    price: price,
                    room_id: room_id,
                    booking_date: booking_date,
                    is_week: is_week,
                    instrument_id: instrument_id,
                    payment_status: 2
                });
            });

            var total_price = $('.amount_later span').text();
            var wallet_amount = $(".amount_wallet span").text();

            if (ItemArray.length != 0) {
                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: { "ItemArray": ItemArray, "total_price": total_price, "wallet_amount": wallet_amount, "action": 'proccess_booking_room_from_user' },
                    cache: false,
                    dataType: "json",
                    success: function (response) {

                        $this.html(
                            'Confirm/Pay'
                        );
                        if (response.success == 1) {
                            window.location.href = booking_list_page;
                            
                        } else {
                            $('.error').show();
                            $('.error').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Error! </strong><span>You have Already Booked Room With Same Session for This Day. </span>  </div> ');
                        }
                    }
                });
            } else {

                $this.html(
                    'Confirm/Pay'
                );
                $('.error').show();
                $('.error').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error! </strong><span> Please Select Avilable Room.</span> </div> ');
            }        

        });
    });
    function CalculateAmount(that){
        var objIndex = that.attr('data-key');
        var wallet = $('.wallet').val();
        var ttl = 0;
        var wallet_total = 0;

        var pay_now_ttl = 0;
        var pay_later_ttl = 0;
        var arr = [];
        var status;
        $('.total_product').each(function() {
            if($(this).find('.use_credit').prop('checked') == false && $(this).find('.pay_now').prop('checked') == false){
                pay_later_ttl += parseFloat($(this).find('.ssrb_checkout_booked').attr('data-price'));
                $(this).find('.ssrb_checkout_booked').attr('data-payment_status',1);
            }else if($(this).find('.use_credit').prop('checked') == true && $(this).find('.pay_now').prop('checked') == true){
                wallet_total += parseFloat($(this).find('.ssrb_checkout_booked').attr('data-price'));           
                $(this).find('.ssrb_checkout_booked').attr('data-payment_status',2);
            }else if($(this).find('.use_credit').prop('checked') == false && $(this).find('.pay_now').prop('checked') == true){
                pay_now_ttl += parseFloat($(this).find('.ssrb_checkout_booked').attr('data-price'));           
                $(this).find('.ssrb_checkout_booked').attr('data-payment_status',2);
            }else if($(this).find('.use_credit').prop('checked') == true && $(this).find('.pay_now').prop('checked') == false){
                wallet_total += parseFloat($(this).find('.ssrb_checkout_booked').attr('data-price'));           
                $(this).find('.ssrb_checkout_booked').attr('data-payment_status',2);
            }
            var status = $(this).find('.ssrb_checkout_booked').attr('data-payment_status');
            // var payment_status = $('.payment_status').val(); 
            // var arr = payment_status.split(",");  
            // $.each(arr, function (key, value) {
            // if(key == objIndex){
            //     arr[key] = status;
            // }
            // });
            arr.push(status);            
        });
        var new_str = arr.join(',');
        $('.payment_status').val(new_str); 
        if(pay_now_ttl < 0){
            pay_now_ttl = pay_now_ttl * (-1);
        }
        
        var now_ttl = parseFloat(wallet)-parseFloat(wallet_total);
        if(now_ttl < 0){
            now_ttl = now_ttl * (-1);
            $('.amount_wallet span').text('0.00');
            pay_now_ttl = pay_now_ttl + now_ttl;           
        }else{
            $('.amount_wallet span').text(now_ttl.toFixed(2));
        }
        $('.amount_now span').text((pay_now_ttl).toFixed(2));
        $('.amount_later span').text((pay_later_ttl).toFixed(2));           
        
        var amount_wallet = $(".amount_wallet span").text();
        $('.wallet_amount').val(amount_wallet);
        $('.item_amount').val(pay_now_ttl*100);
        // $('.stripe-button').attr('data-amount',pay_now_ttl*100); 
        
        var amount_now = $('.amount_now span').text();    
        if(amount_now == 0){
            $('.pay_now_btn').addClass('hide');
            $('.confirm_pay').removeClass('hide');
        }else{
            $('.pay_now_btn').removeClass('hide');
            $('.confirm_pay').addClass('hide');
        }
    }

    function startTimer(duration, display, cartid, min, sec) {
        var timer = duration, minutes, seconds;

        var timer2 = min+":"+sec;
        var interval = setInterval(function() {

        var timer = timer2.split(':');
        //by parsing integer, I avoid all extra string processing
        var minutes = parseInt(timer[0], 10);
        var seconds = parseInt(timer[1], 10);
        --seconds;
        minutes = (seconds < 0) ? --minutes : minutes;
        if (minutes < 0) clearInterval(interval);
        seconds = (seconds < 0) ? 59 : seconds;
        seconds = (seconds < 10) ? '0' + seconds : seconds;

        if (minutes <= 0 && seconds <= 0) {
            minutes = '00';
            seconds = '00';
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {"cartid":cartid,"min":minutes,"sec":seconds,"action":'expire_booking_room'},
                cache: false,
                dataType: "json",
                success: function (response) {
                    if(response.flag == 2){
                        window.location.href = book_page_url;
                        return false;
                    }else
                    if(response.flag == 1){
                        $('#product_'+cartid).remove();
                        recalculateCart();
                    }
                    
                }
            });
        }
        display.text(minutes + " mins " + seconds + " secs");
        timer2 = minutes + ':' + seconds;
        }, 1000);
       
    }

</script> 
