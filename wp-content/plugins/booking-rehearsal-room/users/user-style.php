<style type="text/css">
<?php if($link_active_color){ ?>
	.ssrb_sidebar ul li a.active{

        color: <?php echo $link_active_color?> !important;

    }
<?php } ?>
    .loader{
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?php echo SSRB_URL?>/images/page-loader.gif')  50% 50% no-repeat rgb(249,249,249);
    }
</style>