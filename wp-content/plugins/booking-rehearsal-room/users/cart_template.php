<link rel='stylesheet' id='ssrb-users-css'  href="<?php echo SSRB_URL ?>/css/ssrb-user-style.css" type='text/css' media='all' />
<input type="hidden" class="book_page_url" value="<?php echo site_url().'/'.$book_page_url; ?>">
<input type="hidden" class="checkout_page_url" value="<?php echo site_url().'/'.$checkout_page_url; ?>">
<div class="loader"></div>

<div class="shopping-cart">
<table class="product" border="0">

    <tr>
        <th>
            <label class="product_label product-title">Band</label>
        </th>
        <th>
            <label class="product_label product-details">Description</label>
        </th>
        <th>
            <label class="product_label product-price">Price</label>    
        </th>
        <th>
            <label class="product_label product-line-price">Amount</label>
        </th>
        <th>
            <label class="product_label product-removal"></label>
        </th>
    </tr>
<?php $ttlprice = 0; foreach($cart_list as $value){ 
    
    $user = get_user_by( 'ID', $value->user_id);
    $time = explode('-',$value->expire_time);
    $mins = $time[0];
    $secs = $time[1];
    

    if($value->session_type == 1){
        $start_time = $value->day_start_time;
    }else{
        $start_time = $value->night_start_time;
    }
    $datehtml = date('F d, Y', strtotime($value->booking_date));
    $price = $value->price;
    $band_name = get_user_meta( $cart_list[0]->user_id,'band_name');
    if($band_name[0]){
        $username = ucfirst($band_name[0]);
    }else
    if($user->first_name && $user->last_name ){
        $username =  ucfirst($user->first_name) . ' ' . ucfirst($user->last_name);
    }else{
        $username = ucfirst($user->display_name);
    }

    if($value->is_instrument == 1){
        if($value->i_session_type == $value->session_type || $value->i_session_type == 3){
            $ttlprice = $value->price + $value->i_price;
            $ihtml = '<p>Instrument : '.$value->instrument_name.' (Price : '.SSRB_BOOKING_CURRENCY.' '.$value->i_price.')</p>';
        }else{
            $ttlprice = $value->price;
            $ihtml = '';
        }
        
    }else{
        $ttlprice = $value->price;
        $ihtml = '';
    }
    $subtotal += $ttlprice;
    ?>

    <tr id="product_<?php echo $value->id; ?>" class="total_product">
        <td>
            <label class="product_label2 product-title"><?php echo $username ?></label>
        </td>
        <td>
            <label class="product_label2 product-details"><p style="margin-bottom: 5px;">Booking - <?php echo $datehtml; ?>, <?php echo $start_time ?></p>
            <?php echo $ihtml ?>
            <p>On hold for: <span class="clocktime time" data-min="<?php echo $mins; ?>" data-sec="<?php echo $secs; ?>" data-cartid="<?php echo $value->id; ?>"><?php echo $mins?> mins <?php echo $secs ?> secs</span></p></label>
            
        </td>
        <td>
            <label class="product_label2 product-price"><?php echo SSRB_BOOKING_CURRENCY?> <span><?php echo $price; ?></span></label>    
        </td>
        <td>
            <label class="product_label2 product-line-price"><?php echo SSRB_BOOKING_CURRENCY?> <span><?php echo $ttlprice; ?></span></label>
        </td>
        <td>
        <button class="product_label2 remove-product" data-cartid="<?php echo $value->id; ?>">
            Remove
        </button>
        </td>
    </tr>
<?php } ?>
<tr class="totaltr">
    <td colspan="2"></td>
    <td class="totals-item ">
        <label>Subtotal</label>
    </td>
    <td class="totals-item ">
        <div class="totals-value" id="cart-subtotal"><?php echo SSRB_BOOKING_CURRENCY?> <span><?php echo number_format($subtotal,2) ?></span></div>
    </td>
    <td></td>
</tr>
</table>
  
    <a href="<?php echo site_url().'/'.$book_page_url; ?>" class="newbooking">Make New Booking</a>
    <button class="emptycart submit_button">Empty Cart</button>
    <button class="checkout submit_button">Checkout</button>
</table>
</div>

<script type="text/javascript">
    var $ = jQuery;
    $('.loader').hide();
    var ajaxurl = "<?php echo admin_url('admin-ajax.php') ?>";
    var book_page_url = $('.book_page_url').val();
    var checkout_page_url = $('.checkout_page_url').val();
    $(document).ready(function () {
        // var Minutes = 60 * 1,
        // display = $('.time');
        // startTimer(Minutes, display);

        $('.time').each(function() {
            var $this = $(this), min = $(this).attr('data-min'), sec = $(this).attr('data-sec');
            var Minutes = 60 * min;
            var cartid = $(this).attr('data-cartid');
            startTimer(Minutes, $this, cartid, min, sec);

        });

        $(document).on('click','.remove-product', function(event) {
            event.preventDefault();
            $('.loader').show();
            removeItem(this);
        });

        $(document).on('click','.emptycart', function(event) {
            event.preventDefault();
            $('.loader').show();
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {"action":'empty_cart'},
                cache: false,
                dataType: "json",
                success: function (response) {
                    $('.loader').hide();
                    if(response.flag == 2){
                        $('.total_product').remove();
                        recalculateCart();
                        return false;
                    }else
                    if(response.flag == 1){
                        if(response.success == 1){
                            $('.total_product').remove();
                            recalculateCart();

                        }
                    }
                }
            });
        });

        $(document).on('click','.checkout', function(event) {
            event.preventDefault();
            $('.loader').show();
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {"action":'add_to_checkout'},
                cache: false,
                dataType: "json",
                success: function (response) {
                    $('.loader').hide();
                    window.location.href = checkout_page_url;
                }
            });
        });

    });
    function removeItem(removeButton)
    {
        var cartid = $(removeButton).attr('data-cartid');
        var productRow = $(removeButton).parent().parent();
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {"cartid":cartid,"action":'empty_cart'},
            cache: false,
            dataType: "json",
            success: function (response) {
                $('.loader').hide();
                if(response.success == 1){
                    productRow.remove();
                    recalculateCart();
                }
            }
        });
        
    }
    function recalculateCart()
    {
        var subtotal = 0;
        
        $('.total_product td .product-line-price span').each(function () {
            subtotal += parseFloat($(this).text());
        });  
        $('.totals-value').fadeOut(300, function() {
            $('#cart-subtotal span').html(subtotal.toFixed(2));
            if(subtotal == 0){
            $('.checkout').fadeOut(300);
            }else{
            $('.checkout').fadeIn(300);
            }
            $('.totals-value ').fadeIn(300);
        });
    }

    function startTimer(duration, display, cartid, min, sec) {
        var timer = duration, minutes, seconds;

        var timer2 = min+":"+sec;
        var interval = setInterval(function() {


        var timer = timer2.split(':');
        //by parsing integer, I avoid all extra string processing
        var minutes = parseInt(timer[0], 10);
        var seconds = parseInt(timer[1], 10);
        --seconds;
        minutes = (seconds < 0) ? --minutes : minutes;
        if (minutes < 0) clearInterval(interval);
        seconds = (seconds < 0) ? 59 : seconds;
        seconds = (seconds < 10) ? '0' + seconds : seconds;

        if (minutes <= 0 && seconds <= 0) {
            minutes = '00';
            seconds = '00';
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {"cartid":cartid,"min":minutes,"sec":seconds,"action":'expire_booking_room'},
                cache: false,
                dataType: "json",
                success: function (response) {
                    if(response.flag == 2){
                        window.location.href = book_page_url;
                        return false;
                    }else
                    if(response.flag == 1){
                        $('#product_'+cartid).remove();
                        recalculateCart();
                    }
                    
                }
            });
        }
        display.text(minutes + " mins " + seconds + " secs");
        timer2 = minutes + ':' + seconds;
        }, 1000);

       
    }

</script> 
