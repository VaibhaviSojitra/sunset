<?php

if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

/********************************************
PLUGIN SHORTCODE
**********************************************/
/**
 *
 */
class CREATE_BOOKING_TABLE {
    public function __construct() {
        add_action('admin_init',array($this,'create_booking_table'));
        
    }
    public function create_booking_table() {
        global $wpdb;

        $wp_session_type = SSRB_PREFIX ."session_type";
		if ($wpdb->get_var("show tables like '$wp_session_type'") != $wp_session_type) {
			$sql = "CREATE TABLE $wp_session_type (
				`id` int(11) DEFAULT NULL AUTO_INCREMENT,
				`session_type` tinyint(1) DEFAULT 0 COMMENT '1 for day/2 for night',
				`start_time` varchar(20) DEFAULT NULL,	
                `end_time` varchar(20) DEFAULT NULL,	
				`is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 for not delete/1 for delete',
				`created_date` datetime DEFAULT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
			require_once(ABSPATH . '/wp-admin/includes/upgrade.php');
            dbDelta($sql);
        
        }
        $date = date('Y-m-d H:i:s');
        $wp_rate_service = SSRB_PREFIX ."rate_service";
		if ($wpdb->get_var("show tables like '$wp_rate_service'") != $wp_rate_service) {
			$sql = "CREATE TABLE $wp_rate_service (
				`id` int(11) DEFAULT NULL AUTO_INCREMENT,
				`session_type` tinyint(1) DEFAULT 0 COMMENT '1 for day/2 for night',
				`week_day` varchar(30) DEFAULT NULL,	
                `price` varchar(30) DEFAULT NULL,	
				`is_delete` tinyint(1)  NOT NULL DEFAULT 0 COMMENT '0 for not delete/1 for delete',
				`created_date` datetime DEFAULT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
			require_once(ABSPATH . '/wp-admin/includes/upgrade.php');
            dbDelta($sql);
            $weekdays = array('Monday',
                  'Tuesday',
                  'Wednesday',
                  'Thursday',
                  'Friday',
                  'Saturday',
                  'Sunday');
            foreach ( $weekdays as $days ){
                $wpdb->insert( 
                $wp_rate_service, 
                    array( 
                        'session_type' => 1,
                        'week_day'=>$days,
                        'created_date'=>$date 
                    )
                );
                $wpdb->insert( 
                    $wp_rate_service, 
                        array( 
                            'session_type' => 2,
                            'week_day'=>$days,
                            'created_date'=>$date 
                        )
                    );
            }
            
        }
        
        $wp_rehearsal_rooms = SSRB_PREFIX ."rehearsal_rooms";
		if ($wpdb->get_var("show tables like '$wp_rehearsal_rooms'") != $wp_rehearsal_rooms) {
			$sql = "CREATE TABLE $wp_rehearsal_rooms (
				`id` int(11) DEFAULT NULL AUTO_INCREMENT,
				`room_title` varchar(50) DEFAULT NULL,
				`room_color` varchar(50) DEFAULT NULL,	
                `room_code` varchar(50) DEFAULT NULL,	
                `day_start_time` varchar(50) DEFAULT NULL,	
                `day_end_time` varchar(50) DEFAULT NULL,	
                `night_start_time` varchar(50) DEFAULT NULL,	
                `night_end_time` varchar(50) DEFAULT NULL,	
				`is_delete` tinyint(1)  NOT NULL DEFAULT 0 COMMENT '0 for not delete/1 for delete',
				`created_date` datetime DEFAULT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
			require_once(ABSPATH . '/wp-admin/includes/upgrade.php');
            dbDelta($sql);
        
        }
        
        $wp_instruments = SSRB_PREFIX ."instruments";
		if ($wpdb->get_var("show tables like '$wp_instruments'") != $wp_instruments) {
			$sql = "CREATE TABLE $wp_instruments (
				`id` int(11) DEFAULT NULL AUTO_INCREMENT,
				`instrument_name` varchar(80) DEFAULT NULL,
				`session_type` tinyint(1) DEFAULT 0 COMMENT '1 for day/2 for night/3 for both',
                `price` varchar(30) DEFAULT NULL,
                `image` text NULL DEFAULT NULL,
                `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 for active/0 for inactive',		
				`is_delete` tinyint(1)  NOT NULL DEFAULT 0 COMMENT '0 for not delete/1 for delete',
				`created_date` datetime DEFAULT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
			require_once(ABSPATH . '/wp-admin/includes/upgrade.php');
            dbDelta($sql);
        
        }
        $wp_cart = SSRB_PREFIX ."cart";
		if ($wpdb->get_var("show tables like '$wp_cart'") != $wp_cart) {
			$sql = "CREATE TABLE $wp_cart (
				`id` int(11) DEFAULT NULL AUTO_INCREMENT,
				`user_id` bigint(20) NOT NULL,
				`session_type` tinyint(1) DEFAULT 0 COMMENT '1 for day/2 for night',
                `price` varchar(30) DEFAULT NULL,
                `room_id` int(11) NOT NULL,
                `booking_date` date DEFAULT NULL,
                `is_week` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '1 for yes/0 for no',
                `week_day` VARCHAR(20) DEFAULT NULL,
                `is_instrument` tinyint(1) DEFAULT 0 COMMENT '0 for no/1 for yes',
                `instrument_id` int(11) DEFAULT NULL,
                `expire_time` varchar(30) DEFAULT NULL,
                `session_expire_time` VARCHAR(30) DEFAULT NULL,
                `booking_start_time` datetime DEFAULT NULL,
                `booking_end_time` datetime DEFAULT NULL,
                `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 for cart/2 for checkout',
				`is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 for not delete/1 for delete',
				`created_date` datetime DEFAULT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
			require_once(ABSPATH . '/wp-admin/includes/upgrade.php');
            dbDelta($sql);
        
        }

        $wp_booking_details = SSRB_PREFIX ."booking_details";
		if ($wpdb->get_var("show tables like '$wp_booking_details'") != $wp_booking_details) {
			$sql = "CREATE TABLE $wp_booking_details (
				`id` int(11) DEFAULT NULL AUTO_INCREMENT,
                `order_id` bigint(20) NOT NULL,
                `user_id` bigint(20) NOT NULL,
				`session_type` tinyint(1) DEFAULT 0 COMMENT '1 for day/2 for night',
                `price` varchar(30) DEFAULT NULL,
                `room_id` int(11) NOT NULL,
                `booking_date` date DEFAULT NULL,
                `is_week` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '1 for yes/0 for no',
                `week_day` VARCHAR(20) DEFAULT NULL,
                `is_instrument` tinyint(1) DEFAULT 0 COMMENT '0 for no/1 for yes',
                `instrument_id` int(11) DEFAULT NULL,
                `added_by` tinyint(1) DEFAULT 0 COMMENT '0 for not user/1 for admin',
				`is_delete` tinyint(1)  NOT NULL DEFAULT 0 COMMENT '0 for not delete/1 for delete',
				`created_date` datetime DEFAULT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
			require_once(ABSPATH . '/wp-admin/includes/upgrade.php');
            dbDelta($sql);        
        }

    }
}
new CREATE_BOOKING_TABLE();
?>