<?php

if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}
class SSRB_USER_AJAX_INIT {

     
    function __construct()
    {
        add_action('wp_ajax_proccess_booking_room', array($this, 'fun_proccess_booking_room'));
        add_action('wp_ajax_nopriv_proccess_booking_room', array($this, 'fun_proccess_booking_room'));

        add_action('wp_ajax_get_instrument', array($this, 'fun_get_instrument'));
        add_action('wp_ajax_nopriv_get_instrument', array($this, 'fun_get_instrument'));

        add_action('wp_ajax_expire_booking_room', array($this, 'fun_expire_booking_room'));
        add_action('wp_ajax_nopriv_expire_booking_room', array($this, 'fun_expire_booking_room'));

        add_action('wp_ajax_empty_cart', array($this, 'fun_empty_cart'));
        add_action('wp_ajax_nopriv_empty_cart', array($this, 'fun_empty_cart'));

        add_action('wp_ajax_add_update_band', array($this, 'fun_add_update_band'));
        add_action('wp_ajax_nopriv_add_update_band', array($this, 'fun_add_update_band'));

        add_action('wp_ajax_add_to_checkout', array($this, 'fun_add_to_checkout'));
        add_action('wp_ajax_nopriv_add_to_checkout', array($this, 'fun_add_to_checkout'));

        add_action('wp_ajax_proccess_booking_room_from_user', array($this, 'fun_proccess_booking_room_from_user'));
        add_action('wp_ajax_nopriv_proccess_booking_room_from_user', array($this, 'fun_proccess_booking_room_from_user'));
    }

    public function fun_proccess_booking_room()
    {
        global $wpdb;
        if (!session_id()) {
            session_start();
        }
        $date = date('Y-m-d H:i:s');
        $ItemArray = $_POST['ItemArray'];
        $user_id = get_current_user_id();
        $booking_date = $_POST['booking_date'];
        $instrument_id = $_POST['instrument'];
        $is_week = $_POST['is_week'];
        $table_cart = SSRB_PREFIX ."cart";
        $table_booking_details = SSRB_PREFIX ."booking_details";

        $session_array1 =  $_SESSION['data'];
     
        if($instrument_id){
            $is_instrument = 1; 
        }else{
            $is_instrument = 0;
        }
        $ItemArray1 = [];
        if($is_week == 1){
            for($i=0;$i<=6;$i++){
                foreach($ItemArray as $key => $value){
                    $array['session_type'] = $value['session_type'];
                    $array['price'] = $value['price'];
                    $array['room_id'] = $value['room_id'];
                    if($i == 0){
                        $array['booking_date'] = $value['booking_date'];
                    }else{
                        $array['booking_date'] = date('Y-m-d',strtotime("+".$i." day", strtotime($value['booking_date'])));
                    }
                    $ItemArray1[] = $array;
                }
            }
        }else{
            $ItemArray1 = $ItemArray;
        }

        foreach($ItemArray1 as $key => $value){
            
            $session_type = $value['session_type'];
            $price = $value['price'];
            $room_id = $value['room_id'];
            $booking_date = $value['booking_date'];
            $cart_list = $wpdb->get_results("SELECT * FROM $table_cart  where user_id = $user_id AND booking_date = '".$booking_date."'  AND session_type = $session_type AND is_delete = 0 ");
          
            $booking_details = $wpdb->get_results("SELECT * FROM $table_booking_details  where user_id = $user_id AND booking_date = '".$booking_date."'  AND session_type = $session_type AND is_delete = 0 ");
            
            if(!empty($cart_list)){
                $_payment_status = get_post_meta($cart_list[0]->order_id, '_payment_status', true);
                if($_payment_status != 3 && $_payment_status != 4){
                    $response['success'] = 0;
                    echo json_encode($response);
                    die;
                }
            }elseif(!empty($booking_details)){
                $_payment_status = get_post_meta($booking_details[0]->order_id, '_payment_status', true);
                if($_payment_status != 3 && $_payment_status != 4){
                    $response['success'] = 0;
                    echo json_encode($response);
                    die;
                }
            }else{
                $startTime = date('Y-m-d h:i:s', time());
                $time = time();
                $endTime = date("Y-m-d h:i:s", strtotime('+15 minutes', $time));

                $result = $wpdb->insert(
                    $table_cart,
                    array(
                        'user_id' => get_current_user_id(),
                        'session_type' => sanitize_text_field($session_type),
                        'price' => sanitize_text_field($price),
                        'room_id' => sanitize_text_field($room_id),
                        'booking_date' => $booking_date,
                        'is_week' => $is_week,
                        'is_instrument' => $is_instrument,
                        'instrument_id' => $instrument_id,
                        'expire_time' => '15-00',
                        'session_expire_time' => $time,
                        'booking_start_time' => $startTime,
                        'booking_end_time' => $endTime,
                        'is_delete' => 0,
                        'created_date' => $date
                    )
                );
                $lastid = $wpdb->insert_id;
                $session_array2[$lastid] = $time;
                if(!empty($session_array1)){
                    $session_array = array_push($session_array1,$session_array2);
                }else{
                    $session_array = $session_array2;
                }
                
                $_SESSION['data'] = $session_array;
                $response['success'] = 1;
            }
        }
        // echo "<pre>";
        // print_r($_SESSION);
        echo json_encode($response);
        die;
    }

    public function fun_get_instrument()
    {
        global $wpdb;
        $table_instruments = SSRB_PREFIX ."instruments";
        
        if($_POST['flag'] == 1){
            $session_type = implode(',',$_POST['ItemArray']);
            $instruments_list = $wpdb->get_results("SELECT * FROM $table_instruments where (session_type IN ($session_type) OR session_type = 3) AND is_active = 1 AND is_delete = 0 ");
        }else{
            $id = $_POST['id'];
            $instruments_list = $wpdb->get_results("SELECT * FROM $table_instruments where id = $id AND is_active = 1 AND is_delete = 0 ");
        }

        if(!empty( $instruments_list )){
            $response['data'] = $instruments_list;
            $response['success'] = 1;
        }else{
            $response['data'] = [];
            $response['success'] = 0; 
        }
        echo json_encode($response);
        die;
    }

    public function fun_expire_booking_room()
    {
        global $wpdb;
        $date = date('Y-m-d H:i:s');
        $cart_id = $_POST['cartid'];
        $min = $_POST['min'];
        $sec = $_POST['sec'];
        $user_id = get_current_user_id();
        
        $table_cart = SSRB_PREFIX ."cart";
        if($min<=0 && $sec<=0){
            $expire_time = '00-00';
        }else{
            $expire_time = $min.'-'.$sec;
        }    
      
        if($min<=0 && $sec<=0){
            $result = 	$wpdb->update(
                $table_cart,    
                array(
                    'is_delete' => 1,
                    'expire_time' => '00-00',
                ),
                array('id' => $cart_id)
            );
            $response['flag'] = 1;
        }
    
        if($result < 0){
            $response['success'] = 0;
        }else{
            $response['success'] = 1;
        }
        $cart_list = $wpdb->get_results("SELECT * FROM $table_cart  where user_id = $user_id AND is_delete = 0 ");
        if(empty( $cart_list )){
            $response['flag'] = 2; 
        }
        echo json_encode($response);
        die;
    }

    public function fun_empty_cart()
    {
        global $wpdb;
        $table_cart = SSRB_PREFIX ."cart";
        $cart_id = $_POST['cartid'];
        if(!empty($cart_id)){
            $result = 	$wpdb->update(
                $table_cart,    
                array(
                    'is_delete' => 1,
                    'expire_time' => '00-00',
                ),
                array('user_id' => get_current_user_id(),'id' => $cart_id)
            );
            $response['flag'] = 1;
        }else{
            $result = 	$wpdb->update(
                $table_cart,    
                array(
                    'is_delete' => 1,
                    'expire_time' => '00-00',
                ),
                array('user_id' => get_current_user_id())
            );
            $response['flag'] = 1;
        }
       
       
        if($result < 0){
            $response['success'] = 0;
        }else{
            $response['success'] = 1;
        }
        $cart_list = $wpdb->get_results("SELECT * FROM $table_cart  where user_id = $user_id AND is_delete = 0 ");
        
        if(empty( $cart_list )){
            $response['flag'] = 2; 
        }
        echo json_encode($response);
        die;
    }

    public function fun_add_to_checkout()
    {
        global $wpdb;
        $table_cart = SSRB_PREFIX ."cart";
        $user_id = get_current_user_id();
        $cart_list = $wpdb->get_results("SELECT * FROM $table_cart  where user_id = $user_id AND status = 1 AND is_delete = 0 ");

        if(!empty( $cart_list )){
            $result = 	$wpdb->update(
                $table_cart,    
                array(
                    'status' => 2,
                ),
                array('user_id' => get_current_user_id(),'is_delete' => 0)
            );

            $response['success'] = 1;
        }else{
            $response['success'] = 0; 
        }
        echo json_encode($response);
        die;
    }

    public function fun_add_update_band()
    {
        global $wpdb;

        $band_name = $_POST['band_name'];
        $description = $_POST['description'];
        $contact_no = $_POST['contact_no'];
        $address = $_POST['address'];
        $city = $_POST['city'];
        $state = $_POST['state'];
        $postal_code = $_POST['postal_code'];
        $country = $_POST['country'];
        $userId = get_current_user_id();

        if(empty(get_user_meta($userId, 'is_band'))){
            add_user_meta($userId, 'is_band', 1 );
        }else{
            update_user_meta($userId, 'is_band', 1 );
        }
        
        if(empty(get_user_meta($userId, 'band_name'))){
            add_user_meta($userId, 'band_name', sanitize_text_field($band_name) );
        }else{
            update_user_meta($userId, 'band_name', sanitize_text_field($band_name) );
        }

        if(empty(get_user_meta($userId, 'band_description'))){
            add_user_meta($userId, 'band_description', sanitize_text_field($description) );
        }else{
            update_user_meta($userId, 'band_description', sanitize_text_field($description) );
        }

        if(empty(get_user_meta($userId, 'contact_no'))){
            add_user_meta($userId, 'contact_no', sanitize_text_field($contact_no) );
        }else{
            update_user_meta($userId, 'contact_no', sanitize_text_field($contact_no) );
        }

        if(empty(get_user_meta($userId, 'address'))){
            add_user_meta($userId, 'address', sanitize_text_field($address) );
        }else{
            update_user_meta($userId, 'address', sanitize_text_field($address) );
        }

        if(empty(get_user_meta($userId, 'city'))){
            add_user_meta($userId, 'city', sanitize_text_field($city) );
        }else{
            update_user_meta($userId, 'city', sanitize_text_field($city) );
        }

        if(empty(get_user_meta($userId, 'state'))){
            add_user_meta($userId, 'state', sanitize_text_field($state) );
        }else{
            update_user_meta($userId, 'state', sanitize_text_field($state) );
        }

        if(empty(get_user_meta($userId, 'postal_code'))){
            add_user_meta($userId, 'postal_code', sanitize_text_field($postal_code) );
        }else{
            update_user_meta($userId, 'postal_code', sanitize_text_field($postal_code) );
        }

        if(empty(get_user_meta($userId, 'country'))){
            add_user_meta($userId, 'country', sanitize_text_field($country) );
        }else{
            update_user_meta($userId, 'country', sanitize_text_field($country) );
        }
        
        $response['success'] = 1;
        echo json_encode($response);
        die;
    }


    public function fun_proccess_booking_room_from_user()
    {
        global $wpdb;
        if (!session_id()) {
            session_start();
        }

        $date = date('Y-m-d H:i:s');
        $ItemArray = $_POST['ItemArray'];
        $user_id = get_current_user_id();
        $total_price = $_POST['total_price'];

        $user = get_userdata($user_id);

        $band_name = get_user_meta( $user_id,'band_name',true);
    
        if($user->first_name && $user->last_name ){
            $username =  ucfirst($user->first_name) . ' ' . ucfirst($user->last_name);
        }else{
            $username = ucfirst($user->display_name);
        }
        $table_booking_details = SSRB_PREFIX ."booking_details";
        $table_cart = SSRB_PREFIX ."cart";
        $table_rooms = SSRB_PREFIX ."rehearsal_rooms";
        $table_instruments = SSRB_PREFIX ."instruments";
       
        $payment_data['product_name'] = $band_name;
        $payment_data['amount'] = $total_price*100;
        $payment_data['currency_code'] = 'USD';
        $payment_data['txn_id'] = '';
        $payment_data['first_name'] = $user->first_name ? ucfirst($user->first_name) : ucfirst($user->display_name);
        $payment_data['last_name'] = $user->last_name ? ucfirst($user->last_name) : ucfirst($user->display_name);
        $payment_data['full_name'] = $username;
        $email_options = wp_stripe_checkout_get_email_option();
        add_filter('wp_mail_from', 'wp_stripe_checkout_set_email_from');
        add_filter('wp_mail_from_name', 'wp_stripe_checkout_set_email_from_name');
        $subject = $email_options['purchase_email_subject'];
        $type = $email_options['purchase_email_type'];
        $body = $email_options['purchase_email_body'];
        $body = wp_stripe_checkout_do_email_tags($payment_data, $body);
        if($type == "html"){
            add_filter('wp_mail_content_type', 'wp_stripe_checkout_set_html_email_content_type');
            $body = apply_filters('wp_stripe_checkout_email_body_wpautop', true) ? wpautop($body) : $body;
        }
    
        $amount_wallet = $_POST['wallet_amount'];
        update_user_meta($user_id, 'ssrb_wallet', sanitize_text_field($amount_wallet) );      

        foreach($ItemArray as $key => $value){
            $session_type = $value['session_type'];
            $price = $value['price'];
            $room_id = $value['room_id'];    
            $booking_date = $value['booking_date'];
            $is_week = $value['is_week'];
            $instrument_id = $value['instrument_id'];
            $payment_status = $value['payment_status'];

            if($instrument_id){
                $is_instrument = 1; 
            }else{
                $is_instrument = 0;
            }
            $cart_list = $wpdb->get_results("SELECT * FROM $table_cart  where user_id = $user_id AND booking_date = '".$booking_date."' AND session_type = $session_type AND status = 1 AND is_delete = 0 ");
          
            $booking_details = $wpdb->get_results("SELECT * FROM $table_booking_details  where user_id = $user_id AND booking_date = '".$booking_date."' AND session_type = $session_type AND is_delete = 0 ");

            if(!empty($cart_list)){
                $_payment_status = get_post_meta($cart_list[0]->order_id, '_payment_status', true);
                if($_payment_status != 3 && $_payment_status != 4){
                    $response['success'] = 0;
                    echo json_encode($response);
                    die;
                }
            }elseif(!empty($booking_details)){
                $_payment_status = get_post_meta($booking_details[0]->order_id, '_payment_status', true);
                if($_payment_status != 3 && $_payment_status != 4){
                    $response['success'] = 0;
                    echo json_encode($response);
                    die;
                }
            }else{
               
                $content = '';
                $content .= '<strong>Band name:</strong> '.$band_name.'<br />';
                
                if($is_instrument == 1){
                    $listData = $wpdb->get_results("SELECT * FROM $table_instruments where id =  $instrument_id AND is_delete = 0 ");
                    if($listData[0]->session_type == $session_type)
                    {
                        $instrument_name = $listData[0]->instrument_name;
                        $instrument_price = $listData[0]->price;
                        $amount = $price + $instrument_price;
                    }else{
                        $amount = $price;
                    }
                }else{
                    $amount = $price;
                }

                $content .= '<strong>Amount:</strong> $'.($amount).'<br />';
                $content .= '<strong>Currency:</strong> USD<br />';
                if($user->user_email){
                    $content .= '<strong>Email:</strong> '.$user->user_email.'<br />'; 
                }
                if( $username ){
                    $content .= '<strong>Name:</strong> '. $username.'<br />';
                }

                $content .= '<h3><strong> Currency Booking Details </strong></h3> <br />';

                $rooms_list = $wpdb->get_results("SELECT * FROM $table_rooms where id = $room_id AND is_delete=0 ");
                
                $content .= '<strong>Room Name:</strong> '.$rooms_list[0]->room_title.'<br />';
                
                if($session_type == 1){
                    $session_name = 'Day';
                }else{
                    $session_name = 'Night'; 
                }

                $content .= '<strong>Session Type:</strong> '.$session_name.'<br />';
                
                $content .= '<strong>Booking Date:</strong> '.$booking_date.'<br />';
                $content .= '<strong>Price:</strong> $'.number_format($price,2) .'<br />';
                
                if($is_instrument == 1){
                    $listData = $wpdb->get_results("SELECT * FROM $table_instruments where id =  $instrument_id AND is_delete = 0 ");
                    if($listData[0]->session_type == $session_type)
                    {
                        $instrument_name = $listData[0]->instrument_name;
                        $instrument_price = $listData[0]->price;
                        $content .= '<strong>Instrument Name:</strong> '.$instrument_name.'<br />';
                        $content .= '<strong>Instrument Price:</strong> $'.$instrument_price.'<br />';
                    }
                }
                $content .= '<br />';
                
                $wp_stripe_checkout_order = array(
                    'post_title' => 'order',
                    'post_type' => 'wpstripeco_order',
                    'post_content' => '',
                    'post_status' => 'publish',
                );
                $post_id = wp_insert_post($wp_stripe_checkout_order);  //insert a new order
                $post_updated = false;
                if ($post_id > 0) {
                    $post_content = $content;
                    $updated_post = array(
                        'ID' => $post_id,
                        'post_title' => $post_id,
                        'post_type' => 'wpstripeco_order',
                        'post_content' => $post_content
                    );
                    $updated_post_id = wp_update_post($updated_post);  //update the order
                    if ($updated_post_id > 0) {  //successfully updated
                        $post_updated = true;
                    }
                }
       
                $startTime = date('Y-m-d h:i:s', time());
                $time = time();
                $endTime = date("Y-m-d h:i:s", strtotime('+15 minutes', $time));
                if ($post_updated) {
                    update_post_meta($post_id, '_txn_id', '');
                    update_post_meta($post_id, '_name', $username);
                    update_post_meta($post_id, '_amount', $amount);
                    update_post_meta($post_id, '_email', $user->user_email);
                    update_post_meta($post_id, '_user_id', $user_id);
                    update_post_meta($post_id, '_payment_status', $payment_status);
        
                        $result = $wpdb->insert(
                            $table_booking_details,
                            array(
                                'order_id'=> $post_id,
                                'user_id' => $user_id,
                                'session_type' => sanitize_text_field($session_type),
                                'price' => sanitize_text_field($price),
                                'room_id' => sanitize_text_field($room_id),
                                'booking_date' => $booking_date,
                                'is_instrument' => $is_instrument,
                                'instrument_id' => $instrument_id,
                                'is_week' => $is_week,
                                'added_by' => 1,
                                'is_delete' => 0,
                                'created_date' => $date
                            )
                        );
                        $response['success'] = 1;
                        $bookingid = $wpdb->insert_id;
                        update_post_meta($post_id, '_booking_id', $bookingid);

                        $result = 	$wpdb->delete(
                            $table_cart,   
                            array('user_id' => get_current_user_id())
                        );
                    }

                    $body .= '<strong>Room Name:</strong> '.$rooms_list[0]->room_title.'<br />';
                    if($session_type == 1){
                        $session_name = 'Day';
                    }else{
                        $session_name = 'Night'; 
                    }

                    
                    $body .= '<strong>Session Type:</strong> '.$session_name.'<br />';
                    
                    if($is_week == 1){
                        $body .= '<strong>Booking Date:</strong> '.$booking_date.' - '.$booking_enddate.'<br />';
                        $body .= '<strong>Total Price:</strong> $'.number_format(($price*7),2).'<br />';
                        $body .= '<strong>One Day Price:</strong> $'.number_format($price,2) .'<br />';
                    }else{
                        $body .= '<strong>Booking Date:</strong> '.$booking_date.'<br />';
                        $body .= '<strong>Price:</strong> $'.number_format($price,2) .'<br />';
                    }
                    
                    if($is_instrument == 1){
                        $listData = $wpdb->get_results("SELECT * FROM $table_instruments where id =  $instrument_id AND is_delete = 0 ");
                        if($listData[0]->session_type == $session_type)
                        {
                            $instrument_name = $listData[0]->instrument_name;
                            $instrument_price = $listData[0]->price;
                            $content .= '<strong>Instrument Name:</strong> '.$instrument_name.'<br />';
                            $content .= '<strong>Instrument Price:</strong> $'.$instrument_price.'<br />';
                        }
                    }
            
                    $body .= '<br />';
            }
        }
        $mail_sent = wp_mail($user->user_email, $subject, $body);
       
        echo json_encode($response);
        die;
    }
}