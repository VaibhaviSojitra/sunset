<?php

if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}
class SSRB_ADMIN_AJAX_INIT {

     
    function __construct()
    {
        add_action('wp_ajax_addedit_settings', array($this, 'fun_addedit_settings'));
        add_action('wp_ajax_nopriv_addedit_settings', array($this, 'fun_addedit_settings'));

        add_action('wp_ajax_addedit_rooms', array($this, 'fun_addedit_rooms'));
        add_action('wp_ajax_nopriv_addedit_rooms', array($this, 'fun_addedit_rooms'));

        add_action('wp_ajax_delete_rooms', array($this, 'fun_delete_rooms'));
        add_action('wp_ajax_nopriv_delete_rooms', array($this, 'fun_delete_rooms'));

        add_action('wp_ajax_addedit_instrument', array($this, 'fun_addedit_instrument'));
        add_action('wp_ajax_nopriv_addedit_instrument', array($this, 'fun_addedit_instrument'));

        add_action('wp_ajax_get_instrument_databyid', array($this, 'fun_get_instrument_databyid'));
        add_action('wp_ajax_nopriv_get_instrument_databyid', array($this, 'fun_get_instrument_databyid'));

        add_action('wp_ajax_change_instrument_status', array($this, 'fun_change_instrument_status'));
        add_action('wp_ajax_nopriv_change_instrument_status', array($this, 'fun_change_instrument_status'));

        add_action('wp_ajax_delete_instrument', array($this, 'fun_delete_instrument'));
        add_action('wp_ajax_nopriv_delete_instrument', array($this, 'fun_delete_instrument'));

        add_action('wp_ajax_get_ratebyweekday', array($this, 'fun_get_ratebyweekday'));
        add_action('wp_ajax_nopriv_get_ratebyweekday', array($this, 'fun_get_ratebyweekday'));

        add_action('wp_ajax_get_username', array($this, 'fun_get_username'));
        add_action('wp_ajax_nopriv_get_username', array($this, 'fun_get_username'));

        add_action('wp_ajax_proccess_booking_room_from_admin', array($this, 'fun_proccess_booking_room_from_admin'));
        add_action('wp_ajax_nopriv_proccess_booking_room_from_admin', array($this, 'fun_proccess_booking_room_from_admin'));
        
    }

    public function fun_addedit_settings()
    {
        global $wpdb;
        $hiddenflag = $_POST['hiddenflag'];
        
        if($hiddenflag == 'session_type'){
            $tablename = SSRB_PREFIX ."session_type";
             $tablename2 = SSRB_PREFIX ."rehearsal_rooms";
            $date = date('Y-m-d H:i:s');

            foreach($_POST['session'] as $key => $value){
                if(empty($value['hidden_id'])){
                    $result = $wpdb->insert(
                        $tablename,
                        array(
                            'session_type' => $key,
                            'start_time' => sanitize_text_field($value['starttime']),
                            'end_time' => sanitize_text_field($value['endtime']),
                            'is_delete' => 0,
                            'created_date' => $date
                        )
                    );
                    $lastid = $wpdb->insert_id;
                }else{
                    $result = 	$wpdb->update(
                        $tablename,
                        array(
                            'start_time' => sanitize_text_field($value['starttime']),
                            'end_time' => sanitize_text_field($value['endtime']),
                        ),
                        array('id' => $value['hidden_id'],'session_type' => sanitize_text_field($key))
                    );
                    
                     if($key == 1){
                        $result = 	$wpdb->update(
                            $tablename2,
                            array(
                                'day_start_time' => sanitize_text_field($value['starttime']),
                                'day_end_time' => sanitize_text_field($value['endtime']),
                            ),
                            array('is_delete' =>0)
                        );
                    }else{
                        $result = 	$wpdb->update(
                            $tablename2,
                            array(
                                'night_start_time' => sanitize_text_field($value['starttime']),
                                'night_end_time' => sanitize_text_field($value['endtime']),
                            ),
                            array('is_delete' =>0)
                        );  
                    }
                }
            }
        }
        if($hiddenflag == 'rate_service'){
            $tablename = SSRB_PREFIX ."rate_service";
            $date = date('Y-m-d H:i:s');
           
            foreach($_POST as $week_key => $week_value){
                foreach($week_value as $key => $value){
                    $result = 	$wpdb->update(
                        $tablename,
                        array(
                            'price' => sanitize_text_field($value['price']),
                        ),
                        array('session_type' => $key,'week_day' => sanitize_text_field($week_key))
                    );
                }
            }
        }
        if($result < 0){
            $response['success'] = 0;
        }else{
            $response['success'] = 1;
        }
        echo json_encode($response);
        die;
    }
    
    public function fun_addedit_rooms()
    {
        global $wpdb;

        $table_rooms = SSRB_PREFIX ."rehearsal_rooms";
        $date = date('Y-m-d H:i:s');
        $hidden_room_id = $_POST['hidden_room_id'];
        $room_title = sanitize_text_field($_POST['room_title']);
        $room_code = sanitize_text_field($_POST['room_code']);

        if(empty($hidden_room_id)){
            $rooms_list = $wpdb->get_results("SELECT * FROM $table_rooms where (room_title = '".$room_title."' OR room_code = '".$room_code."') AND is_delete = 0 ");
        }else{
            $rooms_list = $wpdb->get_results("SELECT * FROM $table_rooms where (room_title = '".$room_title."' OR room_code = '".$room_code."') AND id != $hidden_room_id AND is_delete=0 ");
        }
      
        if(!empty($rooms_list)){
            $response['success'] = 0;
            $response['flag'] = 1;
        }else{
            if(empty($hidden_room_id)){
                $result = $wpdb->insert(
                    $table_rooms,
                    array(
                        'room_title' => sanitize_text_field($_POST['room_title']),
                        'room_color' => sanitize_text_field($_POST['room_color']),
                        'room_code' => sanitize_text_field($_POST['room_code']),
                        'day_start_time' => sanitize_text_field($_POST['day_starttime']),
                        'day_end_time' => sanitize_text_field($_POST['day_endtime']),
                        // 'day_price' => sanitize_text_field($_POST['day_price']),
                        'night_start_time' => sanitize_text_field($_POST['night_starttime']),
                        'night_end_time' => sanitize_text_field($_POST['night_endtime']),
                        // 'night_price' => sanitize_text_field($_POST['night_price']),
                        'is_delete' => 0,
                        'created_date' => $date
                    )
                );
                $lastid = $wpdb->insert_id;
            }else{
                $result = 	$wpdb->update(

                    $table_rooms,
        
                    array(
                        'room_title' => sanitize_text_field($_POST['room_title']),
                        'room_color' => sanitize_text_field($_POST['room_color']),
                        'room_code' => sanitize_text_field($_POST['room_code']),
                        'day_start_time' => sanitize_text_field($_POST['day_starttime']),
                        'day_end_time' => sanitize_text_field($_POST['day_endtime']),
                        // 'day_price' => sanitize_text_field($_POST['day_price']),
                        'night_start_time' => sanitize_text_field($_POST['night_starttime']),
                        'night_end_time' => sanitize_text_field($_POST['night_endtime']),
                        // 'night_price' => sanitize_text_field($_POST['night_price'])
                    ),
                    array('id' => $hidden_room_id)
                );
            }

            if($result < 0){
                $response['success'] = 0;
            }else{
                $response['success'] = 1;
            }
            $response['flag'] = 2;
        }
        echo json_encode($response);
        die;
    }

    public function fun_delete_rooms()
    {
        global $wpdb;

        $table_rooms = SSRB_PREFIX ."rehearsal_rooms";
        $id = $_POST['id'];

        $result = 	$wpdb->update(
            $table_rooms,
            array(
                'is_delete' => 1
            ),
            array('id' => $id)
        );

        if($result < 0){
            $response['success'] = 0;
        }else{
            $response['success'] = 1;
        }
        echo json_encode($response);
        die;
    }

    public function fun_addedit_instrument()
    {
        global $wpdb;

        $table_instruments = SSRB_PREFIX ."instruments";
        $date = date('Y-m-d H:i:s');
        $hidden_id = $_POST['hidden_id'];
        $instrument_name = sanitize_text_field($_POST['instrument_name']);
        if(empty($hidden_id)){
            $instruments_list = $wpdb->get_results("SELECT * FROM $table_instruments where instrument_name = '".$instrument_name."' AND is_delete = 0 ");
        }else{
            $instruments_list = $wpdb->get_results("SELECT * FROM $table_instruments where instrument_name = '".$instrument_name."' AND id != $hidden_id AND is_delete=0 ");
        }
      
        if(!empty($instruments_list)){
            $response['success'] = 0;
            $response['flag'] = 3;
        }else{
            if(empty($hidden_id)){
                $result = $wpdb->insert(
                    $table_instruments,
                    array(
                        'instrument_name' => sanitize_text_field($_POST['instrument_name']),
                        'session_type' => sanitize_text_field($_POST['session_type']),
                        'price' => sanitize_text_field($_POST['price']),
                        'image' => sanitize_text_field($_POST['instrument_image']),
                        'is_active' => 1,
                        'is_delete' => 0,
                        'created_date' => $date
                    )
                );
                $lastid = $wpdb->insert_id;
                $response['flag'] = 1;
            }else{
                $result = 	$wpdb->update(
                    $table_instruments,    
                    array(
                        'instrument_name' => sanitize_text_field($_POST['instrument_name']),
                        'session_type' => sanitize_text_field($_POST['session_type']),
                        'price' => sanitize_text_field($_POST['price']),
                        'image' => sanitize_text_field($_POST['instrument_image']),
                    ),
                    array('id' => $hidden_id)
                );
                $lastid = $hidden_id;
                $response['flag'] = 2;
            }
            $listData = $wpdb->get_results("SELECT * FROM $table_instruments where id =  $lastid AND is_delete=0 ");

            if($listData){
                $response['success'] = 1;
                $response['data'] = $listData;
            }else{
                $response['success'] = 0;
                $response['data'] = [];  
            }
        }
        echo json_encode($response);
        die;
    }

    public function fun_get_instrument_databyid()
    {
        global $wpdb;

        $table_instruments = SSRB_PREFIX ."instruments";
        $id = $_POST['id'];
        $listData = $wpdb->get_results("SELECT * FROM $table_instruments where id =  $id AND is_delete = 0 ");

        if($listData){
            $response['success'] = 1;
            $response['data'] = $listData;
        }else{
            $response['success'] = 0;
            $response['data'] = [];  
        }
        echo json_encode($response);
        die;
    }
    
    public function fun_change_instrument_status()
    {
        global $wpdb;

        $table_instruments = SSRB_PREFIX ."instruments";
        $id = $_POST['id'];

        $result = 	$wpdb->update(
            $table_instruments,    
            array(
                'is_active' => sanitize_text_field($_POST['status']),
            ),
            array('id' => $id)
        );
        if($result < 0){
            $response['success'] = 0;
        }else{
            $response['success'] = 1;
        }
        echo json_encode($response);
        die;
    }

    public function fun_delete_instrument()
    {
        global $wpdb;

        $table_instruments = SSRB_PREFIX ."instruments";
        $id = $_POST['id'];

        $result = 	$wpdb->update(
            $table_instruments,
            array(
                'is_delete' => 1
            ),
            array('id' => $id)
        );

        if($result < 0){
            $response['success'] = 0;
        }else{
            $response['success'] = 1;
        }
        echo json_encode($response);
        die;
    }

    public function fun_get_username(){
        global $wpdb;
        $user_id = $_POST['user_id'];

        $user_info = get_user_meta( $user_id,'band_name');
        $user = get_userdata( $user_id );
        if($user_info[0]){
            $username = ucfirst($user_info[0]);
        }else
        if($user->first_name && $user->last_name ){
            $username =  ucfirst($user->first_name) . ' ' . ucfirst($user->last_name);
        }else{
            $username = ucfirst($user->display_name);
        }
        $response['username'] = strtoupper($username);
        echo json_encode($response);
        die;
    }

    public function fun_get_ratebyweekday()
    {
        global $wpdb;
        $weekdayname = $_POST['weekdayname'];
        $date = $_POST['date'];

        $flag = $_POST['flag'];
        $table_rates = SSRB_PREFIX ."rate_service";
        $table_booking_details = SSRB_PREFIX ."booking_details";
        $table_rooms = SSRB_PREFIX ."rehearsal_rooms";
 
        $listData = $wpdb->get_results("SELECT * FROM $table_rates where week_day Like '".$weekdayname."' AND is_delete = 0 ");
        if($flag == 1){
            $booking_details = $wpdb->get_results("SELECT b.*,r.room_title,r.day_start_time,r.night_start_time FROM $table_booking_details as `b` JOIN $table_rooms as `r` ON `r`.`id`=`b`.room_id where (DAYNAME(b.booking_date) = '".$weekdayname."' OR b.week_day = '".$weekdayname."') AND b.is_delete = 0 ORDER BY b.id DESC ");
        }else{
            $booking_details = $wpdb->get_results("SELECT b.*,r.room_title,r.day_start_time,r.night_start_time FROM $table_booking_details as `b` JOIN $table_rooms as `r` ON `r`.`id`=`b`.room_id where (b.booking_date = '".$date."' OR b.week_day = '".$weekdayname."') AND b.is_delete = 0 ");
        }

        $response['data'] = $listData;
        $response['bookingdata'] = $booking_details;
        $response['success'] = 1;
   
        echo json_encode($response);
        die;
    }

    public function fun_proccess_booking_room_from_admin()
    {
        global $wpdb;
        if (!session_id()) {
            session_start();
        }

        $date = date('Y-m-d H:i:s');
        $ItemArray = $_POST['ItemArray'];
        $user_id = $_POST['user_id'];
        $user_name = $_POST['user_name'];
        $user_email = $_POST['user_email'];
        $band_name = $_POST['band_name'] ? $_POST['band_name'] : ""; 
        $user_flag = $_POST['user_flag'];
        $total_price = $_POST['total_price'];
        $booking_date = $_POST['booking_date'];
        $instrument_id = $_POST['instrument'];
        $is_week = $_POST['is_week'];
       
        if($user_flag == 1){   
            $full_name = explode(' ',$user_name);
            if(count($full_name) >= 2){
                $first_name = $full_name[0];
                $last_name = $full_name[1];
            }else{
                $first_name = $user_name;
                $last_name = $user_name;
            }

            if( ( $user_email && null == email_exists( $user_email ) ) || null == username_exists( $user_name ) ) {
                $password = wp_generate_password( 12, true );
                $user_id = wp_create_user ( $user_name, $password, $user_email );

                wp_update_user(
                    array(
                      'ID'       => $user_id,
                      'nickname' => $user_name
                    )
                  );
             
                update_user_meta($user_id,'first_name',$first_name);
                update_user_meta($user_id,'last_name',$last_name);
                update_user_meta($user_id,'band_name',$band_name);
                update_user_meta($user_id,'nickname', $user_name);

                $user = new WP_User( $user_id );
                
                $user->set_role( 'subscriber' );
                if($user_email){
                    wp_mail( $user_email, 'Welcome to Sunset Rehearsals!', 'Your username is: ' . $user_name.' <br/> Your password is: ' . $password );

                }

            }else{

                // update_user_meta($user_id,'first_name',$first_name);
                // update_user_meta($user_id,'last_name',$last_name);
                // update_user_meta($user_id,'band_name',$band_name);
                // update_user_meta($user_id,'nickname', $user_name);

                // wp_update_user(
                //     array(
                //       'ID'       => $user_id,
                //       'nickname' => $user_name
                //     )
                //   );
                $users = get_user_by('login', $user_name);
                $user_id = $users->ID;
                $user = get_userdata($user_id);

            }                
        }else{
            $user = get_userdata($user_id);
        }

        if($instrument_id){
            $is_instrument = 1; 
        }else{
            $is_instrument = 0;
        }
     
        $band_name = get_user_meta( $user_id,'band_name',true);
    
        if($user->first_name && $user->last_name ){
            $username =  ucfirst($user->first_name) . ' ' . ucfirst($user->last_name);
        }else{
            $username = ucfirst($user->display_name);
        }
        $table_booking_details = SSRB_PREFIX ."booking_details";
        $table_cart = SSRB_PREFIX ."cart";
        $table_rooms = SSRB_PREFIX ."rehearsal_rooms";
        $table_instruments = SSRB_PREFIX ."instruments";

        foreach($ItemArray as $key => $value){
            $session_type = $value['session_type'];
            $price = $value['price'];
            $room_id = $value['room_id'];    
            $booking_date = $value['booking_date'];
            $week_day = $value['week_day'];
            if($week_day == 1){
                $where = "(booking_date = '".$booking_date."' OR week_day = '".$week_day."')";
            }else{
                $where = "(booking_date = '".$booking_date."')";
            }
            $cart_list = $wpdb->get_results("SELECT * FROM $table_cart  where user_id = $user_id AND $where AND session_type = $session_type AND is_delete = 0 ");
      
            $booking_details = $wpdb->get_results("SELECT * FROM $table_booking_details  where user_id = $user_id AND $where AND session_type = $session_type AND is_delete = 0 ");
          
            if(!empty($cart_list)){
                $_payment_status = get_post_meta($cart_list[0]->order_id, '_payment_status', true);
                if($_payment_status != 3 && $_payment_status != 4){
                    $response['success'] = 0;
                    echo json_encode($response);
                    die;
                }
            }elseif(!empty($booking_details)){
                $_payment_status = get_post_meta($booking_details[0]->order_id, '_payment_status', true);
                if($_payment_status != 3 && $_payment_status != 4){
                    $response['success'] = 0;
                    echo json_encode($response);
                    die;
                }
            }else{
               
                $content = '';
                $content .= '<strong>Band name:</strong> '.$band_name.'<br />';
                
                if($is_instrument == 1){
                    $listData = $wpdb->get_results("SELECT * FROM $table_instruments where id =  $instrument_id AND is_delete = 0 ");
                    if($listData[0]->session_type == $session_type)
                    {
                        $instrument_name = $listData[0]->instrument_name;
                        $instrument_price = $listData[0]->price;
                        $amount = $price + $instrument_price;
                    }else{
                        $amount = $price;
                    }
                }else{
                    $amount = $price;
                }

                $content .= '<strong>Amount:</strong> '.SSRB_BOOKING_CURRENCY.' '.($amount).'<br />';
                $content .= '<strong>Currency:</strong> '.SSRB_BOOKING_CURRENCY.' <br />';
                if($user->user_email){
                    $content .= '<strong>Email:</strong> '.$user->user_email.'<br />'; 
                }
                if( $username ){
                    $content .= '<strong>Name:</strong> '. $username.'<br />';
                }

                $content .= '<h3><strong> Booking Details </strong></h3> <br />';

                $rooms_list = $wpdb->get_results("SELECT * FROM $table_rooms where id = $room_id AND is_delete=0 ");
                
                $content .= '<strong>Room Name:</strong> '.$rooms_list[0]->room_title.'<br />';
                
                if($session_type == 1){
                    $session_name = 'Day';
                }else{
                    $session_name = 'Night'; 
                }

                $content .= '<strong>Session Type:</strong> '.$session_name.'<br />';
                $content .= '<strong>Booking Date:</strong> '.$booking_date.'<br />';
                $content .= '<strong>Price:</strong> '.SSRB_BOOKING_CURRENCY.' '.number_format($price,2) .'<br />';
                
                if($is_instrument == 1){
                    $listData = $wpdb->get_results("SELECT * FROM $table_instruments where id =  $instrument_id AND is_delete = 0 ");
                    if($listData[0]->session_type == $session_type)
                    {
                        $instrument_name = $listData[0]->instrument_name;
                        $instrument_price = $listData[0]->price;
                        $content .= '<strong>Instrument Name:</strong> '.$instrument_name.'<br />';
                        $content .= '<strong>Instrument Price:</strong> '.SSRB_BOOKING_CURRENCY.' '.$instrument_price.'<br />';
                    }
                }
                $content .= '<br />';
                
                $wp_stripe_checkout_order = array(
                    'post_title' => 'order',
                    'post_type' => 'wpstripeco_order',
                    'post_content' => '',
                    'post_status' => 'publish',
                );
                $post_id = wp_insert_post($wp_stripe_checkout_order);  //insert a new order
                $post_updated = false;
                if ($post_id > 0) {
                    $post_content = $content;
                    $updated_post = array(
                        'ID' => $post_id,
                        'post_title' => $post_id,
                        'post_type' => 'wpstripeco_order',
                        'post_content' => $post_content
                    );
                    $updated_post_id = wp_update_post($updated_post);  //update the order
                    if ($updated_post_id > 0) {  //successfully updated
                        $post_updated = true;
                    }
                }
                if($is_week == 1){
                    $week_day = $week_day;
                }else{
                    $week_day = "";
                }
                $startTime = date('Y-m-d h:i:s', time());
                $time = time();
                $endTime = date("Y-m-d h:i:s", strtotime('+15 minutes', $time));
                if ($post_updated) {
                    update_post_meta($post_id, '_txn_id', '');
                    update_post_meta($post_id, '_name', $username);
                    update_post_meta($post_id, '_amount', $amount);
                    update_post_meta($post_id, '_email', $user->user_email);
                    update_post_meta($post_id, '_user_id', $user_id);
                    update_post_meta($post_id, '_payment_status', 1);
        
                        $result = $wpdb->insert(
                            $table_booking_details,
                            array(
                                'order_id'=> $post_id,
                                'user_id' => $user_id,
                                'session_type' => sanitize_text_field($session_type),
                                'price' => sanitize_text_field($price),
                                'room_id' => sanitize_text_field($room_id),
                                'booking_date' => $booking_date,
                                'is_instrument' => $is_instrument,
                                'instrument_id' => $instrument_id,
                                'is_week' => $is_week,
                                'week_day' => $week_day,
                                'added_by' => 1,
                                'is_delete' => 0,
                                'created_date' => $date
                            )
                        );
                        $response['success'] = 1;
                        $bookingid = $wpdb->insert_id;
                        update_post_meta($post_id, '_booking_id', $bookingid);
                    }


                    $payment_data['product_name'] = $band_name;
                    $payment_data['amount'] = $total_price*100;
                    $payment_data['currency_code'] = 'USD';
                    $payment_data['txn_id'] = '';
                    $payment_data['billing_first_name'] = $user->first_name ? ucfirst($user->first_name) : ucfirst($user->display_name);
                    $payment_data['billing_last_name'] = $user->last_name ? ucfirst($user->last_name) : ucfirst($user->display_name);
                    $payment_data['billing_name'] = $username;
                    $payment_data['room_name'] = $rooms_list[0]->room_title;
                    $payment_data['room_color'] = $rooms_list[0]->room_color;
                    $payment_data['room_code'] = $rooms_list[0]->room_code;
                    if($session_type == 1){
                        $session_name = 'Day';
                        $session_time = $rooms_list[0]->day_start_time.' - '.$rooms_list[0]->day_end_time;
                    }else{
                        $session_name = 'Night'; 
                        $session_time = $rooms_list[0]->night_start_time.' - '.$rooms_list[0]->night_end_time;
                    }            
                    $payment_data['session_time'] = $session_time;
                    $payment_data['booking_date'] = $booking_date;


                    $email_options = wp_stripe_checkout_get_email_option();
                    add_filter('wp_mail_from', 'wp_stripe_checkout_set_email_from');
                    add_filter('wp_mail_from_name', 'wp_stripe_checkout_set_email_from_name');
                    $subject = $email_options['purchase_email_subject'];
                    $type = $email_options['purchase_email_type'];
                    $body = $email_options['purchase_email_body'];
                    $body = wp_stripe_checkout_do_email_tags($payment_data, $body);
                    if($type == "html"){
                        add_filter('wp_mail_content_type', 'wp_stripe_checkout_set_html_email_content_type');
                        $body = apply_filters('wp_stripe_checkout_email_body_wpautop', true) ? wpautop($body) : $body;
                    }
                    
                    // $body .= '<strong>Room Name:</strong> '.$rooms_list[0]->room_title.'<br />';
                    // if($session_type == 1){
                    //     $session_name = 'Day';
                    // }else{
                    //     $session_name = 'Night'; 
                    // }

                    
                    // $body .= '<strong>Session Type:</strong> '.$session_name.'<br />';
                    
                    // if($is_week == 1){
                    //     $body .= '<strong>Permanent Booking Day:</strong>Every '.$week_day.', Upcoming Booking - '.$booking_date.'<br />';
                    //     $body .= '<strong>Price:</strong> $'.number_format(($price),2).'<br />';
                    //     // $body .= '<strong>One Day Price:</strong> $'.number_format($price,2) .'<br />';
                    // }else{
                    //     $body .= '<strong>Booking Date:</strong> '.$booking_date.'<br />';
                    //     $body .= '<strong>Price:</strong> $'.number_format($price,2) .'<br />';
                    // }
                    
                    // if($is_instrument == 1){
                    //     $listData = $wpdb->get_results("SELECT * FROM $table_instruments where id =  $instrument_id AND is_delete = 0 ");
                    //     if($listData[0]->session_type == $session_type)
                    //     {
                    //         $instrument_name = $listData[0]->instrument_name;
                    //         $instrument_price = $listData[0]->price;
                    //         $body .= '<strong>Instrument Name:</strong> '.$instrument_name.'<br />';
                    //         $body .= '<strong>Instrument Price:</strong> $'.$instrument_price.'<br />';
                    //     }
                    // }
            
                    // $body .= '<br />';
            }
        }
        $mail_sent = wp_mail($user->user_email, $subject, $body);
        echo json_encode($response);
        die;
    }
}
