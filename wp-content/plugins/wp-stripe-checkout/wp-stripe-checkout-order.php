<?php

function wp_stripe_checkout_register_order_type() {
    $labels = array(
        'name' => __('Orders', 'wp-stripe-checkout'),
        'singular_name' => __('Order', 'wp-stripe-checkout'),
        'menu_name' => __('Stripe Checkout', 'wp-stripe-checkout'),
        'name_admin_bar' => __('Order', 'wp-stripe-checkout'),
        'add_new' => __('Add New', 'wp-stripe-checkout'),
        'add_new_item' => __('Add New Order', 'wp-stripe-checkout'),
        'new_item' => __('New Order', 'wp-stripe-checkout'),
        'edit_item' => __('View Order', 'wp-stripe-checkout'),
        'view_item' => __('View Order', 'wp-stripe-checkout'),
        'all_items' => __('All Orders', 'wp-stripe-checkout'),
        'search_items' => __('Search Orders', 'wp-stripe-checkout'),
        'parent_item_colon' => __('Parent Orders:', 'wp-stripe-checkout'),
        'not_found' => __('No Orders found.', 'wp-stripe-checkout'),
        'not_found_in_trash' => __('No orders found in Trash.', 'wp-stripe-checkout')
    );
    
    $capability = 'manage_options';
    $capabilities = array(
        'edit_post' => $capability,
        'read_post' => $capability,
        'delete_post' => $capability,
        'create_posts' => $capability,
        'edit_posts' => $capability,
        'edit_others_posts' => $capability,
        'publish_posts' => $capability,
        'read_private_posts' => $capability,
        'read' => $capability,
        'delete_posts' => $capability,
        'delete_private_posts' => $capability,
        'delete_published_posts' => $capability,
        'delete_others_posts' => $capability,
        'edit_private_posts' => $capability,
        'edit_published_posts' => $capability
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'show_in_menu' => current_user_can('manage_options') ? true : false,
        'query_var' => false,
        'rewrite' => false,
        'capabilities' => $capabilities,
        'capabilities' => array(
            'create_posts' => false, 
          ),
        'map_meta_cap' => true,
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('editor')
    );

    register_post_type('wpstripeco_order', $args);
}

function disable_ssrb_update_button() {
    if (isset($_GET['post']) && get_post_type($_GET['post']) == 'wpstripeco_order') {
        echo '<style type="text/css">
        #publishing-action { display:none; }
        </style>';
    }
}
add_action('admin_head', 'disable_ssrb_update_button');

function wp_stripe_checkout_order_columns($columns) {
    unset($columns['title']);
    unset($columns['date']);
    $edited_columns = array(
        'title' => __('Order', 'wp-stripe-checkout'),
        'txn_id' => __('Transaction ID', 'wp-stripe-checkout'),
        'room_name' => __('Room Details', 'wp-stripe-checkout'),
        'is_permanent' => __('Is Permanent', 'wp-stripe-checkout'),
        'booking_date' => __('Booking Day', 'wp-stripe-checkout'),
        'name' => __('Name', 'wp-stripe-checkout'),
        'email' => __('Email', 'wp-stripe-checkout'),
        'amount' => __('Total', 'wp-stripe-checkout'),
        'payment_status' => __('Payment Status', 'wp-stripe-checkout')
    );
    return array_merge($columns, $edited_columns);
}

function wp_stripe_checkout_custom_column($column, $post_id) {
    $_booking_id = get_post_meta($post_id, '_booking_id', true);
    $_user_id = get_post_meta($post_id, '_user_id', true);
    global $wpdb;
    $table_booking_details = SSRB_PREFIX ."booking_details";
    $table_rooms = SSRB_PREFIX ."rehearsal_rooms";
    $booking_details = $wpdb->get_results("SELECT b.*,r.room_title,r.day_start_time,r.night_start_time FROM $table_booking_details as b JOIN $table_rooms as `r` ON `r`.`id`=`b`.room_id  where b.order_id = $post_id AND b.user_id = $_user_id AND b.id = '".$_booking_id."' AND b.is_delete = 0 ");

    switch ($column) {
        case 'title' :
            echo $post_id;
            break;
        case 'txn_id' :
            echo get_post_meta($post_id, '_txn_id', true);
            break;
        case 'room_name' :
            if($booking_details[0]->session_type == 1){
                $session = 'Day ('.$booking_details[0]->day_start_time.')';
            }else{
                $session = 'Night ('.$booking_details[0]->night_start_time.')';
            }
            echo $booking_details[0]->room_title.' - '.$session;
            break;
        case 'name' :
            echo get_post_meta($post_id, '_name', true);
            break;
        case 'email' :
            echo get_post_meta($post_id, '_email', true);
            break;
        case 'amount' :
            echo SSRB_BOOKING_CURRENCY.' '.number_format(get_post_meta($post_id, '_amount', true),2);
            break;
        case 'is_permanent' :
            if($booking_details[0]->is_week == 1){
                echo 'Yes';
            }else{
                echo 'No';
            }
            break;
        case 'booking_date' :
            $payment_status = get_post_meta($post_id, '_payment_status', true);
            if($booking_details[0]->is_week == 1){
            
                
                $bookdate = $booking_details[0]->booking_date;
                if($payment_status == 2){
                    $weekNoNextday =    $booking_date = date('Y-m-d', strtotime('next '.$booking_details[0]->week_day, strtotime($booking_details[0]->booking_date))); 
                }else{
                    $nextday = strtotime('next '.$booking_details[0]->week_day);
                    $weekNoNextday = date('Y-m-d', $nextday);
                } 
              
                if($weekNoNextday == $bookdate){
                    echo 'Every '.$booking_details[0]->week_day.' <br/> Upcoming Booking -  <br/>'.$bookdate;
                }else if($bookdate == date('Y-m-d')){
                    echo 'Every '.$booking_details[0]->week_day.' <br/> Today Booking -  <br/>'.$bookdate;
                }else{
                    echo 'Every '.$booking_details[0]->week_day.' <br/> '.$bookdate;
                }

                
            }else{
                echo $booking_details[0]->booking_date;
            }
            break;
        case 'payment_status' :
            
            $payment_status = get_post_meta($post_id, '_payment_status', true);
            if($payment_status == 1){ $status1 =  'selected'; }
            else if($payment_status == 2){ $status2 =  'selected'; }
            else if($payment_status == 3){ $status3 =  'selected'; }
            else if($payment_status == 4){ $status4 =  'selected'; }
            echo '<select name="ssrb_payment_status" class="ssrb_payment_status" hidden_userid="'.$_user_id.'" hidden_postid="'.$post_id.'" hidden_amount="'.get_post_meta($post_id, '_amount', true).'">
            <option value="1" '.$status1.'>Pending</option>
            <option value="2"'.$status2.'>Completed</option>
            <option value="3"'.$status3.'>Refund</option>
            <option value="4"'.$status4.'>Cancelled</option>
            </select>';
        break;
    }
}

add_action( 'admin_footer', 'custom_status_jquery' );

function custom_status_jquery()
{
   ?>
    <script>
    var $ = jQuery;
    var ajaxurl = "<?php echo admin_url('admin-ajax.php') ?>";
    var hidden_url = "<?php echo admin_url( 'edit.php?post_type=wpstripeco_order' ) ?>";
    $(document).ready(function () {
  
        $(document).on('change', '.ssrb_payment_status', function (event) {
            var id = $(this).val();

            var userid = $(this).attr('hidden_userid');
            var amount = $(this).attr('hidden_amount');
            var post_idd = $(this).attr('hidden_postid');

            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: { "status": id,"user_id": userid,"amount": amount,"post_idd":post_idd, "action": 'update_payment_status' },
                cache: false,
                dataType:"json",
                success: function (response) {
                    window.location = hidden_url;
                }
            });
        });

    });
    </script>
   <?php
}

add_action('wp_ajax_update_payment_status', 'fun_update_payment_status');
add_action('wp_ajax_nopriv_update_payment_status', 'fun_update_payment_status');


function fun_update_payment_status()
{
    $post_idd =  $_POST['post_idd'];
    global $wpdb;
    if($_POST['status'] == 3 || $_POST['status'] == 4){
        $amount = get_user_meta($_POST['user_id'], 'ssrb_wallet',true);
        $amount2 =  (float)$amount+(float)$_POST['amount'];
        update_user_meta($_POST['user_id'], 'ssrb_wallet', $amount2 );

        // $table_booking_details = SSRB_PREFIX ."booking_details";
        // $result = 	$wpdb->update(
        //     $table_booking_details,    
        //     array(
        //         'is_delete' => 1,
        //     ),
        //     array('order_id' => $post_idd)
        // );
    }elseif($_POST['status'] == 1){
        update_post_meta($post_idd, '_txn_id', "");
    }
    // elseif($_POST['status'] == 4){
    //     $table_booking_details = SSRB_PREFIX ."booking_details";
    //     $result = 	$wpdb->update(
    //         $table_booking_details,    
    //         array(
    //             'is_delete' => 1,
    //         ),
    //         array('order_id' => $post_idd)
    //     );
    // }
  
    update_post_meta($post_idd, '_payment_status', $_POST['status']);
 
    echo json_encode(array('response'=>1));
    // wp_redirect( admin_url( 'edit.php?post_type=wpstripeco_order' ) );
    exit();
}

add_action( 'admin_head-edit.php', 'refund_amount' );

function refund_amount()
{
    global $typenow;
    if( 'wpstripeco_order' != $typenow )
        return;
    // if( isset( $_GET['amount'] ) && isset( $_GET['user_id'] ))
    // {
    //     $amount = get_user_meta($_GET['user_id'], 'ssrb_wallet',true);
    //     $amount =  $amount + $_GET['amount'];
    //     update_user_meta($_GET['user_id'], 'ssrb_wallet', sanitize_text_field($amount) );
    //     $post_id =  $_GET['ids'];
    //     update_post_meta($post_id, '_payment_status', 3);
    //     wp_redirect( admin_url( 'edit.php?post_type=wpstripeco_order' ) );
    //     exit();
    // }
}

function wp_stripe_checkout_save_meta_box_data($post_id) {
    /*
     * We need to verify this came from our screen and with proper authorization,
     * because the save_post action can be triggered at other times.
     */
    // Check if our nonce is set.
    if (!isset($_POST['wpstripecheckout_meta_box_nonce'])) {
        return;
    }
    // Verify that the nonce is valid.
    if (!wp_verify_nonce($_POST['wpstripecheckout_meta_box_nonce'], 'wpstripecheckout_meta_box')) {
        return;
    }
    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    // Check the user's permissions.
    if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return;
        }
    } else {
        if (!current_user_can('edit_post', $post_id)) {
            return;
        }
    }
}


add_action( 'admin_action_refund', 'refund_post_type' );

add_action('save_post', 'wp_stripe_checkout_save_meta_box_data');

// function wpstripeco_order_skip_trash($post_id) {
//     if (get_post_type($post_id) == 'wpstripeco_order') {
//         // Force delete
//         wp_trash_post( $post_id, true );
//     }
// } 
// add_action('wp_trash_post', 'wpstripeco_order_skip_trash');