<?php
/**
 * Template Name: Sunset Home
 *
 * Custom Template for Sunset combining the fullwidth business slider with the wigetized homepage section
 * 
 *
 * @package WooFramework
 * @subpackage Template
 */

global $woo_options, $wp_query;
get_header();

$page_template = woo_get_page_template();
?>
    <!-- #content Starts -->
	<?php woo_content_before(); ?>
	<?php if ( ( isset( $woo_options['woo_slider_biz'] ) && 'true' == $woo_options['woo_slider_biz'] ) && ( isset( $woo_options['woo_slider_biz_full'] ) && 'true' == $woo_options['woo_slider_biz_full'] ) ) { $saved = $wp_query; woo_slider_biz(); $wp_query = $saved; } ?>
    <div id="content" class="col-full business">

    <!-- #homepage-widgets Starts -->
    <div id="homepage-widgets">

    <?php if ( ( isset( $woo_options['woo_slider_biz'] ) && 'true' == $woo_options['woo_slider_biz'] ) && ( isset( $woo_options['woo_slider_biz_full'] ) && 'false' == $woo_options['woo_slider_biz_full'] ) ) { $saved = $wp_query; woo_slider_biz(); $wp_query = $saved; } ?>

    	<div class="home-widget-wrapper">
        <?php if ( is_active_sidebar( 'homepage' ) ) {
        	//  This Widget area is loaded outside the main-sidebar-container area,will always be the full width of the #content 
            dynamic_sidebar( 'homepage' );  
        } ?>
    	</div>
    </div><!-- /#homepage-widgets -->

	<div id="main-sidebar-container">

            <!-- #main Starts -->
            <?php woo_main_before(); ?>

            <section id="main">
<?php
	woo_loop_before();

		if (is_active_sidebar( 'homepage-2' ) ) {
			dynamic_sidebar( 'homepage-2' ); // Load the second homepage sidebar if it is active
		} else {

			if ( have_posts() ) { $count = 0;
				while ( have_posts() ) { the_post(); $count++;
					woo_get_template_part( 'content', 'page-template-business' ); // Get the page content template file, contextually.
				}
			}
		}
	woo_loop_after();
?>
            </section><!-- /#main -->
            <?php woo_main_after(); ?>

			<?php get_sidebar(); ?>

		</div><!-- /#main-sidebar-container -->

		<?php get_sidebar( 'alt' ); ?>

    </div><!-- /#content -->
	<?php woo_content_after(); ?>

<?php get_footer(); ?>