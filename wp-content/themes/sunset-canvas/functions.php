<?php
function cron_ssrb_advance_booking_cron_hook_8aeb6b2a() {
    global $wpdb;
    $table_booking_details = SSRB_PREFIX ."booking_details";
    $booking_list = $wpdb->get_results("SELECT * FROM $table_booking_details where DATE(booking_date) <= CURDATE() + INTERVAL 5 DAY AND  is_week = 1 AND is_delete = 0 ");

    foreach($booking_list as $value){
        $payment_status = get_post_meta($value->order_id, '_payment_status', true);
        if($payment_status == 1){
            wp_trash_post( $value->order_id );
            $result = $wpdb->update(
                $table_booking_details,   
                array('is_delete' => 1),
                array('id' => $value->id)
            );
        }        
    }
}

add_action( 'ssrb_advance_booking_cron_hook', 'cron_ssrb_advance_booking_cron_hook_8aeb6b2a', 10, 0 );


function sunset_disable_plugin_updates( $value ) {
    unset( $value->response['wp-user/wp-user.php'] );
	return $value;
 }
 add_filter( 'site_transient_update_plugins', 'sunset_disable_plugin_updates' );

// function ssrb_advance_booking_cron_hook_8aeb6b2a() {
//     global $wpdb;
//     $table_booking_details = SSRB_PREFIX ."booking_details";
//     $booking_list = $wpdb->get_results("SELECT * FROM $table_booking_details where DATE(booking_date) <= CURDATE() + INTERVAL 5 DAY AND  is_week = 1 AND is_delete = 0 ");
//     echo "<pre>";
//     print_r($booking_list);
//     // foreach($booking_list as $value){
//     //     $payment_status = get_post_meta($value->order_id, '_payment_status', true);
//     //     if($payment_status == 1){
//     //         wp_delete_post( $value->order_id );
//     //         $result = $wpdb->update(
//     //             $table_booking_details,   
//     //             array('is_delete' => 1),
//     //             array('id' => $value->id)
//     //         );
//     //     }        
//     // }
// }

// add_action( 'init', 'ssrb_advance_booking_cron_hook_8aeb6b2a' );